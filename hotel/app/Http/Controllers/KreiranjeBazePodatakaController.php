<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HotelskaSoba;
use App\Restoran;
use App\Usluge;
use App\CenovnikRestorana;
use App\Korisnik;

class KreiranjeBazePodatakaController extends Controller
{
	function kreiraj()
	{

		
		//KREIRANJE SOBA
		//1. sprat
		$novaSoba = new HotelskaSoba(['broj_sobe' => 101, 'broj_kreveta' => 1, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 1, 'pogled' => 'Pogled na more', 'opis' => 'Idealna soba za parove koji bi na terasi voleli da uzivaju u mirisu mora i u pogledu na more. Soba ima jedan bracni krevet, terasu, kuhinju sa svim potrebnim priborom i internet.', 'terasa' => true, 'wifi' => true, 'tv' => false, 'klima' => false, 'cena_po_noci' => 5500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 102, 'broj_kreveta' => 1, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 1, 'pogled' => 'Pogled na more', 'opis' => 'Idealna soba za parove koji bi hteli da se odmore od buke grada. Soba ima jedan bracni krevet, televizor, kuhinju sa svim potrebnim priborom i klimu.', 'terasa' => false, 'wifi' => false, 'tv' => true, 'klima' => true, 'cena_po_noci' => 6500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 103, 'broj_kreveta' => 2, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 1, 'pogled' => 'Pogled na more', 'opis' => 'Dvokrevetna soba sa terasom, klima uredjajem i internetom. Terasa je okrenuta ka moru, tako da gosti hotela mogu sa terase da uzivaju u moru.', 'terasa' => true, 'wifi' => true, 'tv' => false, 'klima' => true, 'cena_po_noci' => 7000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 104, 'broj_kreveta' => 2, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 1, 'pogled' => 'Pogled na planinu', 'opis' => 'Dvokrevetna soba sa televizorom i brzim internetom. Ova soba opremljena je i kuhinjom sa svim dodatnim i potrebnim priborom za jelo i pripremu jela. Soba je idealna za goste kojima treba mir i svez vazduh.', 'terasa' => false, 'wifi' => true, 'tv' => true, 'klima' => false, 'cena_po_noci' => 5500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 105, 'broj_kreveta' => 2, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 1, 'pogled' => 'Pogled na planinu', 'opis' => 'Dvokrevetna soba sa terasom i brzim internetom. Ova soba nudi predivan pogled na planinu, koja svojom svezinom nudi gostima cistiji vazduh i uzivanje.', 'terasa' => true, 'wifi' => true, 'tv' => false, 'klima' => false, 'cena_po_noci' => 5000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 106, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 1, 'pogled' => 'Pogled na setaliste', 'opis' => 'Soba je idealna kako za mlade, tako i za porodicne ljude. Soba ima kompletnu opremu: klimu, terasu, brzi internet i televizor. Okrenuta je ka glavnoj ulici.', 'terasa' => true, 'wifi' => true, 'tv' => true, 'klima' => true, 'cena_po_noci' => 7500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 107, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 1, 'pogled' => 'Pogled na setaliste', 'opis' => 'Prostrana trokrevetna soba, sa jednim bracnim krevetom. Idealna je za porodicu sa decom. Ima terasu, sa pogledom na glavnu ulicu i brz internet.', 'terasa' => true, 'wifi' => true, 'tv' => false, 'klima' => false, 'cena_po_noci' => 4500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 108, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 1, 'pogled' => 'Pogled na setaliste', 'opis' => 'Trokrevetna soba, zamisljena kao apartman, ciji jedan deo sadrzi bracni krevet sa televizorom. U drugom delu sobe nalaze se dva lezaja i iz te prostorije se izlazi na teraasu. U sobi je dostupan i internet.', 'terasa' => true, 'wifi' => true, 'tv' => true, 'klima' => false, 'cena_po_noci' => 5500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 109, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 1, 'pogled' => 'Pogled na bazen', 'opis' => 'Mladalacki uredjena soba opremljena kuhinjskim priborom i neophodnim priborom za pripremu jela. Ima terasu, brz internet i klima uredjaj. U sobi se nalaze tri odvojena lezaja.', 'terasa' => true, 'wifi' => true, 'tv' => false, 'klima' => true, 'cena_po_noci' => 6500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 110, 'broj_kreveta' => 4, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 1, 'pogled' => 'Pogled na bazen', 'opis' => 'Soba idealna za mlade. Ima terasu sa pogledom na bazen i direktan izlaz na bazen. U sobi su za koriscenje dostupni internet, televizor i klima uredjaj.', 'terasa' => true, 'wifi' => true, 'tv' => true, 'klima' => true, 'cena_po_noci' => 7000]);
		$novaSoba->save();

		//2. sprat

		$novaSoba = new HotelskaSoba(['broj_sobe' => 201, 'broj_kreveta' => 1, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 2, 'pogled' => 'Pogled na more', 'opis' => 'Soba sa terasom koja gleda na more. Ima jedan bracni krevet, televizor i klima uredjaj, koji se ne doplacuju.', 'terasa' => true, 'wifi' => false, 'tv' => true, 'klima' => true, 'cena_po_noci' => 8500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 202, 'broj_kreveta' => 1, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 2, 'pogled' => 'Pogled na more', 'opis' => 'Kompletno opremljena soba sa pogledom na more i bracnim krevetom. Ima terasu, klima uredjaj, televizor i jako brz internet.', 'terasa' => true, 'wifi' => true, 'tv' => true, 'klima' => true, 'cena_po_noci' => 10000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 203, 'broj_kreveta' => 2, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 2, 'pogled' => 'Pogled na more', 'opis' => 'Dvokrevetna soba koju obozavaju kako mladi ljudi zeljni provoda, tako i parovi koji bi hteli malo mira. Ima internet i klima uredjaj na raspolaganju, bez ikakve dodatne doplate.', 'terasa' => false, 'wifi' => true, 'tv' => false, 'klima' => true, 'cena_po_noci' => 8000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 204, 'broj_kreveta' => 2, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 2, 'pogled' => 'Pogled na planinu', 'opis' => 'Soba koja vam garantuje mir i tisinu. Ima terasu, klima uredjaj i televizor. Okrenuta je ka planini.', 'terasa' => true, 'wifi' => false, 'tv' => true, 'klima' => true, 'cena_po_noci' => 8000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 205, 'broj_kreveta' => 2, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 2, 'pogled' => 'Pogled na planinu', 'opis' => 'Soba sa dva odvojena lezaja, televizorom i klima uredjajem, nudi vam odmor. Pogled na planinu, naravno, doprinosi tom miru, a svez vazduh doprinosi vasem dobrom raspolozenju.', 'terasa' => false, 'wifi' => false, 'tv' => true, 'klima' => true, 'cena_po_noci' => 7500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 206, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 2, 'pogled' => 'Pogled na setaliste', 'opis' => 'Soba koju mladi najcesce biraju. U sobi se nalaze tri medjusobno odvojena lezaja i kompletno opremljena kuhinja. Soba ima terasu, klima uredjaj, brz internet i televizor. Kompletno je opremljena i namestena.', 'terasa' => true, 'wifi' => true, 'tv' => true, 'klima' => true, 'cena_po_noci' => 9000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 207, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 2, 'pogled' => 'Pogled na setaliste', 'opis' => 'Trokrevetna soba orijentisana ka glavnoj ulici omogucava Vam da vec sa terase istrazite grad i njegove delove koje vredi obici. Pored terase, soba je opremljena i televizorom.', 'terasa' => true, 'wifi' => false, 'tv' => true, 'klima' => false, 'cena_po_noci' => 5500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 208, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 2, 'pogled' => 'Pogled na setaliste', 'opis' => 'Soba za mlade porodicne ljude, koja je organizovana tako da se u jednom njenom delu nalazi bracni krevet sa klima uredjajem i izlazom na terasu, a u drugom delu se nalaze dva odvojena lezaja i televizorom.', 'terasa' => true, 'wifi' => false, 'tv' => true, 'klima' => true, 'cena_po_noci' => 7500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 209, 'broj_kreveta' => 4, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 2, 'pogled' => 'Pogled na bazen', 'opis' => 'Jedna od najpovoljnijih soba za vece drustvo. Ima klima uredjaj i terasu koja gleda na bazen. Pored klima uredjaja, u sobi imate jako brz internet.', 'terasa' => true, 'wifi' => true, 'tv' => false, 'klima' => false, 'cena_po_noci' => 5500]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 210, 'broj_kreveta' => 4, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 2, 'pogled' => 'Pogled na bazen', 'opis' => 'Soba organizovana kao apartman, sa dva odvojena dela, gde se u oba dela nalazi po jedan bracni krevet. U jednom delu nalazi se televizor, a u drugom klima uredjaj.', 'terasa' => false, 'wifi' => false, 'tv' => true, 'klima' => true, 'cena_po_noci' => 6500]);
		$novaSoba->save();



		//3.sprat

		$novaSoba = new HotelskaSoba(['broj_sobe' => 301, 'broj_kreveta' => 1, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 3, 'pogled' => 'Pogled na more', 'opis' => 'Apartman, koji se sastoji od tri dela. U jednom delu nalazi se bracni krevet sa klima uredjajem. Drugi deo organizovan je kao dnevni boravak, u kome se nalazi televizor. A treci deo zamisljen je kao kuhinja.', 'terasa' => false, 'wifi' => false, 'tv' => true, 'klima' => true, 'cena_po_noci' => 16000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 302, 'broj_kreveta' => 1, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 3, 'pogled' => 'Pogled na more', 'opis' => 'Apartman koji je idealan za mlade parove. Apartman ima terasu sa pogledom na more, veliki deo za dnevni boravak, iz kojeg se izlazi na terasu, kompletno opremljenu kuhinju i spavacu sobu. Spavaca soba opremljena je velikim bracnim krevetom, televizorom i klima uredjajem, a u celom apartmanu nudimo Vam brz internet.', 'terasa' => true, 'wifi' => true, 'tv' => true, 'klima' => true, 'cena_po_noci' => 20000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 303, 'broj_kreveta' => 2, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 3, 'pogled' => 'Pogled na planinu', 'opis' => 'Apartman namenjen za porodice. Sastoji se od spavace sobe u kojoj se nalazi bracni krevet i izlaz na terasu i od sobe u kojoj se nalazi jedan lezaj. Prostorija sa jednim lezajem opremljena je televizorom i klima uredjajem.', 'terasa' => true, 'wifi' => false, 'tv' => true, 'klima' => true, 'cena_po_noci' => 16000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 304, 'broj_kreveta' => 2, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 3, 'pogled' => 'Pogled na planinu', 'opis' => 'Apartman za porodicne ljude, kojima je potreban odmor od buke. Soba je izolovana od glasne muzike koje mogu doci iz ostalih soba i apartmana, a pored prostorije sa bracnim krevetom i pomocnim lezajem, ima i prostoriju zamisljenu kao dnevni boravak, sa televizorom i klima uredjajem. U celom apartmanu dostupan je brz internet.', 'terasa' => false, 'wifi' => true, 'tv' => true, 'klima' => true, 'cena_po_noci' => 18000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 305, 'broj_kreveta' => 2, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 3, 'pogled' => 'Pogled na planinu', 'opis' => 'Apartman koji se sastoji od dve odvojene prostorije. U svakoj od prostorija nalazi se po jedan bracni krevet, a obe prostorije imaju izlaz na veliku terasu, koja ih spaja. U dnevnom boravku nalazi se televizor.', 'terasa' => true, 'wifi' => false, 'tv' => true, 'klima' => false, 'cena_po_noci' => 12000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 306, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 3, 'pogled' => 'Pogled na setaliste', 'opis' => 'Apartman namenjen za vece porodice, koja pored prostorije u kojoj se nalaze veliki bracni krevet i klima uredjaj, ima odvojenu prostoriju sa dva odvojena lezaja i televizorom. U dnevnom boravku nalazi se klima uredjaj i iz ove prostorije postoji izlaz na terasu.', 'terasa' => true, 'wifi' => false, 'tv' => true, 'klima' => true, 'cena_po_noci' => 15000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 307, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Slobodna', 'sprat' => 3, 'pogled' => 'Pogled na setaliste', 'opis' => 'Apartman sa tri odvojene prostorije, gde se u svakoj od prostorija nalazi po jedan lezaj. U apartmanu je dostupan internet.', 'terasa' => false, 'wifi' => true, 'tv' => false, 'klima' => false, 'cena_po_noci' => 11000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 308, 'broj_kreveta' => 3, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 3, 'pogled' => 'Pogled na setaliste', 'opis' => 'Apartman okrenut ka glavnoj ulici, sa terasom i dve odvojene prostorije, koje terasa spaja. U jednoj prostoriji nalaze se dva odvojena lezaja, a u drugoj jedan lezaj.', 'terasa' => true, 'wifi' => false, 'tv' => false, 'klima' => false, 'cena_po_noci' => 9000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 309, 'broj_kreveta' => 4, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 3, 'pogled' => 'Pogled na bazen', 'opis' => 'Apartman koji mladi najvise vole! U apartmanu se pored odvojene prostorije za kuhinju iz koje se izlazi na terasu koja gleda na bazen, ima dve odvojene prostorije. U obe prostorije nalaze se po dva odvojena lezaja. U jednoj od njih nalazi se klima uredjaj. Glavna prostorija opremljena je televizorom. U celom apartmanu je dostupan internet.', 'terasa' => true, 'wifi' => true, 'tv' => true, 'klima' => true, 'cena_po_noci' => 17000]);
		$novaSoba->save();

		$novaSoba = new HotelskaSoba(['broj_sobe' => 310, 'broj_kreveta' => 4, 'slobodna_zauzeta' => 'Zauzeta', 'sprat' => 3, 'pogled' => 'Pogled na bazen', 'opis' => 'Apartman koji je pogodan i za dve porodice. U obe prostorije nalaze se po jedan bracni krevet i po jedan odvojen lezaj. Apartman je opremljen terasom, brzim internetom, televizorom i klima uredjajem. Klima, televizor i izlaz na terasu se nalaze u glavnoj prostoriji, a internet je dostupan u celom apartmanu.', 'terasa' => true, 'wifi' => true, 'tv' => true, 'klima' => true, 'cena_po_noci' => 17000]);
		$novaSoba->save();

		//KREIRANJE SOBAA





		//KREIRANJE STOLOVA KAFICA
		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 1, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 2, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 3, 'broj_mesta' => 3, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 4, 'broj_mesta' => 2, 'racun' => 1000, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 5, 'broj_mesta' => 3, 'racun' => 1500, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 6, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 7, 'broj_mesta' => 5, 'racun' => 5210, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 8, 'broj_mesta' => 2, 'racun' => 700, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 9, 'broj_mesta' => 4, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 10, 'broj_mesta' => 4, 'racun' => 2000, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();


		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 11, 'broj_mesta' => 3, 'racun' => 1780, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 12, 'broj_mesta' => 2, 'racun' => 1000, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 13, 'broj_mesta' => 5, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 14, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 15, 'broj_mesta' => 4, 'racun' => 150, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 16, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 17, 'broj_mesta' => 3, 'racun' => 1270, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 18, 'broj_mesta' => 4, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 19, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 20, 'broj_mesta' => 5, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();


		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 21, 'broj_mesta' => 2, 'racun' => 300, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 22, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 23, 'broj_mesta' => 5, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 24, 'broj_mesta' => 3, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 25, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 26, 'broj_mesta' => 2, 'racun' => 1750, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 27, 'broj_mesta' => 2, 'racun' => 3000, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 28, 'broj_mesta' => 5, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 29, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Kafic', 'broj_stola' => 30, 'broj_mesta' => 4, 'racun' => 1500, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();





		//KREIRANJE STOLOVA RESTORANA

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 1, 'broj_mesta' => 2, 'racun' => 1540, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 2, 'broj_mesta' => 3, 'racun' => 8700, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 3, 'broj_mesta' => 7, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 4, 'broj_mesta' => 9, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 5, 'broj_mesta' => 5, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 6, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 7, 'broj_mesta' => 6, 'racun' => 8450, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 8, 'broj_mesta' => 4, 'racun' => 2700, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 9, 'broj_mesta' => 2, 'racun' => 1500, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 10, 'broj_mesta' => 10, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();


		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 11, 'broj_mesta' => 3, 'racun' => 3640, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 12, 'broj_mesta' => 5, 'racun' => 1750, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 13, 'broj_mesta' => 6, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 14, 'broj_mesta' => 2, 'racun' => 5100, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 15, 'broj_mesta' => 4, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 16, 'broj_mesta' => 4, 'racun' => 3000, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 17, 'broj_mesta' => 8, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 18, 'broj_mesta' => 2, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 19, 'broj_mesta' => 8, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 20, 'broj_mesta' => 5, 'racun' => 12000, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();


		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 21, 'broj_mesta' => 10, 'racun' => 8000, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 22, 'broj_mesta' => 10, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 23, 'broj_mesta' => 9, 'racun' => 6000, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 24, 'broj_mesta' => 4, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 25, 'broj_mesta' => 7, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 26, 'broj_mesta' => 8, 'racun' => 12000, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 27, 'broj_mesta' => 7, 'racun' => 7800, 'slobodan_ili_zauzet' => 'Zauzet']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 28, 'broj_mesta' => 5, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 29, 'broj_mesta' => 6, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();

		$noviSto = new Restoran(['kafic_ili_restoran' => 'Restoran', 'broj_stola' => 30, 'broj_mesta' => 8, 'racun' => 0, 'slobodan_ili_zauzet' => 'Slobodan']);
		$noviSto->save();





		//KREIRANJE USLUGA

		$novaUsluga = new Usluge(['naziv_usluge' => 'Fitnes centar', 'osnovna_ili_dodatna_usluga' => 'Dodatna usluga', 'cena' => 2000]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Transfer od/do aerodroma', 'osnovna_ili_dodatna_usluga' => 'Osnovna usluga', 'cena' => 1000]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Spa i Welness centar', 'osnovna_ili_dodatna_usluga' => 'Dodatna usluga', 'cena' => 3500]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Bazen', 'osnovna_ili_dodatna_usluga' => 'Osnovna usluga', 'cena' => 500]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Teretana', 'osnovna_ili_dodatna_usluga' => 'Dodatna usluga', 'cena' => 1500]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Mini bar', 'osnovna_ili_dodatna_usluga' => 'Osnovna usluga', 'cena' => 9000]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Iznajmljivanje automobila', 'osnovna_ili_dodatna_usluga' => 'Dodatna usluga', 'cena' => 4000]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Obilazak znamenitosti', 'osnovna_ili_dodatna_usluga' => 'Dodatna usluga', 'cena' => 4000]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Parking', 'osnovna_ili_dodatna_usluga' => 'Osnovna usluga', 'cena' => 700]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Room service', 'osnovna_ili_dodatna_usluga' => 'Osnovna usluga', 'cena' => 500]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Usluzno pranje i peglanje vesa', 'osnovna_ili_dodatna_usluga' => 'Dodatna usluga', 'cena' => 1500]);
		$novaUsluga->save();

		$novaUsluga = new Usluge(['naziv_usluge' => 'Organizovani izleti', 'osnovna_ili_dodatna_usluga' => 'Dodatna usluga', 'cena' => 7000]);
		$novaUsluga->save();




		//KREIRANJE JELOVNIKA
		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Omlet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Omlet sa povrcem', 'cena' => 220, 'opis' => '3 jaja, puter, praziluk, paprika, sampinjoni, ceri paradajz, miks zelenih salata']);
		$novoJelo->save();


		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Omlet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Omlet sa sunkom i kackavaljem', 'cena' => 240, 'opis' => '3 jaja, puter, praska sunka, kravlji kackavalj, krem sir, ceri paradajz, miks zelenih salata']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Omlet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Omlet sa slaninom i kackavaljem', 'cena' => 240, 'opis' => '3 jaja, puter, slanina, kravlji kackavalj, krem sir, ceri paradajz, miks zelenih salata']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Omlet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Omlet sa gravlaksom od lososa', 'cena' => 290, 'opis' => '3 jaja, puter, vlasac, krem sir, miks zelenih salata, gravlaks od lososa']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Omlet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Omlet sa njeguskom prsutom', 'cena' => 290, 'opis' => '3 jaja, puter, njeguska prsuta, vlasac, krem sir, miks zelenih salata']);
		$novoJelo->save();


		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jaja', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Jaja sa slaninom', 'cena' => 240, 'opis' => '3 jaja, slanina, projica sa sirom, sirni namaz, paradajz, ceri paradajz, miks zelenih salata']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jaja', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Jaja sa dimljenom kobasicom', 'cena' => 340, 'opis' => '3 jaja, dimljena kobasica, projica sa sirom, ajvar, kajmak, krastavcici, govedja prsuta']);
		$novoJelo->save();


		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sendvic', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Komplet lepinja', 'cena' => 240, 'opis' => 'Jaja, lepinja, kajmak, kravlji kackavalj, govedja prsuta, kiseli krastavcici']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sendvic', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Posni sendvic sa gravlaksom od lososa', 'cena' => 250, 'opis' => 'Kroasan, gravlaks od lososa, celer, cvekla, sargarepa, jabuka, kupus, miks zelenih salata']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sendvic', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Kroasan', 'cena' => 120, 'opis' => 'Puter kroasan']);
		$novoJelo->save();



		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Hladno predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Daska Nis', 'cena' => 690, 'opis' => 'Peglana pirotska kobasica, duvan cvarci, ajvar, punjena babura sa sirom, 260 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Hladno predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Daska Stambolijski', 'cena' => 680, 'opis' => 'Njeguska prsuta, govedja prsuta, panceta, marinirano povrce, 260 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Hladno predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Daska Balkanskih sireva', 'cena' => 610, 'opis' => 'Kravlji sir, ovciji sir, kravlji kackavalj, ovciji kackavalj, masline, 260 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Hladno predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Tartar biftek', 'cena' => 1450, 'opis' => 'Najfinije junece meso, zacinjeno sa 13 vrsta zacina, sa puterom i tostiranim hlebom (krekerima) 120 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Hladno predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Tartar losos', 'cena' => 890, 'opis' => 'Losos egzoticno zacinje na guhkamoli sa konkase paradajzom i pireom od limuna sa tostiranim hlebom (krekerima) 120 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Hladno predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Rozbif', 'cena' => 810, 'opis' => 'Zacinjeni juneci biftek serviran na rukoli sa sosom od tartufa i krekerima 120 grama']);
		$novoJelo->save();



		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Toplo predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Rizoto sa povrcem', 'cena' => 490, 'opis' => 'Miks povrca, pirinac paradajz sos, puter, parmezan 330 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Toplo predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Rizoto sa biftekom', 'cena' => 810, 'opis' => 'Juneci biftek, pirinac, demiglas sos, puter, parmezan 330grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Toplo predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Pirotski djubek', 'cena' => 550, 'opis' => 'Grilovani sir na miksu zelenih salata, 250 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Toplo predjelo', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Mirocki sir skripovac', 'cena' => 610, 'opis' => '200 grama']);
		$novoJelo->save();



		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Supa i corba', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Teleca corba na staroplaninski nacin', 'cena' => 350, 'opis' => 'Teleca corba, crni luk, sargarepa, celer, pavlaka, jaje i zacini, 300 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Supa i corba', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Govedja supa sa povrcem', 'cena' => 210, 'opis' => 'Sargarepa, celer i domaca rezanca, 300 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Supa i corba', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Potaz dana', 'cena' => 240, 'opis' => 'Sargarepa, celer, domaca rezanca 300 mililitra']);
		$novoJelo->save();




		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Riblji specijalitet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Tuna stejk sa ili bez korice od susama na pireu od celera sa zacinskim sosom', 'cena' => 2050, 'opis' => 'Tuna stejk 300 grama, pire od celera 120 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Riblji specijalitet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'File lososa sa ili bez hrskave korice od korijandera i bibera sa rizotom', 'cena' => 1490, 'opis' => 'Losos 300 grama, pire 120 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Riblji specijalitet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'File orade sa povrcem na puteru ili rizotom od graska', 'cena' => 1290, 'opis' => 'Filet orade 200 grama, povrce 180 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Riblji specijalitet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'File brancina sa povrcem na puteru ili rizotom od graska', 'cena' => 1290, 'opis' => 'File brancina 200 grama, povrce 180 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Riblji specijalitet', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Gambori na zaru sa crnim rizotom', 'cena' => 1680, 'opis' => 'Gambori 270 grama, rizoto 120 grama']);
		$novoJelo->save();



		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Posna i vegeterijanska jela', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Povrce pripremljeno Sous Vide tehnologijom kuvanja,zavrsavano na puteru', 'cena' => 250, 'opis' => '180 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Posna i vegeterijanska jela', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Grilovano povrce', 'cena' => 250, 'opis' => '300 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Posna i vegeterijanska jela', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Zacinski krompir iz rerne', 'cena' => 190, 'opis' => '300 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Posna i vegeterijanska jela', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Rizoto sa povrcem', 'cena' => 490, 'opis' => 'Miks povrca, pirinac paradajz sos, puter, parmezan 330 grama, 330 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Posna i vegeterijanska jela', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Kinoa salata', 'cena' => 540, 'opis' => 'Kinoa, jabuka, paprika, crveni luk, rukola, limun dresing, 250 grama']);
		$novoJelo->save();



		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Preporuke sefa kuhinje', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Teletina Sous Vide', 'cena' => 1490, 'opis' => 'Mlada teletina pripremljena Sous Vide tehnologijom kuvanja na niskoj temperaturi vode u sopstvenim sokovima, zapecena na puteru na podlozi od batat pirea sa krancom od badema i demiglas sosom, 400 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Preporuke sefa kuhinje', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Gambori Pil Pil', 'cena' => 999, 'opis' => 'Ocisceni gambori przeni u vrelom maslinovom ulju sa belim lukom i dimljenom tucanom paprikom, servirani sa prepecenim hlebom']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Preporuke sefa kuhinje', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Sporo kuvane jagnjece grudi', 'cena' => 1490, 'opis' => 'Jagnjeci biftek obavijen grudima sporo kuvan u sopstvenom soku na niskoj temperaturi vode, zapecen na puteru sa jagnjecim sosom na bazi anisa, krancom od lesnika na pireu od celera, 400 grama']);
		$novoJelo->save();



		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jela po porudzbini', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Natur biftek', 'cena' => 1850, 'opis' => 'Posebno odabrani juneci odrezak sa povrcem na puteru, 300 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jela po porudzbini', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Peper stejk', 'cena' => 2090, 'opis' => 'Posebno odabrani juneci odrezak sa prelivom od tri vrste bibera, 350 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jela po porudzbini', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Biftek “Stara planina”', 'cena' => 2090, 'opis' => 'Posebno odabrani juneci odrezak sa sosom od suvih i svezih pecuraka sa Stare planine, 350 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jela po porudzbini', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Biftek u ulju', 'cena' => 2250, 'opis' => 'Odlezali juneci biftek u hladno cedjenom ekstra devicanskom maslinovom ulju sa zacinskim biljem, 350 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jela po porudzbini', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Odrezak juneceg filea kako ga Filip voli', 'cena' => 1890, 'opis' => 'Marinirani juneci file na salati od rukole i ceri paradajza, 350 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jela po porudzbini', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Karadjordjeva snicla', 'cena' => 890, 'opis' => '400 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jela po porudzbini', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Pileci file u sosu od parmezana', 'cena' => 790, 'opis' => '350 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jela po porudzbini', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Pileci file', 'cena' => 650, 'opis' => 'Pripremljen Sous vide tehnologijom kuvanja sa povrcem na puteru, 300 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jela po porudzbini', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'cureci file', 'cena' => 890, 'opis' => 'Pripremljen Sous vide tehnologijom kuvanja, sa pireom od kukuruz secerca i egzoticnim sosom, 300 grama']);
		$novoJelo->save();




		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Specijalitet kuce', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Jagnjetina ispod saca', 'cena' => 1290, 'opis' => 'Jagnjetina 300 gr, krompir iz saca 120 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Specijalitet kuce', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Teletina ispod saca', 'cena' => 1190, 'opis' => 'Teletina 300 gr, krompir iz saca 120 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Specijalitet kuce', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Svinjski vrat u medovini', 'cena' => 890, 'opis' => 'Pecenje od svinjskog vrata sa medom i pivom, 300 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Specijalitet kuce', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Dimljena butkica', 'cena' => 990, 'opis' => 'U sosu od kajmaka i rena sa zacinskim krompirom 350 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Specijalitet kuce', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Pljeskavica Stambolijski', 'cena' => 870, 'opis' => 'Junece meso 330gr, duvan cvarci 50gr, kravlji kackavalj 100 grama, prazi luk']);
		$novoJelo->save();



		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jelo sa rostilja', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Punjeni svinjski file', 'cena' => 880, 'opis' => 'Punjeni svinjski file sa kackavaljem i slaninom 300 grama, krompir 100 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jelo sa rostilja', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Pljeskavica', 'cena' => 440, 'opis' => 'Junece meso 300 grama, krompir 100 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jelo sa rostilja', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'cevapi', 'cena' => 440, 'opis' => 'Junece meso 300 grama, krompir 100 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Jelo sa rostilja', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Leskovacki ustipak', 'cena' => 590, 'opis' => 'Junece meso 300 grama, krompir 100 grama']);
		$novoJelo->save();



		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Prilog', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Povrce pripremljeno Sous Vide tehnologijom kuvanja, zavrsavano na puteru', 'cena' => 250, 'opis' => '180 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Prilog', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Grilovano povrce', 'cena' => 250, 'opis' => '300 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Prilog', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Zacinski krompir iz rerne', 'cena' => 190, 'opis' => '300 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Prilog', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Rizoto sa povrcem', 'cena' => 490, 'opis' => 'Miks povrca, pirinac paradajz sos, puter, parmezan 330 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Prilog', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Pomfrit', 'cena' => 190, 'opis' => '250 grama']);
		$novoJelo->save();




		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Moravska', 'cena' => 290, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Srpska', 'cena' => 250, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'sopska', 'cena' => 290, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Grcka', 'cena' => 450, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Kupus', 'cena' => 190, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Vitaminska salata', 'cena' => 290, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Miks zelenih salata', 'cena' => 290, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Belolucana', 'cena' => 90, 'opis' => '1 komad']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Papricica u ulju', 'cena' => 290, 'opis' => '1 komad']);
		$novoJelo->save();




		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata obrok', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Biftek salata', 'cena' => 990, 'opis' => 'Obrok salata sa mariniranim junecim biftekom, miksom zelenih salata, rukolom, bif dresingom i konkaseom, 360 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata obrok', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Salata sa grilovanom piletinom', 'cena' => 690, 'opis' => 'Salata sa grilovanom piletinom na miksu zelenih salata, susenim paradajzom, hrskavom slaninom i parmezanom, 340 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata obrok', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Salata "Balkan" - za dve osobe', 'cena' => 690, 'opis' => 'Cvekla sa kozijim sirom i orasima na misku zelenih salata, 260 grama']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Salata obrok', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Kinoa salata', 'cena' => 540, 'opis' => 'Kinoa, jabuka, paprika, crveni luk, rukola, limun dresing, 250 grama']);
		$novoJelo->save();




		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Ljubav na prvi pogled', 'cena' => 340, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Gospodin cokoladni', 'cena' => 340, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Lepi Dzoni', 'cena' => 320, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Trilece', 'cena' => 310, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Bledska krempita', 'cena' => 270, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Limun tart', 'cena' => 290, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Nasa torta', 'cena' => 310, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Danilov izbor', 'cena' => 340, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Baklava na Sasin nacin', 'cena' => 240, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Palacinke', 'cena' => 240, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Snezni ananas', 'cena' => 370, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Nasa mala poslasticara', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Vegano kolac', 'cena' => 350, 'opis' => '']);
		$novoJelo->save();




		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Deciji meni', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Jagodica bobica', 'cena' => 370, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Deciji meni', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Pera zdera', 'cena' => 290, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Deciji meni', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Patak Daca', 'cena' => 250, 'opis' => '']);
		$novoJelo->save();

		$novoJelo = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Deciji meni', 'jelo_ili_pice' => 'Jelo', 'naziv_jela_ili_pica' => 'Dusko Dugousko', 'cena' => 290, 'opis' => '']);
		$novoJelo->save();

	

		//Pica

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Aperitiv', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Martini bjanko draj', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Aperitiv', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Martini bjanko', 'cena' => 190, 'opis' => '']);
	$novoPice->save();
	
	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Aperitiv', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Martini roso', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Aperitiv', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kampari', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Aperitiv', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Jegermajster', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Aperitiv', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Gorki list', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Aperitiv', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Ramacoti', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Aperitiv', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Uzo', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Dzin', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Bombaj safir', 'cena' => 250, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Viski', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Dzoni voker red', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Viski', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Dzoni voker blek', 'cena' => 390, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Viski', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Dzoni voker gold', 'cena' => 510, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Viski', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Dzejmison', 'cena' => 220, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Viski', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Dejmison 12g.', 'cena' => 390, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Viski', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Civas 12g.', 'cena' => 490, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Viski', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Civas rojal salut 21g.', 'cena' => 1550, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Burbon', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'For rouzes', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Viski', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Dzek Denijels', 'cena' => 290, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Viski', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Dzek Denijels singl barel', 'cena' => 520, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Konjak', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kurvazije V.S.O.P.', 'cena' => 400, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Konjak', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kurvazije IKS.O.', 'cena' => 1550, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Konjak', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Henesi fajn konjak', 'cena' => 450, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Konjak', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Henesi IKS.O.', 'cena' => 1950, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Brendi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Vinjak 5', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Brendi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Vinjak IKS.O.', 'cena' => 650, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Brendi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Metaksa 7*', 'cena' => 290, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Votka', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Absolut', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Votka', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Grej-gus', 'cena' => 490, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Votka', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Ivan Grozni', 'cena' => 520, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Tekila', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Olmeka bjanko', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rum', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Bakardi superior', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rum', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Havana klub 7', 'cena' => 350, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rum', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Havana klub 3', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Stambolijska viljamovka', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Stambolijska loza', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Stambolijska dunja', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Stambolijska jabukovaca', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Medova rakija Jezdic', 'cena' => 250, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Loza 13.jul', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Stomaklija', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Lincura', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Mastika', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Stara sokolova', 'cena' => 280, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Stara sokolova 12g.', 'cena' => 540, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Dunja Stara pesma 5g.', 'cena' => 420, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Gorda sljiva', 'cena' => 380, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Rakija', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sljiva Jelicki dukat', 'cena' => 250, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Pivo', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Malo toceno', 'cena' => 220, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Pivo', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Veliko toceno', 'cena' => 280, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Pivo', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Pilsner Urkel Plzen', 'cena' => 280, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Pivo', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Hajniken', 'cena' => 320, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Pivo', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Nisko', 'cena' => 195, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Pivo', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Paulajner - svetlo pivo', 'cena' => 340, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Pivo', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kozel - tamno pivo', 'cena' => 340, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Likeri', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Aperol', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Likeri', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Arcers', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Likeri', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Amareto', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Likeri', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Malibu', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Likeri', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Limoncelo', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Likeri', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Bejlis', 'cena' => 240, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Gazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Koka kola', 'cena' => 180, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Gazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Koka kola zero', 'cena' => 180, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Gazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kokta', 'cena' => 180, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Gazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Fanta', 'cena' => 180, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Gazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sprajt', 'cena' => 180, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Gazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sveps limun', 'cena' => 180, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Gazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sveps tonik', 'cena' => 180, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Negazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Domaci kasasti sok od visnje', 'cena' => 320, 'opis' => '100% priroban, bez secera']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Negazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Domaci kasasti sok od maline', 'cena' => 320, 'opis' => '100% priroban, bez secera']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Negazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Domaci kasasti sok od kupine', 'cena' => 320, 'opis' => '100% priroban, bez secera']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Negazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sok od jagode', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Negazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sok od jabuke', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Negazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sok od narandze', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Negazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sok od breskve', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Negazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sok od borovnice', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Negazirani sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Sok od kajsije', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Limunada', 'cena' => 180, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Cedjena narandza', 'cena' => 230, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Cedjeni grepfurt', 'cena' => 240, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Cedjena jabuka', 'cena' => 220, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Cedjeni ananas', 'cena' => 310, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Vitamin C limun', 'cena' => 320, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Vitamin C narandza', 'cena' => 320, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Vitamin C lgrejpfurt', 'cena' => 320, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Vitamin C med', 'cena' => 320, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Imuno miks narandza', 'cena' => 320, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Imuno miks sargarepa', 'cena' => 320, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Imuno miks ananas', 'cena' => 320, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Sveze cedjeni sokovi', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Imuno miks djumbir', 'cena' => 320, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Mineralne vode', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Akva viva 0,33l', 'cena' => 120, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Mineralne vode', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Akva viva 0,75l', 'cena' => 210, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Mineralne vode', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Knjaz Milos 0,25l', 'cena' => 120, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Mineralne vode', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Knjaz Milos 0,75l', 'cena' => 210, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Mineralne vode', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Soda voda 0,75l', 'cena' => 150, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Energetski napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Red Bull', 'cena' => 300, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Bezalkoholni kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Virdzin mohito', 'cena' => 300, 'opis' => 'soda voda, limeta, nana,secer, vocni pire']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Bezalkoholni kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Enegizer', 'cena' => 300, 'opis' => 'narandza sok, limun, limeta, vanila sirup, grenadin']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Polje jagoda', 'cena' => 550, 'opis' => 'absolut votka, pire od jagoda, cedjena limeta, nana, jagoda, Proseko']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Mohito', 'cena' => 420, 'opis' => 'Havana klub 3, soda, zuti secer, limeta, nana']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Margarita', 'cena' => 420, 'opis' => 'Olmeka bjanko, Kontro, cedjena limeta, so']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Martini', 'cena' => 520, 'opis' => 'Bombaj safir, Martini, masline']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Negroni', 'cena' => 520, 'opis' => 'Bombaj safir, Kampari, Martini roso']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kosmopolitan', 'cena' => 450, 'opis' => 'Operol, Proseko, soda voda, kolut narandze']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Seks on d bic', 'cena' => 450, 'opis' => 'Absolut votka, Arcers, narandza sok, brusnica sok']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Aperol spric', 'cena' => 500, 'opis' => 'Aperol, Proseko, soda voda, narandza']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kokteli', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Mai tai', 'cena' => 580, 'opis' => 'Havana klub 3, Havana klub 7, Kontro, Amereto, cedjena limeta, narandza sok, ananas sok, nana']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Domaca kafa', 'cena' => 100, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Espreso', 'cena' => 130, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Dupli espreso', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Makijato', 'cena' => 140, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kapucino', 'cena' => 150, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Fredo kafa', 'cena' => 210, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kafe late', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Nes kafa', 'cena' => 160, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Topla cokolada', 'cena' => 180, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Caj Ronfeld', 'cena' => 140, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Caj set Ronfeld', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kuvana rakija', 'cena' => 190, 'opis' => '']);
	$novoPice->save();

	$novoPice = new CenovnikRestorana(['vrsta_jela_ili_pica' => 'Kafe i topli napici', 'jelo_ili_pice' => 'Pice', 'naziv_jela_ili_pica' => 'Kuvano vino', 'cena' => 210, 'opis' => '']);
	$novoPice->save();


	$admin= new Korisnik(['ime'=>"", 'prezime'=>"", 'email_adresa'=>"admin123@gmail.com", 'lozinka'=>"JaSamAdmin123!", 'tip_korisnika'=>"Admin", 'tip_radnika'=>""]);

	$admin->save();
	
	}

}
