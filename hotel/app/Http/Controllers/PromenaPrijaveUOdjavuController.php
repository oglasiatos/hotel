<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromenaPrijaveUOdjavuController extends Controller
{
    public function proveriDaLiJeKorisnikUlogovan(Request $request)
    {
    	$json = $_POST;
    	if($request->session()->has('hotel_korisnik'))
    	{
    		return response()->json(['Status' => "Korisnik je ulogovan!"]);
    	}
    	else
    	{
    		return response()->json(['Status' => "Korisnik nije ulogovan!"]);
    	}
    }

    public function odjavaKorisnika(Request $request)
    {
    	$json = $_POST;
    	$request->session()->forget('hotel_korisnik');
        return response()->json(['Status'=>"Uspesna odjava!"]);
    }
}
