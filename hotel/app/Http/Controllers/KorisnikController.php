<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Korisnik;

class KorisnikController extends Controller
{
    //Funkcija koja se koristi za registraciju novih korisnika, ali ne i radnika
    //Funkciji se prosledjuju email adresa, ime, prezime i lozinka novog korisnika
    public function registracijaKorisnika(Request $request)
    {
    	$json = $_POST;
    	if(Korisnik::whereEmail_adresa($json['email_adresa'])->count()===0)
    	{
    		$noviKorisnik = new Korisnik(['ime' => $json['ime'], 'prezime' => $json['prezime'], 'email_adresa' => $json['email_adresa'], 'lozinka' => $json['lozinka'], 'tip_korisnika' => "Korisnik"]);
    		$noviKorisnik->save();
            $request->session()->put('hotel_korisnik',$json['email_adresa']);
    		return response()->json(['Status'=> "Uspesna registracija novog korisnika!"]);
    	}
    	else
    	{
    		return response()->json(['Status'=> "Neuspesna registracija novog korisnika!"]);
    	}
    }

    //Funkcija koju admin koristi prilikom zaposljavanja novog radnika
    //Funkciji se prosledjuju ime, prezime, email adresa, lozinka radnika, kao i tip posla koji ce da obavlja (radnik na recepciji, u kaficu ili u restoranu)

    public function zaposljavanjeNovogRadnika(Request $request)
    {
        $json = $_POST;
        if(Korisnik::where('email_adresa', $json['email_adresa'])->count()===0)
        {
            $noviKorisnik = new Korisnik(['ime' => $json['ime'], 'prezime' => $json['prezime'], 'email_adresa' => $json['email_adresa'], 'lozinka' => $json['lozinka'], 'tip_korisnika' => "Radnik", 'tip_radnika' => $json['tip_radnika']]);
            $noviKorisnik->save();
            return response()->json(['Status' => "Uspesno zaposljen radnik!"]);
        }
        return response()->json(['Status' => "Neuspesno zaposljen radnik!"]);
    }


    //Funkcija koju admin koristi za otpustanje radnika
    //Funkciji se prosledjuje email adresa radnika

    public function otpustanjeRadnika(Request $request)
    {
        $json = $_POST;
        Korisnik::destroy(intval($json['id']));
        return response()->json(['Status' => "Uspesno otpusten radnik!"]);
    }


    //Funkcija kojom se menja lozinka korisniku
    //Funkciji se prosledjuje email adresa korisnika i njegova nova lozinka
    public function izmeniLozinkuKorisniku(Request $request)
    {
        $json = $_POST;
        if(Korisnik::where('email_adresa', $json['email_adresa'])->count() > 0)
        {
            $korisnik = Korisnik::where('email_adresa', $json['email_adresa'])->get();
            $korisnik[0]->lozinka = $json['lozinka'];
            $korisnik[0]->save();
            return response()->json(['Status' => "Uspesna izmena lozinke!"]);
        }
        return response()->json(['Status' => "Neuspesna izmena lozinke!"]);
    }


    //Funkcija koju i radnici i korisnici koriste za prijavljivanje
    public function prijavljivanjeKorisnika(Request $request)
    {
    	$json = $_POST;
    	if(Korisnik::whereEmail_adresa($json['email_adresa'])->count()!=0)
    	{
    		if(Korisnik::whereEmail_adresa($json['email_adresa'])->first()['lozinka'] === $json['lozinka'])
    		{
                if(Korisnik::whereEmail_adresa($json['email_adresa'])->first()['tip_korisnika'] === "Korisnik")
                {
                    $request->session()->put('hotel_korisnik',$json['email_adresa']);
                    return response()->json(['Status' => "Uspesno prijavljivanje!"]);
                }
                else if(Korisnik::whereEmail_adresa($json['email_adresa'])->first()['tip_korisnika'] === "Admin")
                {
                    $request->session()->put('hotel_korisnik',$json['email_adresa']);
                    return response()->json(['Status' => "Uspesno prijavljivanje admina!"]);
                }
                else
                {
                    if(Korisnik::whereEmail_adresa($json['email_adresa'])->first()['tip_radnika'] === "Recepcioner")
                    {
                        $request->session()->put('hotel_korisnik',$json['email_adresa']);
                        return response()->json(['Status' => "Uspesno prijavljivanje recepcionera!"]);
                    }
                    else if(Korisnik::whereEmail_adresa($json['email_adresa'])->first()['tip_radnika'] === "Radnik u restoranu")
                    {
                        $request->session()->put('hotel_korisnik',$json['email_adresa']);
                        return response()->json(['Status' => "Uspesno prijavljivanje radnika u restoranu!"]);
                    }
                    else 
                    {
                        $request->session()->put('hotel_korisnik',$json['email_adresa']);
                        return response()->json(['Status' => "Uspesno prijavljivanje radnika u kaficu!"]);
                    }
                }
    		}
    	}
        //
    	return response()->json(['Status' => "Neuspesno prijavljivanje!"]);
    }


    //Funkcija koja vraca korisnika prema email adresi
    //Funkciji se prosledjuje email adresa
    public function vratiKorisnikaPremaEmailAdresi(Request $request)
    {
        $json = $_POST;
        if(Korisnik::whereEmail_adresa($json['email_adresa'])->count() === 0)
        {
            return response()->json(['Status' => "Ne postoji korisnik sa tom email adresom!"]);
        }
        $korisnik = null;
        $korisnik['id'] = Korisnik::whereEmail_adresa($json['email_adresa'])->first()['id'];
        $korisnik['ime'] = Korisnik::whereEmail_adresa($json['email_adresa'])->first()['ime'];
        $korisnik['prezime'] = Korisnik::whereEmail_adresa($json['email_adresa'])->first()['prezime'];
        $korisnik['email_adresa'] = $json['email_adresa'];
        $korisnik['lozinka'] = Korisnik::whereEmail_adresa($json['email_adresa'])->first()['lozinka'];
        $korisnik['tip_korisnika'] = Korisnik::whereEmail_adresa($json['email_adresa'])->first()['tip_korisnika'];
        if($korisnik['tip_korisnika'] === "Radnik")
        {
            $korisnik['tip_radnika'] = Korisnik::whereEmail_adresa($json['email_adresa'])->first()['tip_radnika'];
        }

        return response()->json($korisnik);
    }


    //Funkcija koja vraca celokupnu tabelu Korisnik, u kojoj se nalaze i korisnici i radnici
    public function vratiSveKorisnikeIRadnikeHotela(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	if(!is_null(Korisnik::all()))
    	{
    		$i = 0;
    		$korisnici = Korisnik::all();
    		foreach($korisnici as $value)
    		{
                $slanje[$i]['ime'] = $value['ime'];
                $slanje[$i]['prezime'] = $value['prezime'];
    			$slanje[$i]['email_adresa'] = $value['email_adresa'];
    			$slanje[$i]['lozinka'] = $value['lozinka'];
    			$slanje[$i]['tip_korisnika'] = $value['tip_korisnika'];
                if($value['tip_korisnika']==="Radnik")
                {
                    $slanje[$i]['tip_radnika'] = $value['tip_radnika'];
                }
    			$i = $i + 1;
     		}
    	}
    	return response()->json($slanje);
    }


    //Funkcija koja vraca sve korisnike hotela
    public function vratiSveKorisnikeHotela(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        if(Korisnik::whereTip_korisnika("Korisnik")->count()>0)
        {
            $i = 0;
            $korisnici = Korisnik::where('tip_korisnika', "Korisnik")->get();
            foreach($korisnici as $value)
            {
                $slanje[$i]['ime'] = $value['ime'];
                $slanje[$i]['prezime'] = $value['prezime'];
                $slanje[$i]['email_adresa'] = $value['email_adresa'];
                $slanje[$i]['lozinka'] = $value['lozinka'];
                $slanje[$i]['tip_korisnika'] = $value['tip_korisnika'];
                $i = $i + 1;
            }
        }
        return response()->json($slanje);
    }


    //Funkcija koja vraca sve radnike hotela
    public function vratiSveRadnikeHotela(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        if(Korisnik::whereTip_korisnika("Radnik")->count() > 0)
        {
            $i = 0;
            $korisnici = Korisnik::where('tip_korisnika', "Radnik")->get();
            foreach($korisnici as $value)
            {
                $slanje[$i]['id'] = $value['id'];
                $slanje[$i]['ime'] = $value['ime'];
                $slanje[$i]['prezime'] = $value['prezime'];
                $slanje[$i]['email_adresa'] = $value['email_adresa'];
                $slanje[$i]['lozinka'] = $value['lozinka'];
                $slanje[$i]['tip_korisnika'] = $value['tip_korisnika'];
                $slanje[$i]['tip_radnika'] = $value['tip_radnika'];
                $i = $i + 1;
            }
        }
        return response()->json($slanje);
    }


    //Funkcija koja vraca sve radnike jednog tipa posla (na primer, sve recepcionere ili sve radnike kafica)

    public function vratiSveRadnikeHotelaOdredjenogTipaPosla(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        if($json['tip_radnika'] === "Radnik u kafiću")
            {
                $tipRadnika = "Radnik u kaficu";
            }
            else
            {
                $tipRadnika = $json['tip_radnika'];
            }
        if(Korisnik::whereTip_radnika($tipRadnika)->count() > 0)
        {
            $i = 0;
            $korisnici = Korisnik::where('tip_korisnika', "Radnik")->where('tip_radnika', $tipRadnika)->get();
            foreach($korisnici as $value)
            {
                $slanje[$i]['id'] = $value['id'];
                $slanje[$i]['ime'] = $value['ime'];
                $slanje[$i]['prezime'] = $value['prezime'];
                $slanje[$i]['email_adresa'] = $value['email_adresa'];
                $slanje[$i]['lozinka'] = $value['lozinka'];
                $slanje[$i]['tip_korisnika'] = $value['tip_korisnika'];
                $slanje[$i]['tip_radnika'] = $value['tip_radnika'];
                $i = $i + 1;
            }
        }
        return response()->json($slanje);
    }
}
