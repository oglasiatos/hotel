<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ocene;
use App\KnjigaUtisaka;
use App\CestaPitanja;
use App\Korisnik;

class UtisciIOceneController extends Controller
{
	//OCENE
    //DODAVANJE OCENE
    //Funkciji se prosledjuju ime i prezime korisnika koji ocenjuje hotel i ocena koju je dao
    public function dodavanjeOcene(Request $request)
    {
    	$json = $_POST;
    	$novaOcena = new Ocene(['ime' => $json['ime'], 'prezime' => $json['prezime'], 'ocena' => intval($json['ocena'])]);
    	$novaOcena->save();
    	return response()->json(['Status' => "Uspesno ocenjen hotel!"]);
    }

    public function prosecnaOcenaHotela(Request $request)
    {
    	$json = $_POST;
    	$prosecnaOcena = DB::table('AppOcene')->avg('ocena');
    	return response()->json($prosecnaOcena);
    }


    //CESTA PITANJA

    public function proveriDaLiJeKorisnikPrijavljen(Request $request)
    {
        $json = $_POST;
        if($request->session()->has('hotel_korisnik'))
        {
            $email_adresa = $request->session()->get('hotel_korisnik');
            $korisnik = Korisnik::where('email_adresa', $email_adresa)->get();
            $slanje = null;
            $slanje['ime'] = $korisnik[0]['ime'];
            $slanje['prezime'] = $korisnik[0]['prezime'];
            $slanje['email_adresa'] = $korisnik[0]['email_adresa'];
            $slanje['Status'] = "Ima sesije!";
            return response()->json($slanje);
        }
        else
        {
            return response()->json(['Status' => "Nema sesije!"]);
        }
    }

    //Funkciji se prosledjuju ime i prezime korisnika koji postavlja pitanje i pitanje koje je postavio

    public function dodajPitanje(Request $request)
    {
    	$json = $_POST;
    	$pitanje = new CestaPitanja(['ime' => $json['ime'], 'prezime' => $json['prezime'], 'pitanje' => $json['pitanje'], 'odgovor' => ""]);
    	$pitanje->save();
        return response()->json(['Status' => "Uspesno postavljeno pitanje!"]);
    }

    //Funkciji se prosledjuje pitanje na koje admin zeli da odgovori kao i odgovor koji je uneo

    public function odgovoriNaPitanje(Request $request)
    {
    	$json = $_POST;
        $pitanje = CestaPitanja::find(intval($json['id']));
    	$pitanje->odgovor = $json['odgovor'];
        $pitanje->save();
    	return response()->json(['Status' => "Uspesno odgovaranje na pitanje!"]);
    }

    public function vratiSvaOdgovorenaPitanjaINjihoveOdgovore(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	if(CestaPitanja::all()->count()>0)
    	{
    		$i = 0;
    		$pitanja = CestaPitanja::all();
    		foreach($pitanja as $value)
    		{
    			if($value['odgovor'] != "")
    			{
                    $slanje[$i]['id'] = $value['id'];
                    $slanje[$i]['ime'] = $value['ime'];
                    $slanje[$i]['prezime'] = $value['prezime'];
    				$slanje[$i]['pitanje'] = $value['pitanje'];
    				$slanje[$i]['odgovor'] = $value['odgovor'];
    				$i = $i + 1;
    			}
    		}
    	}

    	return response()->json($slanje);
    }

    public function vratiSvaNeodgovorenaPitanja(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	$pitanja = CestaPitanja::where('odgovor', "")->get();
        $i = 0;
        if(!is_null($pitanja))
        {
            foreach($pitanja as $value)
            {
                $slanje[$i]['id'] = $value['id'];
                $slanje[$i]['ime'] = $value['ime'];
                $slanje[$i]['prezime'] = $value['prezime'];
                $slanje[$i]['pitanje'] = $value['pitanje'];
                $i = $i + 1;
            }
        }
    	return response()->json($slanje);
    }

    //BRISANJE PITANJA
    //Funkciji se prosledjuju ime i prezime korisnika koji je postavio pitanje i samo pitanje koje je postavio

    public function obrisiPitanje(Request $request)
    {
        $json = $_POST;
        if(!is_null(CestaPitanja::find(intval($json['id']))))
        {
            CestaPitanja::destroy(intval($json['id']));
            return response()->json(['Status' => "Uspesno obrisano pitanje!"]);
        }
        return response()->json(['Status' => "Neuspesno obrisano pitanje!"]);
    }



    //KNJIGA UTISAKA
    //DODAVANJE NOVOG UTISKA
    //Funkciji se prosledjuju ime i prezime korisnika koji unosi utisak i sadrzaj utiska

    public function dodavanjeNovogUtiska(Request $request)
    {
    	$json = $_POST;
        $korisnik = Korisnik::find(intval($json['id']));
    	$utisak = new KnjigaUtisaka(['ime' => $korisnik['ime'], 'prezime' => $korisnik['prezime'], 'utisak' => $json['utisak']]);
    	$utisak->save();
    	return response()->json($utisak);
    }

    public function vratiSveUtiske(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
        if($request->session()->has('hotel_korisnik'))
            $slanje[0]['trenutni_korisnik']=$request->session()->get('hotel_korisnik');
        else
            $slanje[0]['trenutni_korisnik'] = "Nema sesije!";
    	if(KnjigaUtisaka::all()->count() > 0)
    	{
            $i = 0;
    		$utisci = KnjigaUtisaka::all();
    		foreach($utisci as $value)
    		{
                $slanje[$i]['id'] = $value['id'];
                $slanje[$i]['ime'] = $value['ime'];
                $slanje[$i]['prezime'] = $value['prezime'];
    			$slanje[$i]['utisak'] = $value['utisak'];
    			$i = $i + 1;
    		}
    	}
    	return response()->json($slanje);
    }

    //Funkciji se prosledjuju ime i prezime korisnika koji su postavili utisak i sam utisak
    public function obrisiUtisak(Request $request)
    {
        $json = $_POST;
        if(!is_null(KnjigaUtisaka::find(intval($json['id']))))
        {
            KnjigaUtisaka::destroy(intval($json['id']));
            return response()->json(['Status' => "Uspesno obrisan utisak!"]);
        }
        return response()->json(['Status' => "Neuspesno obrisan utisak!"]);
    }
}
