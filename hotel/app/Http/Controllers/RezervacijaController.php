<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rezervacija;
use App\HotelskaSoba;
use App\Usluge;
use App\CenovnikRestorana;

class RezervacijaController extends Controller
{


	public function proveriDaLiPostojeRezervacijeZaSobu($brojSobe)
	{
		if(!is_null(Rezervacija::where('broj_sobe', $brojSobe)->get()))
			return true;
		return false;
	}

	public function proveriDaLiJeSobaSlobodna($brojSobe, $datumOd, $datumDo)
	{
        $dOd = (new \DateTime($datumOd))->format('m/d/Y');
        $dDo = (new \DateTime($datumDo))->format('m/d/Y'); 
		if($this->proveriDaLiPostojeRezervacijeZaSobu($brojSobe))
		{
			$postojeceRezervacije = Rezervacija::where('broj_sobe', $brojSobe)->get();
			foreach($postojeceRezervacije as $value)
			{
                $VOd = (new \DateTime($value['rezervisana_od']));
                $VDo = (new \DateTime($value['rezervisana_do']));
                $Od = (new \DateTime($dOd));
                $Do = (new \DateTime($dDo)); 
				if(($VOd < $Od && $VDo < $Od) || ($VOd > $Do && $VDo > $Do))
				{
				}
				else
				{
					return false;
				}
			}
			return true;
		}
		return true;
	}

	public function proveriDaLiSuDatumiKorektnoUneti($datumOd, $datumDo)
	{
        $danasnjiDatum = (new \DateTime("now"));
        $dOd = (new \DateTime($datumOd))->format('m/d/Y');
        $dDo = (new \DateTime($datumDo))->format('m/d/Y');  
        $Od = (new \DateTime($dOd));
        $Do = (new \DateTime($dDo));
        if(($Od <= $Do) && ($Od >= $danasnjiDatum))
        	return true;
        return false;
	}

    public function rezervacijaSobe(Request $request)
    {
    	$json = $_POST;
    	if(Rezervacija::whereBroj_sobe(intval($json['broj_sobe']))->count() > 0)
        {
            if($this->proveriDaLiSuDatumiKorektnoUneti($json['rezervisana_od'], $json['rezervisana_do']))
            {
                if($this->proveriDaLiJeSobaSlobodna(intval($json['broj_sobe']), $json['rezervisana_od'], $json['rezervisana_do']))
                {
                    $datetime1 = (new \DateTime($json['rezervisana_od']));
                    $datetime2 = (new \DateTime($json['rezervisana_do']));
                    $interval = $datetime1->diff($datetime2);
                    $days = $interval->format('%a');
                    $trosak = $days * HotelskaSoba::whereBroj_sobe(intval($json['broj_sobe']))->first()['cena_po_noci'];
                    $d1 = (new \DateTime($json['rezervisana_od']))->format('m/d/Y');
                    $d2 = (new \DateTime($json['rezervisana_do']))->format('m/d/Y');
                    $novaRezervacija = new Rezervacija(['broj_sobe' => intval($json['broj_sobe']), 'email_adresa_korisnika' => $json['email_adresa_korisnika'], 'rezervisana_od' => $d1, 'rezervisana_do' => $d2, 'ukupan_trosak_boravka' => $trosak]);
                    $novaRezervacija->save();
                    return response()->json(['Status' => "Uspesno obavljena rezervacija sobe!"]);
                }
                else
                {
                    return response()->json(['Status' => "Nije slobodna soba!"]);
                }
            }
            else {
                return response()->json(['Status' => "Datum nije validan!"]);
            }
        }
        else
        {
            return response()->json(['Status' => "Soba ne postoji!"]);
        }
    }




    public function otkazivanjeRezervacije(Request $request)
    {
    	$json = $_POST;
    	if(Rezervacija::find(intval($json['id'])))
    	{
            $brojSobe = Rezervacija::find(intval($json['id']))['broj_sobe'];
    		Rezervacija::destroy(intval($json['id']));
            $soba = HotelskaSoba::whereBroj_sobe(intval($brojSobe))->first();
            $soba->slobodna_ili_zauzeta = "Slobodna";
            $soba->save();
    		return response()->json(['Status' => "Uspesno otkazana rezervacija sobe!"]);
    	}
    	return respone()->json(['Status' => "Neuspesno otkazana rezervacija sobe!"]);
    }


    public function otklanjanjeIsteklihRezervacijaIzTabele(Request $request)
    {
    	$json = $_POST;
    	$rezervacije = Rezervacija::all();
    	$danasnjiDatum = (new \DateTime("now"));
    	foreach($rezervacije as $value)
    	{
            $rezervisanaDo = (new \DateTime($value['rezervisana_do']));
    		if($rezervisanaDo < $danasnjiDatum)
    		{
    			Rezervacija::destroy($value['id']);
    			$this->oslobadjanjeSobe($value['broj_sobe']);
    		}
    	}
    	return response()->json(['Status' => "Uspesno azurirane rezervacije!"]);
    }

    public function oslobadjanjeSobe($brojSobe)
    {
    	$soba = HotelskaSoba::whereBroj_sobe($brojSobe)->first();
    	$soba->slobodna_zauzeta = "Slobodna";
    	$soba->save();
        return response()->json(['Status' => "Uspesno oslobodjena soba!"]);
    }


    public function zauzimanjeRezervisaneSobe(Request $request)
    {
    	$json = $_POST;
    	$danasnjiDatum = (new \DateTime("now"));
        $rezervacije = Rezervacija::all();
        foreach($rezervacije as $value)
        {
            $rezervisanaOd = (new \DateTime($value['rezervisana_od']));
            if($rezervisanaOd <= $danasnjiDatum)
            {
                $soba = HotelskaSoba::whereBroj_sobe(intval($value['broj_sobe']))->first();
                $soba->slobodna_ili_zauzeta = "Zauzeta";
                $soba->save();
            }
        }
        return response()->json(['Status' => "Uspesno zauzimanje rezervisanih soba!"]);
    }


    public function dodavanjeUslugeNaRacun(Request $request)
    {
    	$json = $_POST;
    	$rezervacija = Rezervacija::find(intval($json['id']));
    	$cenaUsluge = Usluge::find(intval($json['id_usluge']));
    	$noviTrosak = $rezervacija['ukupan_trosak_boravka'] + $cenaUsluge['cena'];
    	$rezervacija->ukupan_trosak_boravka = $noviTrosak;
    	$rezervacija->save();
    	return response()->json(['Status' => "Uspesno dodata cena usluge na racun!"]);
    }


    public function dodavanjeRoomServiceUslugeNaRacun(Request $request)
    {
    	$json = $_POST;
    	$rezervacija = Rezervacija::find(intval($json['id']));
    	$cenaUsluge = CenovnikRestorana::where('naziv_jela_ili_pica', $json['naziv_jela_ili_pica'])->get();
    	$noviTrosak = $rezervacija['ukupan_trosak_boravka'] + $cenaUsluge[0]['cena'];
    	$rezervacija->ukupan_trosak_boravka = $noviTrosak;
    	$rezervacija->save();
    	return response()->json(['Status' => "Uspesno dodata cena room service usluge na racun!"]);
    }


    public function izlistavanjeSvihRezervacija(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	$rezervacije = Rezervacija::all();
    	if(!is_null($rezervacije))
    	{
    		$i = 0;
    		foreach($rezervacije as $value)
	    	{
	    		$slanje[$i]['id'] = $value['id'];
	    		$slanje[$i]['broj_sobe'] = $value['broj_sobe'];
	    		$slanje[$i]['email_adresa_korisnika'] = $value['email_adresa_korisnika'];
	    		$slanje[$i]['rezervisana_od'] = $value['rezervisana_od'];
	    		$slanje[$i]['rezervisana_do'] = $value['rezervisana_do'];
	    		$slanje[$i]['ukupan_trosak_boravka'] = $value['ukupan_trosak_boravka'];
	    		$i = $i + 1;
	    	}
    	}
    	return response()->json($slanje);
    }

    public function izlistavanjeSvihRezervacijaJednogKorisnika(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	$rezervacije = Rezervacija::where('email_adresa_korisnika', $json['email_adresa_korisnika'])->get();
    	if(!is_null($rezervacije))
    	{
    		$i = 0;
    		foreach($rezervacije as $value)
    		{
    			$slanje[$i]['id'] = $value['id'];
    			$slanje[$i]['broj_sobe'] = $value['broj_sobe'];
    			$slanje[$i]['email_adresa_korisnika'] = $value['email_adresa_korisnika'];
    			$slanje[$i]['rezervisana_od'] = $value['rezervisana_od'];
    			$slanje[$i]['rezervisana_do'] = $value['rezervisana_do'];
    			$slanje[$i]['ukupan_trosak_boravka'] = $value['ukupan_trosak_boravka'];
    			$i = $i + 1;
    		}
            return response()->json($slanje);
    	}
    }


    public function izlistajRezervacijeZaSobuJednogKorisnika(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $danasnjiDatum = (new \DateTime())->format('m/d/Y');
        $rezervacije = Rezervacija::where('email_adresa_korisnika', $json['email_adresa_korisnika'])->where('rezervisana_od', '>', $danasnjiDatum)->where('broj_sobe', intval($json['broj_sobe']))->get();
    
        return response()->json($rezervacije);
    }


    public function izlistavanjeSvihTrenutnihRezervacija(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	$danasnjiDatum = (new \DateTime())->format('m/d/Y');
    	$rezervacije = Rezervacija::where('rezervisana_od', '<', $danasnjiDatum)->where('rezervisana_do', '>', $danasnjiDatum)->get();
    	if(!is_null($rezervacije))
    	{
    		$i = 0;
    		foreach($rezervacije as $value)
    		{
    			$slanje[$i]['id'] = $value['id'];
    			$slanje[$i]['broj_sobe'] = $value['broj_sobe'];
    			$slanje[$i]['email_adresa_korisnika'] = $value['email_adresa_korisnika'];
    			$slanje[$i]['rezervisana_od'] = $value['rezervisana_od'];
    			$slanje[$i]['rezervisana_do'] = $value['rezervisana_do'];
    			$slanje[$i]['ukupan_trosak_boravka'] = $value['ukupan_trosak_boravka'];
    			$i = $i + 1;
    		}
    	}
        return response()->json($slanje);
    }

    public function izlistavanjeSvihRezervacijaKojeTrebaDaSePokupeNaDanasnjiDan(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	$danasnjiDatum = (new \DateTime())->format('m/d/Y');
    	$rezervacije = Rezervacija::where('rezervisana_od', $danasnjiDatum)->get();
    	if(!is_null($rezervacije))
    	{
    		foreach($rezervacije as $value)
    		{
    			$slanje[$i]['id'] = $value['id'];
    			$slanje[$i]['broj_sobe'] = $value['broj_sobe'];
    			$slanje[$i]['email_adresa_korisnika'] = $value['email_adresa_korisnika'];
    			$slanje[$i]['rezervisana_od'] = $value['rezervisana_od'];
    			$slanje[$i]['rezervisana_do'] = $value['rezervisana_do'];
    			$slanje[$i]['ukupan_trosak_boravka'] = $value['ukupan_trosak_boravka'];
    			$i = $i + 1;
    		}
    	}
        return response()->json($slanje);
    }

    public function proveriDaLiSeUklapaDatumSaDrugimRezervacijama($datumDo, $idRezervacije)
    {
        $rezervacija = Rezervacija::find(intval($idRezervacije));
        $brojSobe = $rezervacija['broj_sobe'];

        if(Rezervacija::where('broj_sobe', intval($brojSobe))->count() != 0)
        {
            $datumProduzetka = (new \DateTime($datumDo));
            $sveRezervacijeZaSobu = Rezervacija::where('broj_sobe', intval($brojSobe))->get();
            foreach($sveRezervacijeZaSobu as $value)
            {
                $rezervacijaOd = (new \DateTime($value['rezervisana_od']));
                if($rezervacijaOd <= $datumProduzetka)
                    return false;
            }
        }

        return true;
    }


    public function proveriDaLiJeDatumKorektnoUnet($datumDo, $idRezervacije)
    {
        $rezervacija = Rezervacija::find(intval($idRezervacije));
        $datumOd = (new \DateTime($rezervacija['rezervisana_od']));
        $dDo = (new \DateTime($datumDo));
        if($dDo <= $datumOd)
        {
            return false;
        }
        else
        {
            return $this->proveriDaLiSeUklapaDatumSaDrugimRezervacijama($datumDo, $idRezervacije);
        }
    }


    public function izmeniRezervaciju(Request $request)
    {
        $json = $_POST;
        //return $json;
        $rezervacija = Rezervacija::find(intval($json['id_rezervacije']));
        $noviDatum = (new \DateTime($json['novi_datum_do']))->format('m/d/Y');
        if($this->proveriDaLiJeDatumKorektnoUnet($noviDatum, $json['id_rezervacije'])===true)
        {
            $rezervacija->rezervisana_do = $noviDatum;
            $rezervacija->save();
            return response()->json(['Status' => "Uspesno izmenjena rezervacija!"]);
        }
        else
        {
            return response()->json(['Status' => "Izmena rezervacije nije uspela!"]);
        }
    }

    public function vratiTrenutnoVazeceRezervacijePremaBrojuSobe(Request $request)
    {
        $json = $_POST;
        $rezervacije = Rezervacija::where('broj_sobe', intval($json['broj_sobe']))->get();
        $slanje = null;
        $danasnjiDatum = (new \DateTime("now"));
        $i = 0;
        foreach($rezervacije as $value)
        {
            $rezervacijaOd = (new \DateTime($value['rezervisana_od']));
            $rezervacijaDo = (new \DateTime($value['rezervisana_do']));
            if($rezervacijaOd <= $danasnjiDatum && $danasnjiDatum <= $rezervacijaDo)
            {
                $slanje[$i]['id'] = $value['id'];
                $slanje[$i]['broj_sobe'] = $value['broj_sobe'];
                $slanje[$i]['email_adresa_korisnika'] = $value['email_adresa_korisnika'];
                $slanje[$i]['rezervisana_od'] = $value['rezervisana_od'];
                $slanje[$i]['rezervisana_do'] = $value['rezervisana_do'];
                $slanje[$i]['ukupan_trosak_boravka'] = $value['ukupan_trosak_boravka'];
                $i = $i + 1;
            }
        }
        return response()->json($slanje);
    }


    public function vratiRezervacijuPremaBrojuSobe(Request $request)
    {
        $json = $_POST;
        $rezervacija = Rezervacija::whereBroj_sobe(intval($json['broj_sobe']))->first();
        $slanje = null;
        $slanje['broj_sobe'] = $rezervacija['broj_sobe'];
        $slanje['id'] = $rezervacija['id'];
        $slanje['email_adresa_korisnika'] = $rezervacija['email_adresa_korisnika'];
        $slanje['rezervisana_od'] = $rezervacija['rezervisana_od'];
        $slanje['rezervisana_do'] = $rezervacija['rezervisana_do'];
        $slanje['ukupan_trosak_boravka'] = $rezervacija['ukupan_trosak_boravka'];
        return response()->json($slanje);
    }


    public function vratiRezervacijuPremaIdu(Request $request)
    {
        $json = $_POST;
        $rezervacija = Rezervacija::find(intval($json['id']));
        $slanje = null;
        $slanje['broj_sobe'] = $rezervacija['broj_sobe'];
        $slanje['id'] = $rezervacija['id'];
        $slanje['email_adresa_korisnika'] = $rezervacija['email_adresa_korisnika'];
        $slanje['rezervisana_od'] = $rezervacija['rezervisana_od'];
        $slanje['rezervisana_do'] = $rezervacija['rezervisana_do'];
        $slanje['ukupan_trosak_boravka'] = $rezervacija['ukupan_trosak_boravka'];
        return response()->json($slanje);
    }


}
