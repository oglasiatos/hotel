<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RezervacijaStolova;
use App\Restoran;

class RezervacijaStolovaController extends Controller
{

	public function proveriDaLiJeDatumKorektnoUnet($datumRezervacije)
	{
        $danasnjiDatum = (new \DateTime("now"));
        if($datumRezervacije < $danasnjiDatum)
            return false;
        return true;
	}

	public function proveriDaLiPostojiSto($kaficIliRestoran, $brojMesta, $datumZeljeneRezervacije)
	{
        $slanje = null;
		
        $danasnjiDatum = (new \DateTime())->format('m/d/Y');
        $zeljeniDatum = (new \DateTime($datumZeljeneRezervacije));

        if($danasnjiDatum==$zeljeniDatum)
        {
            $stolovi = Restoran::where('kafic_ili_restoran', $kaficIliRestoran)
                            ->where('broj_mesta', '>=', $brojMesta)
                            ->where('slobodan_ili_zauzet', "Slobodan")
                            ->get();
        }
        else
        {
            $stolovi = Restoran::where('kafic_ili_restoran', $kaficIliRestoran)
                                ->where('broj_mesta', '>=', $brojMesta)
                                ->get();
        }

		if($stolovi->count() != 0)
		{
			foreach($stolovi as $value)
            {
                $rezervacija = RezervacijaStolova::where('broj_stola', $value['broj_stola'])
                                                ->where('kafic_ili_restoran', $kaficIliRestoran)
                                                ->where('datum_rezervacije', $datumZeljeneRezervacije)
                                                ->get();
                if($rezervacija->count() === 0)
                {
                    return $value['broj_stola'];
                }
            }
		}

		return $slanje;
	}

    public function rezervacijaStola(Request $request)
    {
    	$json = $_POST;
        $datumZeljeneRezervacije = (new \DateTime($json['datum_rezervacije']));
        if($this->proveriDaLiJeDatumKorektnoUnet($datumZeljeneRezervacije))
        {
            if($this->proveriDaLiPostojiSto($json['kafic_ili_restoran'], intval($json['broj_mesta']), $json['datum_rezervacije']) != null)
            {
                $brojStola = $this->proveriDaLiPostojiSto($json['kafic_ili_restoran'], intval($json['broj_mesta']), $json['datum_rezervacije']);
                $novaRezervacija = new RezervacijaStolova(['broj_stola' => intval($brojStola), 'rezervacija_na_ime' => $json['rezervacija_na_ime'], 'datum_rezervacije' => $json['datum_rezervacije'], 'kafic_ili_restoran' => $json['kafic_ili_restoran']]);
                $novaRezervacija->save();
                return response()->json(['Status' => "Uspesno obavljena rezervacija stola!"]);
            }
            return response()->json(['Status' => "Nema slobodnih stolova za obavljanje rezervacije!"]);
        }
        return response()->json(['Status' => "Datum nije korektno unet!"]);
        
    }

    public function proveriDaLiJeMoguceOtkazatiNaOsnovuDatuma($datumRezervacije)
    {
    	$danasnjiDatum = (new \DateTime())->format('m/d/Y');
    	if($danasnjiDatum < $datumRezervacije)
    		return true;
    	return false;
    }

    public function otkazivanjeRezervacije(Request $request)
    {
    	$json = $_POST;
    	$rezervacija = RezervacijaStolova::find(intval($json['id']));

    	if(!is_null($rezervacija))
    	{
    		$datumRezervacije = (new \DateTime($rezervacija->datum_rezervacije));
    		if($this->proveriDaLiJeMoguceOtkazatiNaOsnovuDatuma($datumRezervacije))
    		{
    			$rezervacija->rezervacija_na_ime = "";
    			$rezervacija->datum_rezervacije = "";
    			$rezervacija->save();
    			return response()->json(['Status' => "Uspesno otkazana rezervacija!"]);
    		}
    	}

    	return response()->json(['Status' => "Neuspesno otkazana rezervacija!"]);
    }


    public function zauzimanjeRezervisanogStola(Request $request)
    {
    	$json = $_POST;
    	$rezervacija = RezervacijaStolova::where('kafic_ili_restoran', $json['kafic_ili_restoran'])
    									 ->where('broj_stola', intval($json['broj_stola']))
    									 ->get();

    	$danasnjiDatum = (new \DateTime())->format('m/d/Y');
    	if(!is_null($rezervacija))
    	{
    		$b = false;
    		foreach($rezervacija as $value)
	    	{
	    		$datumRezervacije = (new \DateTime($value['datum_rezervacije']));
	    		if($datumRezervacije === $danasnjiDatum)
	    		{
	    			$nadjeniSto = $value['broj_stola'];
	    			$b = true;
	    			break;
	    		}
	    	}
	    	if($b)
	    	{
	    		$sto = Restoran::where('kafic_ili_restoran', $json('kafic_ili_restoran'))
    					->where('broj_stola', $nadjeniSto)
    					->get();

		    	$sto[0]->slobodan_ili_zauzet = "Zauzet";
		    	$sto[0]->save();
		    	return response()->json(['Status' => "Uspesno zauzimanje stola!"]);
	    	}
    	}

    	return response()->json(['Status' => "Neuspesno zauzimanje stola!"]);  	
    }


    public function vratiSveNerezervisaneISlobodneStolove(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	$i = 0;
    	$danasnjiDatum = (new \DateTime())->format('m/d/Y');
    	$stolovi = Restoran::where('kafic_ili_restoran', $json['kafic_ili_restoran'])
    						->where('slobodan_ili_zauzet', "Slobodan")
    						->get();

    	if(!is_null($stolovi))
    	{
    		foreach($stolovi as $value)
    		{
    			$b = true;
    			$rezervacije = RezervacijaStolova::where('kafic_ili_restoran', $json['kafic_ili_restoran'])
    			->where('broj_stola', intval($value['broj_stola']))
    			->get();

    			if(!is_null($rezervacije))
    			{
    				
    				foreach($rezervacije as $val)
    				{
    					$datumRezervacije = (new \DateTime($val['datum_rezervacije']));
    					if($datumRezervacije === $danasnjiDatum)
    						$b = false;
    				}
    			}

    			if($b)
    			{
    				$slanje[$i]['id'] = $value['id'];
    				$slanje[$i]['broj_stola'] = $value['broj_stola'];
    				$slanje[$i]['broj_mesta'] = $value['broj_mesta'];
    				$slanje[$i]['slobodan_ili_zauzet'] = $value['slobodan_ili_zauzet'];
    				$slanje[$i]['kafic_ili_restoran'] = $value['kafic_ili_restoran'];
    				$slanje[$i]['racun'] = $value['racun'];
    				$i = $i + 1;
    			}
    		}
    	}

    	return response()->json($slanje);
    }

}
