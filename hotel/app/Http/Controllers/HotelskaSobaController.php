<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HotelskaSoba;
use App\Rezervacija;
use App\Korisnik;

class HotelskaSobaController extends Controller
{
    public function vratiIzgledSobe($a, Request $request)
    {
        if($request->session()->has('hotel_korisnik'))
        {
            $sesija=$request->session()->get('hotel_korisnik');
            if(Korisnik::whereEmail_adresa($sesija)->count() === 0)
            {
                $sesija="Nema Korisnika!";
                return view("soba",["br_sobe"=>$a,"Korisnik"=>$sesija]); 
            }
            $korisnik = null;
            $korisnik['tip_korisnika'] = Korisnik::whereEmail_adresa($sesija)->first()['tip_korisnika'];
            if($korisnik['tip_korisnika'] === "Radnik")
            {
                $korisnik['tip_radnika'] = Korisnik::whereEmail_adresa($sesija)->first()['tip_radnika'];
                if($korisnik['tip_radnika'] ==="Recepcioner")
                {
                    $sesija="Nema Korisnika!";
                    return view("soba",["br_sobe"=>$a,"Korisnik"=>$sesija]); 
                }
            }
        }
        else
        {
           $sesija="Nema Korisnika!";
        }
        return view("soba",["br_sobe"=>$a,"Korisnik"=>$sesija]);   

    }
    public function vratiSveHotelskeSobe(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $i = 0;
        $sobe = HotelskaSoba::where('broj_sobe', '<', 400)->orderBy('broj_sobe', 'asc')->get();
        if(!is_null($sobe))
        {
            foreach($sobe as $value)
            {
                if(!is_null($value['broj_sobe']))
                {
                    $slanje[$i]['broj_sobe'] = $value['broj_sobe'];
                    $slanje[$i]['broj_kreveta'] = $value['broj_kreveta'];
                    $slanje[$i]['slobodna_zauzeta'] = $value['slobodna_zauzeta'];
                    $slanje[$i]['sprat'] = $value['sprat'];
                    $slanje[$i]['opis'] = $value['opis'];
                    $slanje[$i]['pogled'] = $value['pogled'];
                    $slanje[$i]['terasa'] = $value['terasa'];
                    $slanje[$i]['wifi'] = $value['wifi'];
                    $slanje[$i]['tv'] = $value['tv'];
                    $slanje[$i]['klima'] = $value['klima'];
                    $slanje[$i]['cena_po_noci'] = $value['cena_po_noci'];
                    $i = $i + 1;
                }
            }
        }
        return response()->json($slanje);
    }
    public function vratiSveSlobodneHotelskeSobe(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        if(HotelskaSoba::whereSlobodna_zauzeta('Slobodna')->count()>0)
        {
            $i = 0;
            $sobe = HotelskaSoba::whereSlobodna_zauzeta('Slobodna');
            foreach($sobe as $value)
            {
                $slanje[$i]['broj_sobe'] = $value['broj_sobe'];
                $slanje[$i]['broj_kreveta'] = $value['broj_kreveta'];
                $slanje[$i]['slobodna_zauzeta'] = $value['slobodna_zauzeta'];
                $slanje[$i]['sprat'] = $value['sprat'];
                $slanje[$i]['opis'] = $value['opis'];
                $slanje[$i]['pogled'] = $value['pogled'];
                $slanje[$i]['terasa'] = $value['terasa'];
                $slanje[$i]['wifi'] = $value['wifi'];
                $slanje[$i]['tv'] = $value['tv'];
                $slanje[$i]['klima'] = $value['klima'];
                $slanje[$i]['cena_po_noci'] = $value['cena_po_noci'];
                $i = $i + 1;
            }
        }
        return response()->json($slanje);
    }

    public function vratiHotelskuSobuPremaBrojuSobe(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $soba = HotelskaSoba::whereBroj_sobe(intval($json['broj_sobe']))->first();
        if(!is_null($soba))
        {
            $slanje['id'] = $soba['id'];
            $slanje['broj_sobe'] = $soba['broj_sobe'];
            $slanje['broj_kreveta'] = $soba['broj_kreveta'];
            $slanje['slobodna_zauzeta'] = $soba['slobodna_zauzeta'];
            $slanje['sprat'] = $soba['sprat'];
            $slanje['opis'] = $soba['opis'];
            $slanje['pogled'] = $soba['pogled'];
            $slanje['terasa'] = $soba['terasa'];
            $slanje['wifi'] = $soba['wifi'];
            $slanje['tv'] = $soba['tv'];
            $slanje['klima'] = $soba['klima'];
            $slanje['cena_po_noci'] = $soba['cena_po_noci'];
        }
        return response()->json($slanje);
    }

    public function vratiHotelskuSobuPremaIdu(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $soba = HotelskaSoba::find(intval($json['id']));
        if(!is_null($soba))
        {
            $slanje['id'] = $soba['id'];
            $slanje['broj_sobe'] = $soba['broj_sobe'];
            $slanje['broj_kreveta'] = $soba['broj_kreveta'];
            $slanje['slobodna_zauzeta'] = $soba['slobodna_zauzeta'];
            $slanje['sprat'] = $soba['sprat'];
            $slanje['opis'] = $soba['opis'];
            $slanje['pogled'] = $soba['pogled'];
            $slanje['terasa'] = $soba['terasa'];
            $slanje['wifi'] = $soba['wifi'];
            $slanje['tv'] = $soba['tv'];
            $slanje['klima'] = $soba['klima'];
            $slanje['cena_po_noci'] = $soba['cena_po_noci'];
        } 
        return response()->json($slanje);
    }


    public function filtriranjeSoba(Request $request)
    {
        $json = $_POST;

        $slanje = null;

        $sobe = HotelskaSoba::where('cena_po_noci', '<', intval($json['maksimalna_cena']))
                            ->where('cena_po_noci', '>', intval($json['minimalna_cena']));

        if($json['broj_kreveta'] != 0)
        {
            $sobe = $sobe->where('broj_kreveta', intval($json['broj_kreveta']));
        }

        if($json['tv']==="true")
        {
            $sobe = $sobe->where('tv', true);
        }

        if($json['terasa'] === "true")
        {
            $sobe = $sobe->where('terasa', true);
        }

        if($json['wifi'] === "true")
        {
            $sobe = $sobe->where('wifi', true);
        }

        if($json['klima'] === "true")
        {
            $sobe = $sobe->where('klima', true);
        }

        $sobe = $sobe->orderBy('broj_sobe', 'asc')->get();

        if(!is_null($sobe))
        {
            return response()->json($this->filtriranjeSobaPremaDatumu($sobe, $json['datum_od'], $json['datum_do']));
        }

        return response()->json($slanje);
    }


    public function filtriranjeSobaPremaDatumu($sobe, $datum_od, $datum_do)
    {
        $slanje = null;
        $i = 0;

        if($datum_od === "" && $datum_do === "")
        {
            foreach($sobe as $value)
            {
                $slanje[$i]['id'] = $value['id'];
                $slanje[$i]['broj_sobe'] = $value['broj_sobe'];
                $slanje[$i]['broj_kreveta'] = $value['broj_kreveta'];
                $slanje[$i]['slobodna_zauzeta'] = $value['slobodna_zauzeta'];
                $slanje[$i]['opis'] = $value['opis'];
                $slanje[$i]['pogled'] = $value['pogled'];
                $slanje[$i]['terasa'] = $value['terasa'];
                $slanje[$i]['wifi'] = $value['wifi'];
                $slanje[$i]['klima'] = $value['klima'];
                $slanje[$i]['tv'] = $value['tv'];
                $slanje[$i]['cena_po_noci'] = $value['cena_po_noci'];
                $i = $i + 1;
            }
        }
        else if($datum_od === "")
        {
            $datumDo = (new \DateTime($datum_do));
            foreach($sobe as $value)
            {
                $b = true;
                $rezervacije = Rezervacija::where('broj_sobe', intval($value['broj_sobe']))->get();
                if(!is_null($rezervacije))
                {
                    foreach($rezervacije as $val)
                    {
                        $rezervisanaOd = (new \DateTime($val['rezervisana_od']));
                        if($rezervisanaOd <= $datumDo)
                            $b = false;
                    }
                }
                if($b)
                {
                    $slanje[$i]['id'] = $value['id'];
                    $slanje[$i]['broj_sobe'] = $value['broj_sobe'];
                    $slanje[$i]['broj_kreveta'] = $value['broj_kreveta'];
                    $slanje[$i]['slobodna_zauzeta'] = $value['slobodna_zauzeta'];
                    $slanje[$i]['opis'] = $value['opis'];
                    $slanje[$i]['pogled'] = $value['pogled'];
                    $slanje[$i]['terasa'] = $value['terasa'];
                    $slanje[$i]['klima'] = $value['klima'];
                    $slanje[$i]['wifi'] = $value['wifi'];
                    $slanje[$i]['tv'] = $value['tv'];
                    $slanje[$i]['cena_po_noci'] = $value['cena_po_noci'];
                    $i = $i + 1;
                }
            }
        }
        else if($datum_do === "")
        {
            $datumOd = (new \DateTime($datum_od));
            foreach($sobe as $value)
            {
                $b = true;
                $rezervacije = Rezervacija::where('broj_sobe', intval($value['broj_sobe']))->get();
                if(!is_null($rezervacije))
                {
                    foreach($rezervacije as $val)
                    {
                        $rezervisanaDo = (new \DateTime($val['rezervisana_do']));
                        if($rezervisanaDo >= $datumOd)
                            $b = false;
                    }
                }
                if($b)
                {
                    $slanje[$i]['id'] = $value['id'];
                    $slanje[$i]['broj_sobe'] = $value['broj_sobe'];
                    $slanje[$i]['broj_kreveta'] = $value['broj_kreveta'];
                    $slanje[$i]['slobodna_zauzeta'] = $value['slobodna_zauzeta'];
                    $slanje[$i]['opis'] = $value['opis'];
                    $slanje[$i]['pogled'] = $value['pogled'];
                    $slanje[$i]['terasa'] = $value['terasa'];
                    $slanje[$i]['klima'] = $value['klima'];
                    $slanje[$i]['tv'] = $value['tv'];
                    $slanje[$i]['wifi'] = $value['wifi'];
                    $slanje[$i]['cena_po_noci'] = $value['cena_po_noci'];
                    $i = $i + 1;
                }
            }
        }
        else
        {
            $datumOd = (new \DateTime($datum_od));
            $datumDo = (new \DateTime($datum_do));
            foreach($sobe as $value)
            {
                $b = true;
                $rezervacije = Rezervacija::where('broj_sobe', intval($value['broj_sobe']))->get();
                if(!is_null($rezervacije))
                {
                    foreach($rezervacije as $val)
                    {
                        $rezervisanaOd = (new \DateTime($val['rezervisana_od']));
                        $rezervisanaDo = (new \DateTime($val['rezervisana_do']));

                        if((($rezervisanaDo >= $datumOd) && ($rezervisanaDo <= $datumDo))
                        || (($rezervisanaOd <= $datumDo) && ($rezervisanaOd >= $datumOd)))
                            $b = false;
                    }
                }
                if($b)
                {
                    $slanje[$i]['id'] = $value['id'];
                    $slanje[$i]['broj_sobe'] = $value['broj_sobe'];
                    $slanje[$i]['broj_kreveta'] = $value['broj_kreveta'];
                    $slanje[$i]['slobodna_zauzeta'] = $value['slobodna_zauzeta'];
                    $slanje[$i]['opis'] = $value['opis'];
                    $slanje[$i]['pogled'] = $value['pogled'];
                    $slanje[$i]['terasa'] = $value['terasa'];
                    $slanje[$i]['wifi'] = $value['wifi'];
                    $slanje[$i]['klima'] = $value['klima'];
                    $slanje[$i]['tv'] = $value['tv'];
                    $slanje[$i]['cena_po_noci'] = $value['cena_po_noci'];
                    $i = $i + 1;
                }
            }
        }
        return $slanje;

    }
}
