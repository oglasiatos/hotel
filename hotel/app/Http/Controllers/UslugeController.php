<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usluge;
use App\HotelskaSoba;
use App\Rezervacija;

class UslugeController extends Controller
{
    public function izlistajSveUslugePremaVrsti(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $i = 0;
        $usluge = Usluge::where('osnovna_ili_dodatna_usluga', $json['vrsta_usluge'])->get();
        foreach($usluge as $value)
        {
            $slanje[$i]['id'] = $value['id'];
            $slanje[$i]['naziv_usluge'] = $value['naziv_usluge'];
            $slanje[$i]['cena'] = $value['cena'];
            $i = $i + 1;
        }
        return response()->json($slanje);

    }
    public function izlistajSveOsnovneUsluge(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	$i = 0;
    	$usluge = Usluge::where('osnovna_ili_dodatna_usluga', "Osnovna usluga")->get();
    	foreach($usluge as $value)
    	{
            $slanje[$i]['id'] = $value['id'];
    		$slanje[$i]['naziv_usluge'] = $value['naziv_usluge'];
    		$slanje[$i]['cena'] = $value['cena'];
    	}

    	return response()->json($slanje);
    }

    public function izlistajSveDodatneUsluge(Request $request)
    {
    	$json = $_POST;
    	$slanje = null;
    	$i = 0;
    	$usluge = Usluge::where('osnovna_ili_dodatna_usluga', "Dodatna usluga")->get();
    	foreach($usluge as $value)
    	{
            $slanje[$i]['id'] = $value['id'];
    		$slanje[$i]['naziv_usluge'] = $value['naziv_usluge'];
    		$slanje[$i]['cena'] = $value['cena'];
    	}
    	
    	return response()->json($slanje);
    }

    public function dodavanjeNoveUsluge(Request $request)
    {
        $json = $_POST;
        $novaUsluga = new Usluge(['naziv_usluge' => $json['naziv_usluge'], 'osnovna_ili_dodatna_usluga' => $json['osnovna_ili_dodatna_usluga'], 'cena' => intval($json['cena'])]);
        $novaUsluga->save();
        return response()->json(['Status' => "Uspesno dodata usluga!"]);
    }

    public function ukloniUslugu(Request $request)
    {
        $json = $_POST;
        if(!is_null(Usluge::find(intval($json['id']))))
        {
            Usluge::destroy(intval($json['id']));
            return response()->json(['Status' => "Uspesno obrisana usluga!"]);
        }
        return response()->json(['Status' => "Neuspesno obrisana usluga!"]);
    }

    public function izmeniCenuUsluge(Request $request)
    {
        $json = $_POST;
        $usluga = Usluge::find(intval($json['id']));
        if(!is_null($usluga))
        {
            $usluga->cena = intval($json['cena']);
            $usluga->save();
            return response()->json(['Status' => "Uspesno izmenjena usluga!"]);
        }
        return response()->json(['Status' => "Neuspesno izmenjena usluga!"]);
    }

    public function dodajUsluguNaRacunZaSobu(Request $request)
    {
        $json = $_POST;
        $rezervacija = Rezervacija::find(intval($json['id_rezervacije']));
        $usluga = Usluge::find(intval($json['id_usluge']));
        $soba->ukupan_trosak_boravka = $soba->ukupan_trosak_boravka + intval($usluga['cena']);
        $soba->save();
        return response()->json(['Status' => "Uspesno dodata usluga na racun!"]);
    }

    public function izlistajSveUsluge(Request $request)
    {
        $json = $_POST;
        $usluge = Usluge::all();
        $slanje = null;
        $i = 0;
        foreach($usluge as $value)
        {
            $slanje[$i]['id'] = $value['id'];
            $slanje[$i]['naziv_usluge'] = $value['naziv_usluge'];
            $slanje[$i]['cena'] = $value['cena'];
            $i = $i + 1;
        }
        return response()->json($slanje);
    }
}
