<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CenovnikRestorana;
use App\Restoran;
use App\RezervacijaStolova;

class RestoranController extends Controller
{
    //MENI
    public function iscitavanjeSvihJelaIPica(Request $request)
    {
    	$json = $_POST;
    	$cenovnik = CenovnikRestorana::all();
    	$slanje = null;
    	$i = 0;
    	foreach($cenovnik as $value)
    	{
            $slanje[$i]['id'] = $value['id'];
    		$slanje[$i]['vrsta_jela_ili_pica'] = $value['vrsta_jela_ili_pica'];
    		$slanje[$i]['naziv_jela_ili_pica'] = $value['naziv_jela_ili_pica'];
    		$slanje[$i]['cena'] = $value['cena'];
            $slanje[$i]['opis'] = $value['opis'];
    		$i = $i + 1;
    	}
    	return response()->json($slanje);
    }

    public function iscitavanjeSvihJela(Request $request)
    {
        $json = $_POST;
        $cenovnik = CenovnikRestorana::where('jelo_ili_pice', "Jelo")->get();
        $slanje = null;
        $i = 0;
        foreach($cenovnik as $value)
        {
            $slanje[$i]['id'] = $value['id'];
            $slanje[$i]['vrsta_jela_ili_pica'] = $value['vrsta_jela_ili_pica'];
            $slanje[$i]['naziv_jela_ili_pica'] = $value['naziv_jela_ili_pica'];
            $slanje[$i]['cena'] = $value['cena'];
            $slanje[$i]['opis'] = $value['opis'];
            $i = $i + 1;
        }
        return response()->json($slanje);
    }

    public function iscitavanjeSvihJelaIPicaPremaVrsti(Request $request)
    {
    	$json = $_POST;
    	$cenovnik = CenovnikRestorana::where('vrsta_jela_ili_pica', $json['vrsta_jela_ili_pica'])->get();
    	$slanje = null;
    	$i = 0;
        if(!is_null($cenovnik))
        {
            foreach($cenovnik as $value)
            {
                $slanje[$i]['id'] = $value['id'];
                $slanje[$i]['vrsta_jela_ili_pica'] = $value['vrsta_jela_ili_pica'];
                $slanje[$i]['naziv_jela_ili_pica'] = $value['naziv_jela_ili_pica'];
                $slanje[$i]['cena'] = $value['cena'];
                $slanje[$i]['opis'] = $value['opis'];
                $i = $i + 1;
            }
        }
    	return response()->json($slanje);
    }

    public function vratiListuJela(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $i = 0;
        $cenovnik = CenovnikRestorana::where('jelo_ili_pice', "Jelo")->get();
        foreach($cenovnik as $value)
        {
            $slanje[$i]['id'] = $value['id'];
            $slanje[$i]['vrsta_jela_ili_pica'] = $value['vrsta_jela_ili_pica'];
            $slanje[$i]['naziv_jela_ili_pica'] = $value['naziv_jela_ili_pica'];
            $slanje[$i]['cena'] = $value['cena'];
            $slanje[$i]['opis'] = $value['opis'];
            $i = $i + 1;
        }
        return response()->json($slanje);
    }

    //MENI ZA KAFIC

    public function vratiListuPica(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $i = 0;
        $cenovnik = CenovnikRestorana::where('jelo_ili_pice', "Pice")->get();
        foreach($cenovnik as $value)
        {
            $slanje[$i]['id'] = $value['id'];
            $slanje[$i]['vrsta_jela_ili_pica'] = $value['vrsta_jela_ili_pica'];
            $slanje[$i]['naziv_jela_ili_pica'] = $value['naziv_jela_ili_pica'];
            $slanje[$i]['cena'] = $value['cena'];
            $slanje[$i]['opis'] = $value['opis'];
            $i = $i + 1;
        }
        return response()->json($slanje);
    }



    public function vratiRacunZaSto(Request $request)
    {
        $json = $_POST;
        $sto = Restoran::find(intval($json['id']));

        return response()->json($sto->racun);
    }

    public function formirajRacunZaSto(Request $request)
    {
        $json = $_POST;
        $sto = Restoran::find(intval($json['id']));
        $sto->racun = $sto->racun + intval(CenovnikRestorana::find(intval($json['racun']))['cena']);
        if($sto->slobodan_ili_zauzet === "Slobodan")
            $sto->slobodan_ili_zauzet = "Zauzet";
        $sto->save();
        return response()->json(['Status' => "Uspesno formiran racun za sto!"]);
    }

    public function vratiSveSlobodneStolove(Request $request)
    {
        $json = $_POST;
        $slanje = null;
        $stolovi = Restoran::where('kafic_ili_restoran', $json['kafic_ili_restoran'])
                           ->where('slobodan_ili_zauzet', "Slobodan")
                           ->get();

        if(!is_null($stolovi))
        {
            $i = 0;
            foreach($stolovi as $value)
            {
                $slanje[$i]['broj_stola'] = $value['broj_stola'];
                $slanje[$i]['broj_mesta'] = $value['broj_mesta'];
                $slanje[$i]['racun'] = $value['racun'];
                $slanje[$i]['slobodan_ili_zauzet'] = $value['slobodan_ili_zauzet'];
                $slanje[$i]['id'] = $value['id'];
                $i = $i + 1;
            }
        }
        return response()->json($slanje);
    }

    public function oslobodiSto(Request $request)
    {
        $json = $_POST;
        $sto = Restoran::find(intval($json['id']));

        if(!is_null($sto) && $sto->slobodan_ili_zauzet === "Zauzet")
        {
            $sto->slobodan_ili_zauzet = "Slobodan";
            $sto->racun = 0;
            $sto->save();
            return response()->json(['Status' => "Uspesno oslobodjen sto!"]);
        }
        
        return response()->json(['Status' => "Neuspesno oslobodjen sto!"]);


    }

    
    public function vratiSveStoloveRestorana(Request $request)
    {
        $json = $_POST;
        $danasnjiDatum = (new \DateTime())->format('m/d/Y');
        $slanje = null;
        $i = 0;
        $stolovi = Restoran::where('kafic_ili_restoran', "Restoran")->orderBy('broj_stola', 'asc')->get();
        foreach($stolovi as $value)
        {
            $slanje[$i]['id'] = $value['id'];
            $slanje[$i]['broj_stola'] = $value['broj_stola'];
            $slanje[$i]['broj_mesta'] = $value['broj_mesta'];
            $slanje[$i]['racun'] = $value['racun'];

            $rezervacija = RezervacijaStolova::where('broj_stola', $value['broj_stola'])
                                            ->where('kafic_ili_restoran', "Kafic")
                                            ->get();
            $b = true;
            foreach($rezervacija as $val)
            {
                $datumRezervacije = (new \DateTime($val['datum_rezervacije']))->format('m/d/Y');
                if($datumRezervacije == $danasnjiDatum)
                    $b = false;
            }
            if(!$b)
            {
                $slanje[$i]['status'] = "Rezervisan";
            }
            else
            {
                $slanje[$i]['status'] = $value['slobodan_ili_zauzet'];
            }
            $i = $i + 1;
        }
        return response()->json($slanje);
    }

    public function vratiSveStoloveKafica(Request $request)
    {
        $json = $_POST;
        $danasnjiDatum = (new \DateTime())->format('m/d/Y');
        $slanje = null;
        $i = 0;
        $stolovi = Restoran::where('kafic_ili_restoran', "Kafic")->orderBy('broj_stola', 'asc')->get();
        foreach($stolovi as $value)
        {
            $slanje[$i]['id'] = $value['id'];
            $slanje[$i]['broj_stola'] = $value['broj_stola'];
            $slanje[$i]['broj_mesta'] = $value['broj_mesta'];
            $slanje[$i]['racun'] = $value['racun'];

            $rezervacija = RezervacijaStolova::where('broj_stola', $value['broj_stola'])
                                            ->where('kafic_ili_restoran', "Kafic")
                                            ->get();
            $b = true;
            foreach($rezervacija as $val)
            {
                $datumRezervacije = (new \DateTime($val['datum_rezervacije']))->format('m/d/Y');
                if($datumRezervacije == $danasnjiDatum)
                    $b = false;
            }
            if(!$b)
            {
                $slanje[$i]['status'] = "Rezervisan";
            }
            else
            {
                $slanje[$i]['status'] = $value['slobodan_ili_zauzet'];
            }
            $i = $i + 1;
        }
        return response()->json($slanje);
    }

    public function vratiSveStoloveSaTolikoIliViseMesta(Request $request)
    {
        $json = $_POST;
        $stolovi = Restoran::where('kafic_ili_restoran', $json['kafic_ili_restoran'])->where('broj_mesta', '>=', intval($json['broj_mesta']))->orderBy('broj_stola', 'asc')->get();
        $danasnjiDatum = (new \DateTime())->format('m/d/Y');
        $slanje = null;
        $i = 0;
        foreach($stolovi as $value)
        {
            $slanje[$i]['id'] = $value['id'];
            $slanje[$i]['broj_stola'] = $value['broj_stola'];
            $slanje[$i]['broj_mesta'] = $value['broj_mesta'];
            $slanje[$i]['racun'] = $value['racun'];

            $rezervacija = RezervacijaStolova::where('broj_stola', $value['broj_stola'])
                                            ->where('kafic_ili_restoran', "Kafic")
                                            ->get();
            $b = true;
            foreach($rezervacija as $val)
            {
                $datumRezervacije = (new \DateTime($val['datum_rezervacije']))->format('m/d/Y');
                if($datumRezervacije == $danasnjiDatum)
                    $b = false;
            }
            if(!$b)
            {
                $slanje[$i]['status'] = "Rezervisan";
            }
            else
            {
                $slanje[$i]['status'] = $value['slobodan_ili_zauzet'];
            }
            $i = $i + 1;
        }
        return response()->json($slanje);
    }
}
