<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;

class Rezervacija extends NeoEloquentModel
{
    protected $fillable = ['broj_sobe', 'email_adresa_korisnika', 'rezervisana_od', 'rezervisana_do', 'ukupan_trosak_boravka'];
}
