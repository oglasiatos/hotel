<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;

class Ocene extends NeoEloquentModel
{
    protected $fillable = ['ime', 'prezime', 'ocena'];
}
