<?php

namespace App;

use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;
use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;

class Korisnik extends NeoEloquentModel
{
    protected $fillable = ['ime', 'prezime', 'email_adresa', 'lozinka', 'tip_korisnika', 'tip_radnika'];
}
