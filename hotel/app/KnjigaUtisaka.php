<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;

class KnjigaUtisaka extends NeoEloquentModel
{
    protected $fillable = ['ime', 'prezime', 'utisak'];
}
