<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;

class CestaPitanja extends NeoEloquentModel
{
	protected $fillable = ['ime', 'prezime', 'pitanje', 'odgovor'];
}
