<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;

class RezervacijaStolova extends NeoEloquentModel
{
    protected $fillable = ['kafic_ili_restoran', 'broj_stola', 'rezervacija_na_ime', 'datum_rezervacije'];
}
