<?php

namespace App;


use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;

class Usluge extends NeoEloquentModel
{
    protected $fillable = ['naziv_usluge', 'osnovna_ili_dodatna_usluga', 'cena'];
}
