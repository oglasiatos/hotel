<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
//use Illuminate\Database\Eloquent\Model;
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;

class HotelskaSoba extends NeoEloquentModel
{
    protected $fillable = ['broj_sobe', 'broj_kreveta', 'slobodna_zauzeta', 'sprat', 'pogled', 'opis', 'terasa', 'wifi', 'tv', 'klima', 'cena_po_noci'];
}
