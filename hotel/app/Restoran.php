<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;

class Restoran extends NeoEloquentModel
{
    protected $fillable = ['kafic_ili_restoran', 'broj_stola', 'broj_mesta', 'racun', 'slobodan_ili_zauzet'];
}
