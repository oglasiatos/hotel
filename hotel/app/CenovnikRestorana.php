<?php

namespace App;

use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquentModel;
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;

class CenovnikRestorana extends NeoEloquentModel
{
    protected $fillable = ['vrsta_jela_ili_pica', 'jelo_ili_pice', 'naziv_jela_ili_pica', 'cena', 'opis'];
}
