<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hotel | Kontakt </title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!--link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet"-->
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">
  </head>
  <body>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

     <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="index.html">Hotel</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Opcije
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="index.html" class="nav-link">Početna stranica</a></li>
            <li class="nav-item"><a href="sobe.html" class="nav-link">Sobe</a></li>
            <li class="nav-item"><a href="restoran.html" class="nav-link">Restoran</a></li>
            <li class="nav-item"><a href="kafic.html" class="nav-link">Kafić</a></li>
            <li class="nav-item active"><a href="kontakt.html" class="nav-link">Kontakt</a></li>
            <li class="nav-item klasaPrijavljivanje"><a href="prijavljivanje.html" class="nav-link">Prijavljivanje</a></li>
            <li class="nav-item klasaRegistracija"><a href="registrovanje.html" class="nav-link">Registrovanje</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->

    <div class="hero-wrap" style="background-image: url('images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
          <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
          	<div class="text">
	            <p class="breadcrumbs mb-2"><span>Ovde nas možete kontaktirati.</span></p>
	            <h1 class="mb-4 bread">Česta pitanja</h1>
            </div>
          </div>
        </div>
      </div>
    </div>


    <section class="ftco-section contact-section bg-light">
      <div class="container">
        <div class="row d-flex mb-5 contact-info">
          <div class="col-md-12 mb-4">
            <h2 class="h3">Vaša pitanja možete poslati ovde.</h2>
          </div>
          <div class="w-100"></div>
          <div class="col-md-4 d-flex">
          	<div class="info bg-white p-4">
	            <p><span>Addresa:</span> Bulevar Nemanjića, Niš</p>
	          </div>
          </div>
          <div class="col-md-4 d-flex">
          	<div class="info bg-white p-4">
	            <p><span>Telefon:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
	          </div>
          </div>
          <div class="col-md-4 d-flex">
          	<div class="info bg-white p-4">
	            <p><span>E-mail:</span> <span>info@atostim.com</span></p>
	          </div>
          </div>
         
        </div>
        <div class="row block-9"> 

          <div class="col-md-6 order-md-last d-flex">
            <form  class="bg-white p-5 contact-form formaZaSlanjePoruke">
              <div class="form-group">
                <input id="ime" type="text" class="form-control" placeholder="Ime">
              </div>
              <div class="form-group">
                <input id="prezime" type="text" class="form-control" placeholder="Prezime">
              </div>
              <div class="form-group">
                <input id="email" type="text" class="form-control" placeholder="E-mail adresa">
              </div>
              <div class="form-group">
                <textarea id="pitanje" cols="30" rows="7" class="form-control" placeholder="Pitanje"></textarea>
              </div>
              <div class="form-group pull-right">
                <input type="button" value="Slanje" class="btn btn-primary py-3 px-5 ">
              </div>
            </form>
          
          </div>

          <div class="col-md-6 d-flex">
          	<div id="map" class="bg-white"></div>
          </div>
        </div>
      </div>
    </section>



  <section class="instagram pt-5">
      <div class="container-fluid">
        
        <div class="row no-gutters">
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-1.jpg" class="insta-img image-popup" style="background-image: url(images/insta-1.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-2.jpg" class="insta-img image-popup" style="background-image: url(images/insta-2.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-3.jpg" class="insta-img image-popup" style="background-image: url(images/insta-3.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-4.jpg" class="insta-img image-popup" style="background-image: url(images/insta-4.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-5.jpg" class="insta-img image-popup" style="background-image: url(images/insta-5.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Hotel</h2>
              <p>Dobrodošli u naš hotel. Nadamo se da ćete uživati u uslugama našeg hotela. Pored raskošnih soba, nudimo Vam i usluge našeg spa centra, bazena, restorana i kafića.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="https://twitter.com/"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Linkovi </h2>
              <ul class="list-unstyled">
                <li><a href="index.html" class="py-2 d-block">Početna stranica</a></li>
                <li><a href="sobe.html" class="py-2 d-block">Sobe</a></li>
                <li><a href="restoran.html" class="py-2 d-block">Restoran</a></li>
                <li><a href="kafic.html" class="py-2 d-block">Kafić</a></li>
                <li><a href="prijavljivanje.html" class="py-2 d-block">Prijavljivanje</a></li>
                <li><a href="registrovanje.html" class="py-2 d-block">Registrovanje</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Imate pitanja?</h2>
              <ul class="list-unstyled">
                <li><a href="kontakt.html" class="py-2 d-block">Kontakt</a></li>
                <li><a href="oNama.html" class="py-2 d-block">O nama</a></li>
                <li><a href="knjigaUtisaka.html" class="py-2 d-block">Knjiga utisaka i česta pitanja</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">O nama</h2>
              <div class="block-23 mb-3">
                <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text">Bulevar Nemanjića, Niš</span></li>
                  <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
                  <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@atostim.com</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <p> Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed by <span>AtosTim<sup>&copy;</sup></span></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script-->
  <script src="js/main.js"></script>
  <script src="js/jquery-confirm.min.js"></script>
  <script type="text/javascript">
    var Settings = {
      sastavljanjePorukeZaAdmina_url: "{{ url('/sastavljanjePorukeZaAdmina') }}",
      posaljiPitanjeAdminu_url: "{{ url('/posaljiPitanjeAdminu') }}",
      proveriDaLiJeKorisnikPrijavljen_url: "{{ url('/proveriDaLiJeKorisnikPrijavljen') }}",
      odjaviSe_url: "{{ url('/odjavaKorisnika') }}",
      pocetnaStranica_url: "{{ url('/index.html') }}"
    }
  </script>
  <script src="js/kontakt.js"></script>
  <script src="js/promenaPrijaveUOdjavu.js"></script>
    
  </body>
</html>