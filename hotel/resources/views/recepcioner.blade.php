<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hotel | Recepcioner </title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!--link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet"-->

    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">
  </head>
  <body>
  	<input type="hidden" name="_token" value="{{ csrf_token() }}">

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="index.html">Hotel</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Opcije
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="index.html" class="nav-link">Početna stranica</a></li>
            <li class="nav-item"><a href="sobe.html" class="nav-link">Sobe</a></li>
            <li class="nav-item"><a href="restoran.html" class="nav-link">Restoran</a></li>
            <li class="nav-item"><a href="kafic.html" class="nav-link">Kafić</a></li>
            <li class="nav-item active"><a href="recepcioner.html" class="nav-link">Recepcioner</a></li>
            <li class="nav-item"><a href="kontakt.html" class="nav-link">Kontakt</a></li>
            <li class="nav-item klasaPrijavljivanje"><a href="prijavljivanje.html" class="nav-link">Prijavljivanje</a></li>
            <li class="nav-item klasaRegistracija"><a href="registrovanje.html" class="nav-link">Registrovanje</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->

    <div class="hero-wrap" style="background-image: url('images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
          <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
          	<div class="text">
	            <!--p class="breadcrumbs mb-2"><span></span></p>
	            <h1 class="mb-4 bread">Recepcioner</h1-->
            </div>
          </div>
        </div>
      </div>
    </div>
<section class="ftco-booking pt-5">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-12">
    				<form class="booking-form">
	        		<div class="row">
	        			<div class="col-md-2 d-flex">
	        				<div class="form-group p-4 align-self-stretch d-flex align-items-end">
	        					<div class="wrap">
				    					<label>Datum prijema</label>
				    					<input type="text" name="datum_od" class="form-control checkin_date filtriranjeSoba" >
			    					</div>
			    				</div>
	        			</div>
	        			<div class="col-md-2 d-flex">
	        				<div class="form-group p-4 align-self-stretch d-flex align-items-end">
	        					<div class="wrap">
				    					<label>Datum odjave</label>
				    					<input type="text" name="datum_do" class="form-control checkout_date filtriranjeSoba" >
			    				</div>
			    				</div>
	        			</div>
	        			<div class="col-md-2 d-flex">
	        				<div class="form-group p-4 align-self-stretch d-flex align-items-end">
	        					<div class="wrap">
			      					<label>Broj kreveta</label>
			      					<div class="form-field">
			        					<input type="number" name="broj_kreveta" class="form-control filtriranjeSoba" placeholder="">
				              </div>
				            </div>
		              </div>
	        			</div>
	        			<div class="col-md-2 d-flex">
	        				<div class="form-group p-4 align-self-stretch d-flex align-items-end">
	        					<div class="wrap">
			      					<label >Opseg cene</label>
			      					<div class="form-field">
			        				<input type="number" name="minimalna_cena" class="form-control filtriranjeSoba" placeholder="od">
			        				<input type="number" name="maksimalna_cena" class="form-control filtriranjeSoba" placeholder="do">
				              </div>
				            </div>
		              </div>
	        			</div>
	        				<div class="col-md-3 d-flex">
	        				<div class="form-group p-4 align-self-stretch d-flex align-items-end">
	        					<div class="wrap">
			      					<label>Dodatne karakteristike</label>
			      					
			      					<div class="form-field">
			      						<div class="row">
			      							<div class="col-md-6 offset-1">
			        					<input type="checkbox" name="terasa" class="form-check-input pretrazivanje filtriranjeSoba">
									<label class="form-check-label">
										<span style="color: black;">Terasa</span>
									</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" name="tv" class="form-check-input pretrazivanje filtriranjeSoba" >
									<label class="form-check-label">
										<span style="color: black;">Tv</span>
									</label>
								</div>
									</div>
									<div class="row">
										<div class="col-md-4 offset-1">
									<input type="checkbox" name="klima" class="form-check-input pretrazivanje filtriranjeSoba">
									<label class="form-check-label">
										<span style="color: black;">Klima</span>
									</label>
								</div>
								<div class="col-md-4 offset-2">
									<input type="checkbox" name="wifi" class="form-check-input pretrazivanje filtriranjeSoba" >
									<label class="form-check-label">
										<span style="color: black;">Wifi</span>
									</label>
									</div>
								</div>
				              </div>
				            </div>
		              </div>
	        			</div>
	        			<div class="col-md-1 d-flex">
	        				<div class="form-group d-flex align-self-stretch">
			              <input type="button" value="Proveri" class="btn btn-primary  align-self-stretch dugmeZaFiltriranjeSoba">
			            </div>
	        			</div>
	        		</div>
	        	</form>
	    		</div>
    		</div>
    	</div>
    </section>

 <section class="ftco-section bg-light">
    	<div class="container">
    		<div class="row">
	        <div class="col-lg-9 offset-1">
		    		<div class="row izlistavanjeSoba">
		    			<div class="col-sm col-md-6 col-lg-4 ftco-animate">
		    				<div class="room">
		    					<a href="soba.html" class="img d-flex justify-content-center align-items-center" style="background-image: url(images/room-1.jpg);">
		    						<div class="icon d-flex justify-content-center align-items-center">
		    							<span class="icon-search2"></span>
		    						</div>
		    					</a>
		    					<div class="text p-3 text-center">
		    						<h3 class="mb-3"><a href="soba.html">Soba 22</a></h3>
		    						<p><span class="price mr-2">120.00€</span> <span class="per">po noći</span></p>
		    						<ul class="list">
		    							<li><span>Broj sobe:</span> 3 </li>
		    							<li><span>Pogled:</span> Pogled na more </li>
		    							<li><span>Broj kreveta:</span> 1</li>
		    						</ul>
		    						<hr>
		    						<p class="pt-1"><a href="soba.html" class="btn-custom"> Detaljnije <span class="icon-long-arrow-right"></span></a></p>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-sm col-md-6 col-lg-4 ftco-animate">
		    				<div class="room">
		    					<a href="rooms-single.html" class="img d-flex justify-content-center align-items-center" style="background-image: url(images/room-2.jpg);">
		    						<div class="icon d-flex justify-content-center align-items-center">
		    							<span class="icon-search2"></span>
		    						</div>
		    					</a>
		    					<div class="text p-3 text-center">
		    						<h3 class="mb-3"><a href="rooms-single.html">Family Room</a></h3>
		    						<p><span class="price mr-2">$20.00</span> <span class="per">per night</span></p>
		    						<ul class="list">
		    							<li><span>Max:</span> 3 Persons</li>
		    							<li><span>Size:</span> 45 m2</li>
		    							<li><span>View:</span> Sea View</li>
		    							<li><span>Bed:</span> 1</li>
		    						</ul>
		    						<hr>
		    						<p class="pt-1"><a href="room-single.html" class="btn-custom">Book Now <span class="icon-long-arrow-right"></span></a></p>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-sm col-md-6 col-lg-4 ftco-animate">
		    				<div class="room">
		    					<a href="rooms-single.html" class="img d-flex justify-content-center align-items-center" style="background-image: url(images/room-3.jpg);">
		    						<div class="icon d-flex justify-content-center align-items-center">
		    							<span class="icon-search2"></span>
		    						</div>
		    					</a>
		    					<div class="text p-3 text-center">
		    						<h3 class="mb-3"><a href="rooms-single.html">Deluxe Room</a></h3>
		    						<p><span class="price mr-2">$150.00</span> <span class="per">per night</span></p>
		    						<ul class="list">
		    							<li><span>Max:</span> 5 Persons</li>
		    							<li><span>Size:</span> 45 m2</li>
		    							<li><span>View:</span> Sea View</li>
		    							<li><span>Bed:</span> 2</li>
		    						</ul>
		    						<hr>
		    						<p class="pt-1"><a href="room-single.html" class="btn-custom">Book Now <span class="icon-long-arrow-right"></span></a></p>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-sm col-md-6 col-lg-4 ftco-animate">
		    				<div class="room">
		    					<a href="rooms-single.html" class="img d-flex justify-content-center align-items-center" style="background-image: url(images/room-4.jpg);">
		    						<div class="icon d-flex justify-content-center align-items-center">
		    							<span class="icon-search2"></span>
		    						</div>
		    					</a>
		    					<div class="text p-3 text-center">
		    						<h3 class="mb-3"><a href="rooms-single.html">Classic Room</a></h3>
		    						<p><span class="price mr-2">$130.00</span> <span class="per">per night</span></p>
		    						<ul class="list">
		    							<li><span>Max:</span> 5 Persons</li>
		    							<li><span>Size:</span> 45 m2</li>
		    							<li><span>View:</span> Sea View</li>
		    							<li><span>Bed:</span> 2</li>
		    						</ul>
		    						<hr>
		    						<p class="pt-1"><a href="room-single.html" class="btn-custom">Book Now <span class="icon-long-arrow-right"></span></a></p>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-sm col-md-6 col-lg-4 ftco-animate">
		    				<div class="room">
		    					<a href="rooms-single.html" class="img d-flex justify-content-center align-items-center" style="background-image: url(images/room-5.jpg);">
		    						<div class="icon d-flex justify-content-center align-items-center">
		    							<span class="icon-search2"></span>
		    						</div>
		    					</a>
		    					<div class="text p-3 text-center">
		    						<h3 class="mb-3"><a href="rooms-single.html">Superior Room</a></h3>
		    						<p><span class="price mr-2">$300.00</span> <span class="per">per night</span></p>
		    						<ul class="list">
		    							<li><span>Max:</span> 6 Persons</li>
		    							<li><span>Size:</span> 45 m2</li>
		    							<li><span>View:</span> Sea View</li>
		    							<li><span>Bed:</span> 3</li>
		    						</ul>
		    						<hr>
		    						<p class="pt-1"><a href="room-single.html" class="btn-custom">Book Now <span class="icon-long-arrow-right"></span></a></p>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-sm col-md-6 col-lg-4 ftco-animate">
		    				<div class="room">
		    					<a href="rooms-single.html" class="img d-flex justify-content-center align-items-center" style="background-image: url(images/room-6.jpg);">
		    						<div class="icon d-flex justify-content-center align-items-center">
		    							<span class="icon-search2"></span>
		    						</div>
		    					</a>
		    					<div class="text p-3 text-center">
		    						<h3 class="mb-3"><a href="rooms-single.html">Luxury Room</a></h3>
		    						<p><span class="price mr-2">$500.00</span> <span class="per">per night</span></p>
		    						<ul class="list">
		    							<li><span>Max:</span> 5 Persons</li>
		    							<li><span>Size:</span> 45 m2</li>
		    							<li><span>View:</span> Sea View</li>
		    							<li><span>Bed:</span> 2</li>
		    						</ul>
		    						<hr>
		    						<p class="pt-1"><a href="room-single.html" class="btn-custom">Book Now <span class="icon-long-arrow-right"></span></a></p>
		    					</div>
		    				</div>
		    			</div>
		    		</div>
		    	</div>
		    	
		    </div>
    	</div>
    </section>
    <section id = "sekcijaRezervacije" class="ftco-section bg-light">
    	<div class="container">
    		<div class="row">
    		<div class=" no-gutters justify-content-center pb-5">
                  <div class="col-md-10 text-center heading-section ftco-animate">
            <h2><span>Rezervacije:</span></h2>
          </div>
           </div>
         
       
    		<br> 
			  <table class="table table-dark table-striped">
 				   <thead>
					 <tr>
      				  <th>Broj sobe</th>
    				  <th>E-mail adresa</th>
       				  <th>Datum od</th>
        			  <th>Datum do</th>
         			  <th>Ukupna cena</th>
         			  <th></th>
         			  <th></th>
         			  <th><input type="button" value="Dodaj rezervaciju" class="dodajRezervaciju float-right btn btn-secondary btn-sm  py-2 px-4 dodavanjeNoveRezervacije"></th>
      				 </tr>
   				  </thead>
  			  <tbody class="listaSvihRezervacija">
    			  <tr>
        <td>22</td>
     
        <td>jovana.kostic@elfak.rs</td>
        <td>12/1/2020</td>
        <td>20/1/2020</td>
        <td>120</td>
        <td>
		<input type="button" value="Dodaj uslugu" class="dodajUslugu btn btn-primary btn-sm  py-2 px-4"></td>
        <td>
		<input type="button" value="Izmeni" class="izmeni btn btn-primary btn-sm  py-2 px-4"></td>
		<td>
		<input type="button" value="Naplati" class="naplati btn btn-primary btn-sm  py-2 px-4"></td>
      </tr>
      
    </tbody>
  </table>
     <div class="input-group col-sm-2 offset-10">
    <input type="text" class="form-control sadrzajPretrage" style="border-top-left-radius:10px; border-bottom-left-radius:10px;" placeholder="Broj sobe">
    <div class="input-group-append">
      <button class="btn btn-secondary btn-sm dugmeZaPretrazivanjeRezervacija" type="button">
        <icon class="icon-search2"></icon>
      </button>
    </div>
  </div>
		    </div>
    	</div>
    	<div id = "deoZaRacun" class="container pt-5 room deoZaRacun">
    		<div class="row ">
    			<div class="col-md-3 text offset-3">
    				<h3>Račun</h3>

    				<ul class="list pt-2 listaZaUkupniRacun">
    					<!--li><span>Cena po noći:</span><label class="float-right">50</label></li>
    					<li><span>Masaža:</span><label class="float-right">20</label></li>
    					<li><span>Bazen:</span><label class="float-right">10</label></li-->
    					<hr>
    					<li><span>Ukupno:</span>		
    					<!--
    						Kad se klikne na "dodaj uslugu" generise div racun i div usluge,
    						a kad se klikne na "naplati" sve to isto samo sa dodatim ovim button-om u okviru racuna (ovde pre label)

    						input type="button" value="Naplati" class="naplati btn btn-primary btn-sm  py-2 px-4"-->
    						<label class="float-right">80</label>
    				</ul>
    			</div>
    			<div class="col-md-3 text ">
    				<h3 class="usluge">Usluge:</h3>
    				<br>
    				<ul class="list listaUsluga">
    					<li><span>Bazen: </span><input type="button" value="+" class="float-right dodaj btn btn-primary btn-sm "><label class="float-right">20 &nbsp; </label><hr></li>
    					<li><span>Teretana: </span><input type="button" value="+" class="float-right dodaj btn btn-primary btn-sm "><label class="float-right">20 &nbsp; </label><hr></li>
    					<li><span>Spa centar: </span><input type="button" value="+" class="float-right dodaj btn btn-primary btn-sm "><label class="float-right">20 &nbsp; </label><hr></li>
    				</ul>
    			</div>
    			
    		</div>
    	</div>
    </section>

        

 <section class="instagram pt-5">
      <div class="container-fluid">
        
        <div class="row no-gutters">
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-1.jpg" class="insta-img image-popup" style="background-image: url(images/insta-1.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-2.jpg" class="insta-img image-popup" style="background-image: url(images/insta-2.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-3.jpg" class="insta-img image-popup" style="background-image: url(images/insta-3.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-4.jpg" class="insta-img image-popup" style="background-image: url(images/insta-4.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-5.jpg" class="insta-img image-popup" style="background-image: url(images/insta-5.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Hotel</h2>
              <p>Dobrodošli u naš hotel. Nadamo se da ćete uživati u uslugama našeg hotela. Pored raskošnih soba, nudimo Vam i usluge našeg spa centra, bazena, restorana i kafića.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="https://twitter.com/"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Linkovi </h2>
              <ul class="list-unstyled">
                <li><a href="index.html" class="py-2 d-block">Početna stranica</a></li>
                <li><a href="sobe.html" class="py-2 d-block">Sobe</a></li>
                <li><a href="restoran.html" class="py-2 d-block">Restoran</a></li>
                <li><a href="kafic.html" class="py-2 d-block">Kafić</a></li>
                <li><a href="prijavljivanje.html" class="py-2 d-block">Prijavljivanje</a></li>
                <li><a href="registrovanje.html" class="py-2 d-block">Registrovanje</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Imate pitanja?</h2>
              <ul class="list-unstyled">
                <li><a href="kontakt.html" class="py-2 d-block">Kontakt</a></li>
                <li><a href="oNama.html" class="py-2 d-block">O nama</a></li>
                <li><a href="knjigaUtisaka.html" class="py-2 d-block">Knjiga utisaka i česta pitanja</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">O nama</h2>
              <div class="block-23 mb-3">
                <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text">Bulevar Nemanjića, Niš</span></li>
                  <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
                  <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@atostim.com</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <p> Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed by <span>AtosTim<sup>&copy;</sup></span></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script-->
  <script src="js/main.js"></script>
  <script src="js/jquery-confirm.min.js"></script>
  <script type="text/javascript">
  	var Settings = {
  		izlistavanjeSvihSobaHotela_url: "{{ url('/izlistavanjeSvihSobaHotela') }}",
  		filtriranjeSobaPremaUnetimKriterijumima_url: "{{ url('/filtriranjeSobaPremaUnetimKriterijumima') }}",
  		prikazVelikeSobe_url: "{{ url('/soba') }}",
  		listaSvihRezervacija_url: "{{ url('/listaSvihRezervacija') }}",
  		dodavanjeNoveRezervacije_url: "{{ url('/dodavanjeNoveRezervacije') }}",
  		izmenaDatumaRezervacije_url: "{{ url('/izmenaDatumaRezervacije') }}",
  		pretrazivanjeRezervacija_url: "{{ url('/pretrazivanjeRezervacija') }}",
  		naplataBoravka_url: "{{ url('/naplataBoravka') }}",
  		prikazRacuna_url: "{{ url('/prikazRacuna') }}",
  		vratiListuUsluga_url: "{{ url('/vratiListuUsluga') }}",
  		dodavanjeUslugeNaRacun_url: "{{ url('/dodavanjeUslugeNaRacun') }}",
  		uklanjanjeProteklihRezervacija_url: "{{ url('/uklanjanjeProteklihRezervacija') }}",
  		zauzimanjeSobaZaRezervacijeKojeSuNaRedu_url: "{{ url('/zauzimanjeSobaZaRezervacijeKojeSuNaRedu') }}",
  		proveriDaLiJeKorisnikPrijavljen_url: "{{ url('/proveriDaLiJeKorisnikPrijavljen') }}",
      	odjaviSe_url: "{{ url('/odjavaKorisnika') }}",
      	pocetnaStranica_url: "{{ url('/index.html') }}",
      	izlistajUslugePremaVrstiUsluga_url: "{{ url('/izlistajUslugePremaVrstiUsluga') }}"
  	}
  </script>
  <script src="js/recepcioner.js"></script>
  <script src="js/promenaPrijaveUOdjavu.js"></script>
    
  </body>
</html>