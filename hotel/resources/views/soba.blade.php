<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hotel | Soba </title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
   <input type="hidden" name="br_sobe" class="br_sobe" value="{{$br_sobe}}">
   <input type="hidden" name="korisnik" class="korisnik" value="{{$Korisnik}}">
   <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="index.html">Hotel</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Opcije
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="index.html" class="nav-link">Početna stranica</a></li>
            <li class="nav-item active"><a href="sobe.html" class="nav-link">Sobe</a></li>
            <li class="nav-item"><a href="restoran.html" class="nav-link">Restoran</a></li>
            <li class="nav-item"><a href="kafic.html" class="nav-link">Kafić</a></li>
            <li class="nav-item"><a href="kontakt.html" class="nav-link">Kontakt</a></li>
            <li class="nav-item klasaPrijavljivanje"><a href="prijavljivanje.html" class="nav-link">Prijavljivanje</a></li>
        <li class="nav-item klasaRegistracija"><a href="registrovanje.html" class="nav-link">Registrovanje</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->

    <div class="hero-wrap" style="background-image: url('images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text d-flex align-itemd-end justify-content-center">
          <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
          	<div class="text">
	            <p class="breadcrumbs mb-2" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"> <span class="mr-2"><span>Opis i karakteristike</span></p>
	            <h1 class="mb-4 bread">Soba</h1>
            </div>
          </div>
        </div>
      </div>
    </div>


    <section class="ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="row ">
              <div class=" col-md-12 ftco-animate">
                <h2 class="mb-4">Soba <span id="brojSobe"> 22</span></h2>
                <div class="single-slider owl-carousel">
                  <div class="item">
                    <div class="room-img" style="background-image: url(images/room-1.jpg);"></div>
                  </div>
                  <div class="item">
                    <div class="room-img" style="background-image: url(images/room-2.jpg);"></div>
                  </div>
                  <div class="item">
                    <div class="room-img" style="background-image: url(images/room-3.jpg);"></div>
                  </div>
                </div>
              </div>
          		<!--div class="col-md-12 room-single mt-4 mb-5 ftco-animate">
                
                <h2>Karakteristike sobe:</h2>
    						<div class=" sidebar-box ftco-animate col-md-6 ">
                <div class="categories ">
                <li><a href="">Broj sobe: &nbsp;<span>22</span></a></li>
                <li><a href="">Cena po noći: &nbsp;<span>50</span></a></li>
                <li><a href="">Sprat: &nbsp;<span>7</span></a></li>
                <li><a href="">Broj kreveta: &nbsp;<span>2</span></a></li>
                <li><a href="">Pogled: &nbsp;<span>Pogled na more</span></a></li>
                
              </div>
            </div>
            <div class=" sidebar-box ftco-animate col-md-6 ">
                <div class="categories ">
                <li><a href="">Terasa: &nbsp;<span>Ne</span></a></li>
                <li><a href="">Klima: &nbsp;<span>Da</span></a></li>
                <li><a href="">WiFi: &nbsp;<span>Da</span></a></li>
                <li><a href="">Tv: &nbsp;<span>Da</span></a></li>
              </div>
            </div>
            <br>
            <div class=" sidebar-box ftco-animate col-md-12 ">
            <h2>Opis sobe:</h2><br>
                <p>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p>
          		</div>
            </div-->
          	

          		<!--div class="col-md-12 properties-single ftco-animate mb-5 mt-4">
          			<h4 class="mb-4">Review &amp; Ratings</h4>
          			<div class="row">
          				<div class="col-md-6">
          					<form method="post" class="star-rating">
										  <div class="form-check">
												<input type="checkbox" class="form-check-input" id="exampleCheck1">
												<label class="form-check-label" for="exampleCheck1">
													<p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i> 100 Ratings</span></p>
												</label>
										  </div>
										  <div class="form-check">
									      <input type="checkbox" class="form-check-input" id="exampleCheck1">
									      <label class="form-check-label" for="exampleCheck1">
									    	   <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i> 30 Ratings</span></p>
									      </label>
										  </div>
										  <div class="form-check">
									      <input type="checkbox" class="form-check-input" id="exampleCheck1">
									      <label class="form-check-label" for="exampleCheck1">
									      	<p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i><i class="icon-star-o"></i> 5 Ratings</span></p>
									     </label>
										  </div>
										  <div class="form-check">
										    <input type="checkbox" class="form-check-input" id="exampleCheck1">
									      <label class="form-check-label" for="exampleCheck1">
									      	<p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i><i class="icon-star-o"></i><i class="icon-star-o"></i> 0 Ratings</span></p>
									      </label>
										  </div>
										  <div class="form-check">
									      <input type="checkbox" class="form-check-input" id="exampleCheck1">
									      <label class="form-check-label" for="exampleCheck1">
									      	<p class="rate"><span><i class="icon-star"></i><i class="icon-star-o"></i><i class="icon-star-o"></i><i class="icon-star-o"></i><i class="icon-star-o"></i> 0 Ratings</span></p>
										    </label>
										  </div>
										</form>
          				</div>
          			</div>
          		</div-->
          		

          	</div>
          </div> <!-- .col-md-8 -->
         <div class=" sidebar-box ftco-animate col-lg-4 room ">
            <div class="text p-3">
            <span class="price mr-2">Opis sobe:</span>
                <p class="opissobe"><br>When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane. Pityful a rethoric question ran over her cheek, then she continued her way.</p><br>

                 <!--div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">
                          <p class="rate"><span><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i><i class="icon-star-o"></i></span></p>
                       </label>
                      </div-->
                    </div>
              </div>

        </div>
        <div class="room ">
        <div class=" p-3" >
            <h3>Karakteristike sobe:</h3><br>
                <div class="row">
                  <div class="text col-lg-3" style="border-right-color: transparent;">
                    <ul class="list list1">
                      <li><span>Broj sobe: &nbsp;</span> 3 </li>
                      <li><span>Cena po noći: &nbsp;</span>50</li>
                      <li><span>Sprat: &nbsp;</span>4</li>
                      <li><span>Broj kreveta: &nbsp;</span> 1</li>
                      <li><span>Pogled: &nbsp;</span> Pogled na more </li>
                    </ul>
                  </div>
                  <div class="text col-lg-2" style="border-left-color: transparent;">
                     <ul class="list list2">
                      <li><span>Terasa: &nbsp;</span> Da </li>
                      <li><span>Klima: &nbsp;</span> Ne </li>
                      <li><span>Wifi: &nbsp;</span> Da </li>
                      <li><span>Tv: &nbsp;</span>  Ne </li>
                    </ul>
                    <br>
                   <div class=" pull-right">
                   <div class="form-group col-lg-3 offset-2 ">
                    <input type="button" value="Rezervacija" class="rezervacija btn btn-primary btn-sm px-2 py-2" >
                  </div>
                  </div>
                  </div>


                    <div class="text col-lg-6  proverirez">
                      <div class="row">
                      <div class=" col-sm-4">
                      <input type="text" id="checkin_date" class="form-control checkin_date p-2" placeholder="Datum prijema">
                      </div>
                      <div class="form-group col-sm-4">
                      <input type="text" id="checkout_date" class="form-control checkout_date p-2" placeholder="Datum odjave">
                      </div> 

                      <div class="col-sm-3 pt-2 px-2">
                      <input type="button" value="Rezerviši" class="rezervisi btn btn-primary btn-sm px-3 py-2" >
                  </div>
                </div>
                </div>



                  </div>
                 
                </div>
              </div>
      </div>

    </section> <!--      .section -->



 <section class="instagram pt-5">
      <div class="container-fluid">
        
        <div class="row no-gutters">
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-1.jpg" class="insta-img image-popup" style="background-image: url(images/insta-1.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-2.jpg" class="insta-img image-popup" style="background-image: url(images/insta-2.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-3.jpg" class="insta-img image-popup" style="background-image: url(images/insta-3.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-4.jpg" class="insta-img image-popup" style="background-image: url(images/insta-4.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
          <div class="col-sm-12 col-md ftco-animate">
            <a href="images/insta-5.jpg" class="insta-img image-popup" style="background-image: url(images/insta-5.jpg);">
              <div class="icon d-flex justify-content-center">
                <span class="icon-instagram align-self-center"></span>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Hotel</h2>
              <p>Dobrodošli u naš hotel. Nadamo se da ćete uživati u uslugama našeg hotela. Pored raskošnih soba, nudimo Vam i usluge našeg spa centra, bazena, restorana i kafića.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="https://twitter.com/"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="https://www.instagram.com/"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Linkovi </h2>
              <ul class="list-unstyled">
                <li><a href="index.html" class="py-2 d-block">Početna stranica</a></li>
                <li><a href="sobe.html" class="py-2 d-block">Sobe</a></li>
                <li><a href="restoran.html" class="py-2 d-block">Restoran</a></li>
                <li><a href="kafic.html" class="py-2 d-block">Kafić</a></li>
                <li><a href="prijavljivanje.html" class="py-2 d-block">Prijavljivanje</a></li>
                <li><a href="registrovanje.html" class="py-2 d-block">Registrovanje</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Imate pitanja?</h2>
              <ul class="list-unstyled">
                <li><a href="kontakt.html" class="py-2 d-block">Kontakt</a></li>
                <li><a href="oNama.html" class="py-2 d-block">O nama</a></li>
                <li><a href="knjigaUtisaka.html" class="py-2 d-block">Knjiga utisaka i česta pitanja</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">O nama</h2>
              <div class="block-23 mb-3">
                <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text">Bulevar Nemanjića, Niš</span></li>
                  <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
                  <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@atostim.com</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <p> Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Designed by <span>AtosTim<sup>&copy;</sup></span></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="js/main.js"></script>

   <script type="text/javascript">
    var Settings={
      url_vratisobu:"{{url('/vratiSobuPramBrojuSobe')}}",
      url_rezrvacija:"{{url('/rezervisiSobu')}}",
      url_pocetna:"{{url('/index.html')}}",
      url_otkazirezervaciju:"{{url('/otkazivanjeRezervacijeZaSobu')}}",
      url_obrisirezervaciju:"{{url('/obrisiRezervacijuPrilikomOtkazivanja')}}",
      proveriDaLiJeKorisnikPrijavljen_url: "{{ url('/proveriDaLiJeKorisnikPrijavljen') }}",
      odjaviSe_url: "{{ url('/odjavaKorisnika') }}",
      pocetnaStranica_url: "{{ url('/index.html') }}"
    }
  </script>


  <script type="text/javascript" src="js/soba.js"></script>
  <script src="js/promenaPrijaveUOdjavu.js"></script>
    
  </body>
</html>