(function ($) {
    "use strict";


    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $(document).ready(function(){
		$( "form" ).submit(function( event ) {
			console.log("beja ovde");
			var ulazi=$(".form-control");
	    	var niz={};
	    	$.each(ulazi, function( index, value ) {
			   console.log(value.value.length);
			   if(value.id==="email")
			   {
			   		niz["email_adresa"]=value.value;
			   }
			   else
			   {
			   		niz[value.id]=value.value;
			   }
			});
			console.log(niz);
			$.ajax({
	            method: "POST",
	            url: Settings.url_prijavljivanje,
	            data:niz,
	            success: function(data)
	            {
	                console.log(data);
	                if(data['Status']==="Uspesno prijavljivanje!")
	                {
	                		window.location.replace(Settings.url_pocetna);
	                }
	                else if(data['Status'] === "Uspesno prijavljivanje recepcionera!")
	                {
	                	window.location.replace(Settings.recepcioner_url);
	                }
	                else if(data['Status'] === "Uspesno prijavljivanje radnika u restoranu!")
	                {
	                	window.location.replace(Settings.radnikURestoranu_url);
	                }
	                else if(data['Status'] === "Uspesno prijavljivanje radnika u kaficu!")
	                {
	                	window.location.replace(Settings.radnikUKaficu_url);
	                }
	                else if(data['Status'] === "Uspesno prijavljivanje admina!")
	                {
	                	window.location.replace(Settings.admin_url);
	                }
	                else
	                {
	                		alert("Pogresno ste uneli podatke ili je doslo do problema na serveru pokusajte ponovo!");
	                }



	                
	            },
	            dataType: "json"
        	});

		});
    });


})(jQuery);