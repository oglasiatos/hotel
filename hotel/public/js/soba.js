(function ($) {
    "use strict";


    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    $(document).ready(function(){
		
		var niz={};
		niz['broj_sobe']=$(".br_sobe").val();
    	$.ajax({
            method: "POST",
            url: Settings.url_vratisobu,
            data:niz,
            success: function(data)
            {
                console.log(data);
                if(!$.isEmptyObject(data))
                {
                	$('#brojSobe').empty();
                	$('#brojSobe').append(data['broj_sobe']);
                	$(".opissobe").empty();
                	$(".opissobe").append("<br>"+data["opis"]);
                	$(".list1").empty();
                	var s="<li><span>Broj sobe: &nbsp;</span> "+data["broj_sobe"]+" </li>";
                    s+="<li><span>Cena po noći: &nbsp;</span>"+data["cena_po_noci"]+"</li>";
                    s+="<li><span>Sprat: &nbsp;</span>"+data["sprat"]+"</li>";
                    s+="<li><span>Broj kreveta: &nbsp;</span> "+data["broj_kreveta"]+"</li>";
                    s+="<li><span>Pogled: &nbsp;</span> "+data["pogled"]+" </li>";
                    $(".list1").append(s);
                    $(".list2").empty();
                    s="<li><span>Terasa: &nbsp;</span> "+data["terasa"]+" </li>";
                    s+="<li><span>Klima: &nbsp;</span> "+data["klima"]+" </li>";
                    s+="<li><span>Wifi: &nbsp;</span> "+data["wifi"]+" </li>";
                    s+="<li><span>Tv: &nbsp;</span>  "+data["tv"]+" </li>";
                    s=s.split("true").join("Da");
                    s=s.split("false").join("Ne");
                    $(".list2").append(s);
                }

            },
            dataType: "json"
    	});
		if($(".korisnik").val() === "Nema Korisnika!")
		{
			$(".rezervacija").hide();
			$(".proverirez").hide();
		}
		else
		{
			$(".proverirez").hide();
			var niz={};
			niz["broj_sobe"]=$(".br_sobe").val();
			niz["email_adresa_korisnika"]=$(".korisnik").val();
			$.ajax({
	            method: "POST",
	            url: Settings.url_otkazirezervaciju,
	            data:niz,
	            success: function(data)
	            {
	                console.log(data); 
	                if(!$.isEmptyObject(data))
	                {
	                	$(".rezervacija").val("Otkazi");
	                	$(".rezervacija").addClass("otkazi");
	                	$(".otkazi").removeClass("rezervacija");

	                	$(".otkazi").click(function(){
	                		console.log(data[0]["id"]);
	                		var niz={};
							niz['id']=data[0]["id"];
					    	$.ajax({
					            method: "POST",
					            url: Settings.url_obrisirezervaciju,
					            data:niz,
					            success: function(data)
					            {
					                console.log(data);
					                if(data["Status"]==="Uspesno otkazana rezervacija sobe!")
					                {
					                	location.reload();
					                }
					                else
					                {
					                	alert("Otkazivanje nije uspelo pokusajte kasnije");
					                }

					            },
					            dataType: "json"
					    	});

	                	});
	                }
	                else
	                {
	                	$(".rezervacija").click(function(){
							$(".proverirez").show();
						});



						$(".rezervisi").click(function(){
							var niz={};
							niz["broj_sobe"]=$(".br_sobe").val();
							niz["rezervisana_od"]=$(".checkin_date").val();
							niz["rezervisana_do"]=$(".checkout_date").val();
							niz["email_adresa_korisnika"]=$(".korisnik").val();
							console.log(niz);
							$.ajax({
					            method: "POST",
					            url: Settings.url_rezrvacija,
					            data:niz,
					            success: function(data)
					            {
					            	if(data["Status"]==="Nije slobodna soba!")
					            	{
					            		alert("Soba je zauzeta u zaljenom periodu za radi lakseg odabira sobe koristite filter na stranici sobe!!");
					            	}
					            	else if (data["Status"]==="Datum nije validan!") 
					            	{
					            		alert("Datum nije unet ispravno");
					            	}
					            	else
					            	{
					                	$(".proverirez").hide();
					            		$(".rezervacija").val("Otkazi");
					            		 location.reload();
					            	}
					            },
					            dataType: "json"
				        	});
						});
	                }
	            },
	            dataType: "json"
        	});
						
		}
		
    });


})(jQuery);