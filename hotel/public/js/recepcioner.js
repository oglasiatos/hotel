(function($){

	$(function(){
		console.log("Recepcioner");
		ukloniProsleRezervacijeIzListeRezervacija();
		zauzimanjeSobaZaRezervacijeKojeSuNaRedu();
		izlistajSveSobeHotela();
		$('.deoZaRacun').hide();
		izlistavanjeSvihRezervacija();
	});

	function izlistajSveSobeHotela()
	{
		var s = "";
		$('.izlistavanjeSoba').empty();
		$.ajax({
			method: "POST",
			url: Settings.izlistavanjeSvihSobaHotela_url,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<div class='col-sm col-md-6 col-lg-4 ftco-animate fadeInUp ftco-animated'><div class='room'>";
					s += "<a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"' class='img d-flex justify-content-center align-items-center' style='background-image: url(images/room-1.jpg);'>";
					s += "<div class='icon d-flex justify-content-center align-items-center'>";
					s += "<span class='icon-search2'></span></div></a>";
					s += "<div class='text p-3 text-center'>";
					s += "<h3 class='mb-3'><a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"'>Soba "+l['broj_sobe']+"</a></h3>";
					s += "<p><span class='price mr-2'>"+l['cena_po_noci']+"€</span> <span class='per'>po noći</span></p>";
					s += "<ul class='list'><li><span>Broj sobe:</span> "+l['broj_sobe']+" </li>";
					s += "<li><span>Pogled:</span> "+l['pogled']+" </li><li><span>Broj kreveta:</span> "+l['broj_kreveta']+"</li>";
					s += "</ul><hr><p class='pt-1'><a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"' class='btn-custom'> Detaljnije <span class='icon-long-arrow-right'></span></a></p>";
					s += "</div></div></div>";

					$('.izlistavanjeSoba').append(s);

					s = "";
				});
			},
			dataType: "json"
		});
	};

	$('.dugmeZaFiltriranjeSoba').click(function(){
		prikupiInformacijeZaFiltriranjeSoba();
	});

	function prikupiInformacijeZaFiltriranjeSoba()
	{
		var input = $('.filtriranjeSoba');
		var stringArray = {};
		for(var i = 0; i < input.length; i++)
		{
			if($(input[i]).attr('type')==="checkbox")
			{
				stringArray[$(input[i]).attr('name')] = $(input[i]).is(":checked");
			}
			else
			{
				if($(input[i]).attr('name') === "broj_kreveta")
				{
					if($(input[i]).val() === "")
					{
						stringArray[$(input[i]).attr('name')] = 0;
					}
					else
					{
						stringArray[$(input[i]).attr('name')] = parseInt($(input[i]).val());
					}
				}
				else if($(input[i]).attr('name') === "minimalna_cena")
				{
					if($(input[i]).val()==="")
					{
						stringArray[$(input[i]).attr('name')] = 0;
					}
					else
					{
						stringArray[$(input[i]).attr('name')] = $(input[i]).val();
					}
				}
				else if($(input[i]).attr('name') === "maksimalna_cena")
				{
					if($(input[i]).attr('name') === "maksimalna_cena")
					{
						if($(input[i]).val() === "")
						{
							stringArray[$(input[i]).attr('name')] = 15000;
						}
						else 
						{
							stringArray[$(input[i]).attr('name')] = $(input[i]).val();
						}
					}
				}
				else
				{
					stringArray[$(input[i]).attr('name')] = $(input[i]).val();
				}
			}
		}
		console.log(stringArray);
		filtriranjeSobaPremaUnetimKriterijumima(stringArray);
	};

	function filtriranjeSobaPremaUnetimKriterijumima(stringArray)
	{
		var s = "";
		$('.izlistavanjeSoba').empty();
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.filtriranjeSobaPremaUnetimKriterijumima_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				$.each(data, function(i, l){

					s += "<div class='col-sm col-md-6 col-lg-4 ftco-animate fadeInUp ftco-animated'><div class='room'>";
					s += "<a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"' class='img d-flex justify-content-center align-items-center' style='background-image: url(images/room-1.jpg);'>";
					s += "<div class='icon d-flex justify-content-center align-items-center'>";
					s += "<span class='icon-search2'></span></div></a>";
					s += "<div class='text p-3 text-center'>";
					s += "<h3 class='mb-3'><a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"'>Soba "+l['broj_sobe']+"</a></h3>";
					s += "<p><span class='price mr-2'>"+l['cena_po_noci']+"€</span> <span class='per'>po noći</span></p>";
					s += "<ul class='list'><li><span>Broj sobe:</span> "+l['broj_sobe']+" </li>";
					s += "<li><span>Pogled:</span> "+l['pogled']+" </li><li><span>Broj kreveta:</span> "+l['broj_kreveta']+"</li>";
					s += "</ul><hr><p class='pt-1'><a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"' class='btn-custom'> Detaljnije <span class='icon-long-arrow-right'></span></a></p>";
					s += "</div></div></div>";

					$('.izlistavanjeSoba').append(s);

					s = "";
				});
			},
			dataType: "json"
		});
	};

	function izlistavanjeSvihRezervacija()
	{
		$('.listaSvihRezervacija').empty();
		var s = "";
		$.ajax({
			method: "POST",
			url: Settings.listaSvihRezervacija_url,
			success: function(data){
				console.log(data);

				$.each(data, function(i, l){

					s += "<tr><td>"+l['broj_sobe']+"</td><td>"+l['email_adresa_korisnika']+"</td>";
					s += "<td>"+l['rezervisana_od']+"</td><td class = 'inputZaIzmenuRezervacije"+l['id']+"'>"+l['rezervisana_do']+"</td><td>"+l['ukupan_trosak_boravka']+"</td>";
					s += "<td class = 'dugmeZaUslugu"+l['id']+"'>";
					s += "<a href = '#deoZaRacun'>"
					s += "<input type='button' value='Dodaj uslugu' class='dodajUslugu btn btn-primary btn-sm  py-2 px-4 dodavanjeUsluge"+l['id']+"'></a></td>";
					s += "<td class = 'dugmeZaIzmenu"+l['id']+"'>";
					s += "<input type='button' value='Izmeni' class='izmeni btn btn-primary btn-sm  py-2 px-4 izmenaRezervacije"+l['id']+"'></td>";
					s += "<td>";
					s += "<input type='button' value='Naplati' class='naplati btn btn-primary btn-sm  py-2 px-4 naplataBoravka"+l['id']+" dugmeZaNaplatu"+l['id']+"'></td>";
					s += "</tr>";

					$('.listaSvihRezervacija').append(s);

					$('.izmenaRezervacije'+l['id']).click(function(d){
						console.log(l['id']);
						izmenaRezervacije(l['id'], l['rezervisana_do'], "");
					});

					$('.naplataBoravka'+l['id']).click(function(d){
						console.log(l['id']);
						naplatiBoravak(l['id'], "");
					});

					$('.dodavanjeUsluge'+l['id']).click(function(d){
						console.log(l['id']);
						dodajUslugu(l['id'], "");
					});

					s = "";
				});
			},
			dataType: "json"
		});
	};

	$('.dodavanjeNoveRezervacije').click(function(){
		dodavanjeNoveRezervacije();
	});

	function dodavanjeNoveRezervacije()
	{
		$('.dodavanjeNoveRezervacije').hide();
		var s = "";
		s += "<tr><td>";
		s += "<input type = 'text' name= 'broj_sobe' class = 'dodavanjeRezervacije' style='width:100%;'/>";
		s +="</td><td><input type = 'text' name = 'email_adresa_korisnika' class='dodavanjeRezervacije'></td>";
		s += "<td><input type='date' name='rezervisana_od' class='dodavanjeRezervacije' style='width:85%;'></td><td>";
		s += "<input type='date' name='rezervisana_do' class='dodavanjeRezervacije' style='width:85%;'></td><td>";
		s += "</td>";
		s += "<td>";
		s += "<input type='button' value='Sačuvaj rezervaciju' class='dodajUslugu btn btn-primary btn-sm  py-2 px-4 sacuvajNovuRezervaciju'></td>";
		s += "<td>";
		s += "<input type='button' value='Poništi' class='izmeni btn btn-primary btn-sm  py-2 px-4 ponistavanjeRezervacije'></td>";
		s += "<td></td></tr>";
		$('.listaSvihRezervacija').append(s);

		$('.sacuvajNovuRezervaciju').click(function(){
			prikupljanjeInformacijaZaDodavanjeNoveRezervacije();
		});

		$('.ponistavanjeRezervacije').click(function(){
			ponistavanjeRezervacije();
		})
	};

	function ponistavanjeRezervacije()
	{
		$.confirm({
			title: "Ponistavanje rezervacije",
			content: "Da li ste sigurni da želite da poništite rezervaciju?",
			buttons: {
				Da: function(){
					$('.dodavanjeNoveRezervacije').show();
					izlistavanjeSvihRezervacija();
				},
				Ne: function(){

				},
			}
		});
	};

	function prikupljanjeInformacijaZaDodavanjeNoveRezervacije()
	{
		var stringArray = {};
		var input = $('.dodavanjeRezervacije');
		var b = true;
		for(var i = 0; i < input.length; i++)
		{
			if($(input[i]).val() === "")
			{
				b = false;
			}
			stringArray[$(input[i]).attr('name')] = $(input[i]).val();
		}
		if(b)
		{
			$.confirm({
				title: "Prikupljanje informacija",
				content: "Da li ste sigurni da želite da dodate rezervaciju?",
				buttons: {
					Da: function(){
						dodajNovuRezervaciju(stringArray);
					},
					Ne: function(){

					},
				}
			});
		}
		else
		{
			$.confirm({
				title: "Prikupljanje informacija",
				content: "Sve informacije u vezi sa rezervacijom moraju biti unete!",
				buttons: {
					OK: function(){

					},
				},
			});
		}
	};

	function dodajNovuRezervaciju(stringArray)
	{
		console.log(stringArray);
		$.ajax({
				method: "POST",
				url: Settings.dodavanjeNoveRezervacije_url,
				data: stringArray,
				success: function(data)
				{
					console.log(data);
					if(data['Status'] === "Uspesno obavljena rezervacija sobe!")
					{
						$.confirm({
							title: "Uspesno obavljena rezervacija",
							content: "Uspešno ste obavili rezervaciju sobe!",
							buttons: {
								OK: function() {
									$('.dodavanjeNoveRezervacije').show();
									izlistavanjeSvihRezervacija();
								},
							}
						});
					}
					else if(data['Status'] === "Datum nije validan!")
					{
						$.confirm({
							title: "Nekorektan datum!",
							content: "Datumi koje ste uneli nisu validni!",
							buttons: {
								OK: function(){

								},
							}
						});
					}
					else if(data['Status'] === "Soba ne postoji!")
					{
						$.confirm({
							title: "Nepostojeca soba",
							content: "Uneta soba ne postoji!",
							buttons: {
								OK: function(){

								},
							}
						});
					}
					else
					{
						$.confirm({
							title: "Nema slobodnih soba",
							content: "Trenutno u hotelu nema slobodnih soba za datume koje ste uneli!",
							buttons: {
								OK: function(){
									$('.dodavanjeNoveRezervacije').show();
									izlistavanjeSvihRezervacija();
								},
							}
						});
					}
				},
				dataType: "json"
			});
	};


	function izmenaRezervacije(idRezervacije, rezervisanaDo, sadrzajPretrage)
	{
		console.log(rezervisanaDo);
		var s = "";
		trenutnaVrednostDatumaDo = $('.inputZaIzmenuRezervacije'+idRezervacije).val();
		console.log(trenutnaVrednostDatumaDo);
		$('.inputZaIzmenuRezervacije'+idRezervacije).empty();
		s += "<input name = 'rezervisana_do' type = 'date' value = '' class = 'inputZaIzmenuRezervacije'>";
		$('.inputZaIzmenuRezervacije'+idRezervacije).append(s);
		$('.dugmeZaUslugu'+idRezervacije).empty();
		$('.dugmeZaIzmenu'+idRezervacije).empty();
		$('.dugmeZaNaplatu'+idRezervacije).hide();
		s = "";
		s += "<input type='button' value='Sačuvaj izmenu' class='dodajUslugu btn btn-primary btn-sm  py-2 px-4 sacuvajIzmenu"+idRezervacije+"'></td>";
		$('.dugmeZaUslugu'+idRezervacije).append(s);
		s = "";
		s += "<input type='button' value='Poništi izmenu' class='dodajUslugu btn btn-primary btn-sm  py-2 px-4 ponistiIzmenu'></td>";
		$('.dugmeZaIzmenu'+idRezervacije).append(s);

		$('.ponistiIzmenu').click(function(){
			ponistavanjeIzmene(sadrzajPretrage);
		});

		$('.sacuvajIzmenu'+idRezervacije).click(function(d){
			sacuvajIzmenuRezervacije(idRezervacije, $('.inputZaIzmenuRezervacije').val(), sadrzajPretrage);
			console.log(idRezervacije); console.log($('.inputZaIzmenuRezervacije').val());
		});
	};


	$('.dugmeZaPretrazivanjeRezervacija').click(function(){
		var sadrzajPretrage = $('.sadrzajPretrage').val();
		console.log(sadrzajPretrage);
		if(sadrzajPretrage === "")
		{
			izlistavanjeSvihRezervacija();
		}
		else
		{
			pretraziRezervacijePremaBrojuSobe(sadrzajPretrage);
		}
	});

	function pretraziRezervacijePremaBrojuSobe(sadrzajPretrage)
	{
		var s = "";
		$('.listaSvihRezervacija').empty();
		var stringArray = {};
		stringArray['broj_sobe'] = sadrzajPretrage;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.pretrazivanjeRezervacija_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				$.each(data, function(i, l){
					s += "<tr><td>"+l['broj_sobe']+"</td><td>"+l['email_adresa_korisnika']+"</td>";
					s += "<td>"+l['rezervisana_od']+"</td><td class = 'inputZaIzmenuRezervacije"+l['id']+"'>"+l['rezervisana_do']+"</td><td>"+l['ukupan_trosak_boravka']+"</td>";
					s += "<td class = 'dugmeZaUslugu"+l['id']+"'>";
					s += "<input type='button' value='Dodaj uslugu' class='dodajUslugu btn btn-primary btn-sm  py-2 px-4 dodavanjeUsluge"+l['id']+"'></td>";
					s += "<td class = 'dugmeZaIzmenu"+l['id']+"'>";
					s += "<input type='button' value='Izmeni' class='izmeni btn btn-primary btn-sm  py-2 px-4 izmenaRezervacije"+l['id']+"'></td>";
					s += "<td>";
					s += "<input type='button' value='Naplati' class='naplati btn btn-primary btn-sm  py-2 px-4 naplataBoravka"+l['id']+" dugmeZaNaplatu"+l['id']+"'></td>";
					s += "</tr>";

					$('.listaSvihRezervacija').append(s);

					$('.izmenaRezervacije'+l['id']).click(function(d){
						console.log(l['id']);
						izmenaRezervacije(l['id'], l['rezervisana_do'], sadrzajPretrage);
					});

					$('.naplataBoravka'+l['id']).click(function(d){
						console.log(l['id']);
						naplatiBoravak(l['id'], sadrzajPretrage);
					});

					$('.dodavanjeUsluge'+l['id']).click(function(d){
						console.log(l['id']);
						dodajUslugu(l['id'], sadrzajPretrage);
					});

					s = "";
				});
			},
			dataType: "json"
		});
	};

	function ponistavanjeIzmene(sadrzajPretrage)
	{
		$.confirm({
			title: "Ponistavanje izmene",
			content: "Da li ste sigurni da želite da poništite izmenu?",
			buttons:{
				Da: function(){
					if(sadrzajPretrage === "")
					{
						izlistavanjeSvihRezervacija();
					}
					else
					{
						pretraziRezervacijePremaBrojuSobe(sadrzajPretrage);
					}
				},
				Ne: function(){

				},
			}
		});
	};

	function sacuvajIzmenuRezervacije(idRezervacije, noviDatumDo, sadrzajPretrage)
	{
		var stringArray = {};
		stringArray['id_rezervacije'] = idRezervacije;
		stringArray['novi_datum_do'] = noviDatumDo;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.izmenaDatumaRezervacije_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				if(data['Status'] === "Uspesno izmenjena rezervacija!")
				{
					$.confirm({
						title: "Izmenjena rezervacija",
						content: "Uspešno ste izmenili rezervaciju!",
						buttons: {
							OK: function(){
								if(sadrzajPretrage === "")
								{
									izlistavanjeSvihRezervacija();
								}
								else
								{
									pretraziRezervacijePremaBrojuSobe(sadrzajPretrage);
								}
							},
						}
					});
				}
				else
				{
					$.confirm({
						title: "Neuspela izmena",
						content: "Izmena rezervacije nije uspela!",
						buttons: {
							OK: function(){
							},
						}
					});
				}
			},
			dataType: "json"
		});
	};

	function naplatiBoravak(idRezervacije, sadrzajPretrage)
	{
		var stringArray = {};
		stringArray['id'] = idRezervacije;
		console.log(stringArray);
		$('.deoZaRacun').show();
		$('.listaZaUkupniRacun').empty();
		var s = "";
		$.ajax({
			method: "POST",
			url: Settings.prikazRacuna_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				s += "<a href='#sekcijaRezervacije'><button class = 'naplati btn btn-primary btn-sm  py-2 px-4 naplatiRacun'>Naplati</button></a>";
				//s += "<input type='button' value='Zatvori' class=' naplati btn btn-primary btn-sm  py-2 px-4 zatvoriRacun'/>";	
				s += "<hr><li><span>Ukupno:</span>";
				s += "<label class='float-right'>"+data['ukupan_trosak_boravka']+"</label>";
						
				$('.listaZaUkupniRacun').append(s);

				s = "";
				$('.naplatiRacun').click(function(){
					naplataBoravka(idRezervacije, sadrzajPretrage);
					$('.deoZaRacun').hide();
				});
			},
			dataType: "json"
		});

		prikaziListuUsluga(idRezervacije);
	};

	function naplataBoravka(idRezervacije, sadrzajPretrage)
	{
		var stringArray = {};
		stringArray['id'] = idRezervacije;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.naplataBoravka_url,
			data:stringArray,
			success: function(data){
				if(data['Status'] === "Uspesno otkazana rezervacija sobe!")
				{
					$.confirm({
						title: "Naplata boravka",
						content: "Uspešno obavljena naplata!",
						buttons:{
							OK: function(){
								if(sadrzajPretrage === "")
								{
									izlistavanjeSvihRezervacija();
								}
								else
								{
									pretraziRezervacijePremaBrojuSobe(sadrzajPretrage);
								}
							},
						}
					});
				}
				else
				{
					$.confirm({
						title: "Naplata boravka",
						content: "Naplata boravka nije prošla uspešno!",
						buttons: {
							OK: function(){
							},
						}
					});
				}
			},
			dataType: "json"
		});
	}

	function prikazRacuna(idRezervacije)
	{
		var stringArray = {};
		stringArray['id'] = idRezervacije;
		console.log(stringArray);
		$('.deoZaRacun').show();
		$('.listaZaUkupniRacun').empty();
		var s = "";
		$.ajax({
			method: "POST",
			url: Settings.prikazRacuna_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				s += "<a href='#sekcijaRezervacije'><button class = 'naplati btn btn-primary btn-sm  py-2 px-4 zatvoriRacun'>Zatvori</button></a>";
				//s += "<input type='button' value='Zatvori' class=' naplati btn btn-primary btn-sm  py-2 px-4 zatvoriRacun'/>";	
				s += "<hr><li><span>Ukupno:</span>";
				s += "<label class='float-right'>"+data['ukupan_trosak_boravka']+"</label>";
						
				$('.listaZaUkupniRacun').append(s);

				s = "";
				$('.zatvoriRacun').click(function(){
					$('.deoZaRacun').hide();
				});
			},
			dataType: "json"
		});
	}


	function dodajUslugu(idRezervacije)
	{
		prikazRacuna(idRezervacije);
		prikaziListuUsluga(idRezervacije);
	};

	function prikaziListuUsluga(idRezervacije)
	{
		$('.usluge').empty();

		var s = "";
		s += "<h3> Usluge: ";
		s += "<select class='odabirVrsteUsluge float-right'>";
		s += "<option class='odabir'>Vrsta usluge:</option>";
		s += "<option>Osnovna</option><option>Dodatna</option>";
		s += "<option>Prikazi sve</option>";
		s += "</select></h3>";
		$('.usluge').append(s);

		$('.odabir').hide();

		$('.odabirVrsteUsluge').on('change', function(){
			console.log($(this).val());
			if($(this).val()==="Prikazi sve")
			{
				prikaziListuUsluga(idRezervacije);
			}
			else
			{
				izlistajUslugePremaVrstiUsluga(idRezervacije, $(this).val());
			}
		});


		$('.listaUsluga').empty();

		s = "";
		$.ajax({
			method: "POST",
			url: Settings.vratiListuUsluga_url,
			success: function(data){
				$.each(data, function(i, l){
					s += "<li><span>"+l['naziv_usluge']+": </span>";
					s += "<input type='button' value='+'";
					s += "class='float-right dodaj btn btn-primary btn-sm dodajUslugu"+l['id']+"'><label class='float-right'>"+l['cena']+" &nbsp; </label>";
					s += "<hr></li>";

					$('.listaUsluga').append(s);

					$('.dodajUslugu'+l['id']).click(function(d){
						console.log(l['id']);
						dodajUsluguNaRacun(idRezervacije, l['id']);
					});

					s = "";

				});
			},
			dataType: "json"
		});
	};

	function izlistajUslugePremaVrstiUsluga(idRezervacije, vrstaUsluge)
	{
		$('.listaUsluga').empty();
		var stringArray = {};
		stringArray['vrsta_usluge'] = vrstaUsluge+" usluga";
		console.log(stringArray);
		s = "";
		$.ajax({
			method: "POST",
			url: Settings.izlistajUslugePremaVrstiUsluga_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				$.each(data, function(i, l){
					s += "<li><span>"+l['naziv_usluge']+": </span>";
					s += "<input type='button' value='+'";
					s += "class='float-right dodaj btn btn-primary btn-sm dodajUslugu"+l['id']+"'><label class='float-right'>"+l['cena']+" &nbsp; </label>";
					s += "<hr></li>";

					$('.listaUsluga').append(s);

					$('.dodajUslugu'+l['id']).click(function(d){
						console.log(l['id']);
						dodajUsluguNaRacun(idRezervacije, l['id']);
					});

					s = "";

				});
			},
			dataType: "json"
		});
	}

	function dodajUsluguNaRacun(idRezervacije, idUsluge)
	{
		var stringArray = {};
		stringArray['id'] = idRezervacije;
		stringArray['id_usluge'] = idUsluge;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.dodavanjeUslugeNaRacun_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				prikazRacuna(idRezervacije);
			},
			dataType: "json"
		});
	};

	function ukloniProsleRezervacijeIzListeRezervacija()
	{
		$.ajax({
			method: "POST",
			url: Settings.uklanjanjeProteklihRezervacija_url,
			success: function(data){
				console.log(data);
			},
			dataType: "json"
		});
	};

	function zauzimanjeSobaZaRezervacijeKojeSuNaRedu()
	{
		$.ajax({
			method: "POST",
			url: Settings.zauzimanjeSobaZaRezervacijeKojeSuNaRedu_url,
			success: function(data){
				console.log(data);
			},
			dataType: "json"
		});
	};

	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});

})(jQuery);