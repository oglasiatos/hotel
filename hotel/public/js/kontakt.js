(function($){

	$(function(){
		console.log("Kontakt");
		sastavljanjePorukeZaAdmina();
	});

	function sastavljanjePorukeZaAdmina()
	{
		console.log("Sastavljanje poruke");
		var s = "";
		$('.formaZaSlanjePoruke').empty();
		$.ajax({
			method: "POST",
			url: Settings.sastavljanjePorukeZaAdmina_url,
			success: function(data){

				console.log(data);
				if(data['Status'] === "Nema sesije!")
				{
					s += "<div class='form-group'><input id='ime' name='ime' type='text' class='form-control' placeholder='Ime'>";
					s += "</div><div class='form-group'>";
					s += "<input id='prezime' name='prezime' type='text' class='form-control' placeholder='Prezime'>";
					s += "</div><div class='form-group'>";
					s += "<input id='email' name='email_adresa' type='text' class='form-control' placeholder='E-mail adresa'>";
					s += "</div><div class='form-group'>";
					s += "<textarea id='pitanje' name='pitanje' cols='30' rows='7' class='form-control' placeholder='Pitanje'></textarea>";
					s += "</div><div class='form-group pull-right'>";
					s += "<input type='button' value='Slanje' class='btn btn-primary py-3 px-5 dugmeZaSlanjePoruke'></div>";

				}
				else
				{
					s += "<div class='form-group'><input id='ime' name='ime' type='text' class='form-control' value='"+data['ime']+"' disabled='true'/>";
					s += "</div><div class='form-group'>";
					s += "<input id='prezime' name='prezime' type='text' class='form-control' value='"+data['prezime']+"' disabled = 'true'/>";
					s += "</div><div class='form-group'>";
					s += "<input id='email' name='email_adresa' type='text' class='form-control' value='"+data['email_adresa']+"' disabled='true'>";
					s += "</div><div class='form-group'>";
					s += "<textarea id='pitanje' name='pitanje' cols='30' rows='7' class='form-control' placeholder='Pitanje'></textarea>";
					s += "</div><div class='form-group pull-right'>";
					s += "<input type='button' value='Slanje' class='btn btn-primary py-3 px-5 dugmeZaSlanjePoruke'></div>";
				}

				$('.formaZaSlanjePoruke').append(s);

					$('.dugmeZaSlanjePoruke').click(function(){
						prikupljanjeInformacijaZaSlanjePoruke();
					});

			},
			dataType: "json"
		});
	};

	function prikupljanjeInformacijaZaSlanjePoruke()
	{
		var input = $('.form-control');
		console.log(input);
		var stringArray = {};
		var b = true;
		for(var i = 0; i < input.length; i++)
		{
			stringArray[$(input[i]).attr('name')] = $(input[i]).val();
			if($(input[i]).val() === "")
				b = false;
		}
		if(b)
		{
			posaljiPitanjeAdminu(stringArray);
		}
		else
		{
			$.confirm({
				title: "Nevalidne informacije",
				content: "Unesite sve informacije neophodne za slanje poruke!",
				buttons: {
					OK: function(){

					},
				}
			});
		}
	};

	function posaljiPitanjeAdminu(stringArray)
	{
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.posaljiPitanjeAdminu_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				if(data['Status'] === "Uspesno postavljeno pitanje!")
				{
					$.confirm({
						title: "Poslato pitanje",
						content: "Uspešno ste postavili pitanje adminu",
						buttons: {
							OK: function(){
								sastavljanjePorukeZaAdmina();
							},
						}
					});
				}
			},
			dataType: "json"
		});
	};


	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});




})(jQuery);