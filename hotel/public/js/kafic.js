(function($){

	$(function(){
		console.log("Kafic");
		izlistajSvaPica();
	});

	function izlistajSvaPica()
	{
		$('.kartaPica').empty();

		var s = "";

		$.ajax({
			method: "POST",
			url: Settings.izlistajSvaPica_url,
			success: function(data)
			{
				var duzina;
				if(data.length % 2 === 0)
					duzina = data.length/2;
				else
					duzina = (data.length+1)/2;
				console.log(duzina);

				s += "<div class='col-md-6'>";

				for(var i = 0; i < duzina; i++)
				{
					s += "<div class='pricing-entry d-flex ftco-animate fadeInUp ftco-animated'>";
					s += "<div class='img' style='background-image: url(images/menu-1.jpg);'></div>";
					s += "<div class='desc pl-3'><div class='d-flex text align-items-center'>";
					s += "<h3><span>"+data[i]['naziv_jela_ili_pica']+"</span></h3><span class='price'>$"+data[i]['cena']+"</span>";
					s += "</div><div class='d-block'><p></p>";
					s += "</div></div></div>";
				}

				s += "</div><div class='col-md-6'>";

				for(var j = i; j < data.length; j++)
				{
					s += "<div class='pricing-entry d-flex ftco-animate fadeInUp ftco-animated'>";
					s += "<div class='img' style='background-image: url(images/menu-1.jpg);'></div>";
					s += "<div class='desc pl-3'><div class='d-flex text align-items-center'>";
					s += "<h3><span>"+data[j]['naziv_jela_ili_pica']+"</span></h3><span class='price'>$"+data[j]['cena']+"</span>";
					s += "</div><div class='d-block'><p></p>";
					s += "</div></div></div>";
				}

				s += "</div>";

				$('.kartaPica').append(s);
			},
			dataType: "json"
		});
	};


	$('.dugmeZaRezervacijuStola').click(function(){
		prikupiInformacijeZaRezervacijuStola();
	});

	function prikupiInformacijeZaRezervacijuStola()
	{
		var input = $('.rezervacijaStola');
		var b = true;
		var stringArray = {};
		for(var i = 0;i < input.length; i++)
		{
			if($(input[i]).val() != "")
			{
				if($(input[i]).attr('name') === "broj_mesta")
				{
					stringArray[$(input[i]).attr('name')] = parseInt($(input[i]).val());
				}
				else
				{
					stringArray[$(input[i]).attr('name')] = $(input[i]).val()
				}
			}
			else
			{
				b = false;
			}
		}
		console.log(stringArray);

		if(b)
		{
			$.confirm({
				title: "Rezervacija stola!",
				content: "Da li ste sigurni da želite da obavite rezervaciju?",
				buttons: {
					Da: function(){
						console.log(stringArray);
						rezervisiStoUKaficu(stringArray);
					},
					Ne: function(){
					},
				}
			});
		}
		else
		{
			$.confirm({
				title: "Rezervacija stola!",
				content: "Sve informacije u vezi sa rezervacijom moraju biti unete!",
				buttons: {
					OK: function(){
						
					},
				}
			});
		}
	};


	function rezervisiStoUKaficu(stringArray)
	{
		stringArray['kafic_ili_restoran'] = "Kafic";
		$.ajax({
			method: "POST",
			url: Settings.rezervisanjeStolaUKaficu_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				if(data['Status'] === "Uspesno obavljena rezervacija stola!")
				{
					$.confirm({
						title: "Obavljena rezervacija",
						content: "Uspešno ste obavili rezervaciju!",
						buttons: {
							OK: function(){
								$('.rezervacijaStola').val("");
							},
						}
					});
				}
				else
				{
					console.log("Nema");
					$.confirm({
						title: "Neobavljena rezervacija",
						content: "U kafiću nema slobodnih stolova za obavljanje rezervacije. Probajte da obavite rezervaciju za neki drugi datum!",
						buttons: {
							OK: function(){
								
							},
						}
					});
				}
			},
			dataType: "json"
		});
	};



	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});



})(jQuery);