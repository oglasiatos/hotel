(function($){
	"use strict";
	$(function(){
		console.log("Usao");
		izlistavanjeSvihOdgovorenihPitanja();
		izlistavanjeSvihUtisaka();
	});

	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});


	function izlistavanjeSvihOdgovorenihPitanja()
	{
		var s = "";
		$('.izlistavanjeOdgovorenihPitanja').empty();

		$.ajax({
			method: "POST",
			url: Settings.izlistavanjeSvihOdgovorenihPitanja,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<li class='comment'><h5>"+l['ime']+" "+l['prezime']+"</h5>";
					s += "<span class='pitanje'>"+l['pitanje']+"</span>";
					s += "<div class='col-md-10 pt-2'><span class='odgovor'>";
					s += "<input type='text' disabled='true' class='input-group border-0' placeholder='"+l['odgovor']+"'> </span>";
					s += "</div><hr></li>";
				});
				$('.izlistavanjeOdgovorenihPitanja').append(s);
			},
			dataType: "json"
		});

	};

	var trKorisnik;
	function izlistavanjeSvihUtisaka()
	{
		var s = "<h3>Knjiga utisaka</h3><br>";
		$('.izlistavanjeUtisaka').empty();

		$.ajax({
			method: "POST",
			url: Settings.izlistavanjeSvihUtisaka,
			success: function(data){
				trKorisnik = data[0]['trenutni_korisnik'];
				console.log(trKorisnik);

				console.log(data);
				

				$.each(data, function(i, l){
					s += "<h5>"+l['ime']+" "+l['prezime']+"</h5><div class='col-lg'>";
					s += "<textarea class='border-0' style='width:400px; min-height:70px;overflow:auto' disabled='true' >"+l['utisak']+"</textarea></div>";
				});
				$('.izlistavanjeUtisaka').append(s);

				if(trKorisnik != "Nema sesije!")
				{
					console.log(trKorisnik);
					s = "";
					var stringArray = {};
					stringArray['email_adresa'] = trKorisnik;
					console.log(stringArray);
					$.ajax({
						method: "POST",
						url: Settings.vratiKorisnikaCijaSesijaJeAktivna,
						data: stringArray,
						success: function(data)
						{
							console.log(data);
							console.log(data['ime']);
							console.log(data['prezime']);
							s += "<br><h5>"+data['ime']+" "+data['prezime']+"</h5><div class='col-lg'>";
							s += "<textarea class='unosUtiska' style='width:400px; min-height:70px;overflow:auto'></textarea>";
							s += "<br><input type='button' value='Postavi' class=' float-right postavi btn btn-primary btn-sm dodavanjeNovogUtiska"+data['id']+"'></div>";

							$('.izlistavanjeUtisaka').append(s);

							$('.dodavanjeNovogUtiska'+data['id']).click(function(){
								console.log(data['id']);
								prikupljanjeInformacijaZaDodavanjeNovogUtiska(data['id']);
							})
						},
						dataType: "json"
					});
				}
			},
			dataType: "json"
		});
	};

	function prikupljanjeInformacijaZaDodavanjeNovogUtiska(id)
	{
		if($('.unosUtiska').val() === "")
		{
			$.confirm({
				title: "Unos novog utiska",
				content: "Unesite utisak!",
				buttons: {
					OK: function(){

					},
				}
			});
		}
		else
		{
			$.confirm({
				title: "Unos novog utiska",
				content: "Da li ste sigurni da želite da unesete ovaj utisak?",
				buttons: {
					Da: function(){
						dodavanjeNovogUtiska(id);
					},
					Ne: function(){

					},
				}
			});
		}
	};


	function dodavanjeNovogUtiska(id)
	{
		var stringArray = {};
		stringArray['id'] = id;
		stringArray['utisak'] = $('.unosUtiska').val();
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.dodavanjeNovogUtiska_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				$.confirm({
					title: "Unesen utisak",
					content: "Hvala Vam što ste uneli svoj utisak o našem hotelu!",
					buttons: {
						OK: function(){

						},
					}
				});
				izlistavanjeSvihUtisaka();
			},
			dataType: "json"
		});
	}


})(jQuery);