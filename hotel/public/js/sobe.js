(function($){
	"use strict";
	$(function(){

		console.log("Usao");
		izlistajSveHotele();
	});

	function izlistajSveHotele()
	{
		$('.izlistavanjeSoba').empty();
		var s = "";
		console.log("kale");

		$.ajax({
			method: "POST",
			url: Settings.base_url,
			success: function(data) {
				console.log(data);

				$.each(data, function(i, l){
					console.log("Katarina");
					s+="<div class='col-sm col-md-6 col-lg-4 ftco-animate fadeInUp ftco-animated'>";
					s+="<div class='room'>";
					s+="<a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"' class='img d-flex justify-content-center align-items-center' style='background-image: url(images/room-1.jpg);''>";
					s+="<div class='icon d-flex justify-content-center align-items-center'>";
					s+="<span class='icon-search2'></span></div></a><div class='text p-3 text-center'>";
					s+="<h3 class='mb-3'><a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"'>Soba"+l['broj_sobe']+"</a></h3>";
					s+="<p><span class='price mr-2'>"+l['cena_po_noci']+"€</span> <span class='per'>po noći</span></p>";
					s+="<ul class='list'><li><span>Broj sobe:</span> "+l['broj_sobe']+" </li><li><span>Pogled:</span> "+l['pogled']+" </li>";
					s+="<li><span>Broj kreveta:</span> "+l['broj_kreveta']+"</li></ul><hr>";
					s+="<p class='pt-1'><a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"' class='btn-custom'> Detaljnije <span class='icon-long-arrow-right'></span></a></p>";
					s+="</div></div></div>";

					//$('.izlistavanjeSoba').append(s);
					//s="";
				});
				//s+="</div>";
				$('.izlistavanjeSoba').append(s);
			},
			dataType: "json"
		});
	};

	$('.minimalnaCena').on('input', function (ev) {
                               $('.vrednostiSlajdera').empty();
                               var s = "<input type='number' value="+$('.minimalnaCena').val()+" min='0' max='120000'/>	-";
                               s+="     <input type='number' value="+$('.maksimalnaCena').val()+" min='0' max='120000'/>";
                               $('.vrednostiSlajdera').append(s);
                           });

	$('.maksimalnaCena').on('input', function (ev) {
                               $('.vrednostiSlajdera').empty();
                               var s = "<input type='number' value="+$('.minimalnaCena').val()+" min='0' max='120000'/>	-";
                               s+="     <input type='number' value="+$('.maksimalnaCena').val()+" min='0' max='120000'/>";
                               $('.vrednostiSlajdera').append(s);
                           });

	$('.pretraziSobe').click(function(){
		var input = $('.pretrazivanje');
		var stringArray = {};
		for(var i = 0; i<input.length;i++)
		{
			if($(input[i]).attr('type')==="checkbox")
			{
				stringArray[$(input[i]).attr('name') ] = $(input[i]).is(':checked');
			}
			else
			{
				if($(input[i]).attr('name') === "broj_kreveta")
				{
					if($(input[i]).val() === "")
					{
						stringArray[$(input[i]).attr('name')] = 0;
					}
					else
					{
						stringArray[$(input[i]).attr('name')] = parseInt($(input[i]).val());
					}
				}
				else
				{
					stringArray[$(input[i]).attr('name')] = $(input[i]).val();
				}
			}
		}
		stringArray['minimalna_cena'] = parseInt($('.minimalnaCena').val());
		stringArray['maksimalna_cena'] = parseInt($('.maksimalnaCena').val());
		console.log(stringArray);
		filtriranjeSobaPremaKriterijumima(stringArray);
	});

	function filtriranjeSobaPremaKriterijumima(stringArray)
	{
		console.log("Pocelo filtriranje");

		$('.izlistavanjeSoba').empty();
		var s = "";
		console.log("kale");
		console.log(stringArray);

		$.ajax({
			method: "POST",
			url: Settings.filtriranjeSoba_url,
			data: stringArray,
			success: function(data) {
				console.log(data);

				$.each(data, function(i, l){
					console.log("Katarina");
					
					s+="<div class='col-sm col-md-6 col-lg-4 ftco-animate fadeInUp ftco-animated'>";
					s+="<div class='room'>";
					s+="<a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"' class='img d-flex justify-content-center align-items-center' style='background-image: url(images/room-1.jpg);''>";
					s+="<div class='icon d-flex justify-content-center align-items-center'>";
					s+="<span class='icon-search2'></span></div></a><div class='text p-3 text-center'>";
					s+="<h3 class='mb-3'><a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"'>Soba"+l['broj_sobe']+"</a></h3>";
					s+="<p><span class='price mr-2'>"+l['cena_po_noci']+"€</span> <span class='per'>po noći</span></p>";
					s+="<ul class='list'><li><span>Broj sobe:</span> "+l['broj_sobe']+" </li><li><span>Pogled:</span> "+l['pogled']+" </li>";
					s+="<li><span>Broj kreveta:</span> "+l['broj_kreveta']+"</li></ul><hr>";
					s+="<p class='pt-1'><a href='"+Settings.prikazVelikeSobe_url+l['broj_sobe']+"' class='btn-custom'> Detaljnije <span class='icon-long-arrow-right'></span></a></p>";
					s+="</div></div></div>";
				});

				$('.izlistavanjeSoba').append(s);
			},
			dataType: "json"
		});

	}

	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});


})(jQuery);