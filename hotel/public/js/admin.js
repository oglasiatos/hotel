(function($){

	$(function(){
		console.log("Pocetak");
		izlistajSvaNeodgovorenaPitanja();
		izlistajSveZaposlene();
	});

	function izlistajSvaNeodgovorenaPitanja()
	{
		$('.listaNeodgovorenihPitanja').empty();
		var s = "";
		$.ajax({
			method: "POST",
			url: Settings.listaNeodgovorenihPitanja_url,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<li><span class='pitanje'>"+l['pitanje']+"</span>";
					s += "<input type='button' value='Odgovori' class='float-right dodaj btn btn-primary btn-sm odgovaranjeNaPitanje"+l['id']+"'>";
					s += "<div class='col-md-10 pt-2'><span class='odgovor'>";
					s += "<input type='text' class='input-group border-0 odgovorNaPitanje' placeholder='Odgovor'> </span>";
					s += "</div><hr></li>";

					$('.listaNeodgovorenihPitanja').append(s);

					$('.odgovaranjeNaPitanje'+l['id']).click(function(d){
						console.log(l['id']);
						prikupiInformacijeZaOdgovaranjeNaPitanje(l['id']);
					});

					s = "";
				});
			},
			dataType: "json"
		});
	};

	function prikupiInformacijeZaOdgovaranjeNaPitanje(id)
	{
		console.log(id);
		if($('.odgovorNaPitanje').val() === "")
		{
			$.confirm({
				title: "Unos odgovora",
				content: "Unesite odgovor na pitanje!",
				buttons: {
					OK: function(){

					},
				}
			});
		}
		else
		{
			$.confirm({
				title: "Unos odgovora",
				content: "Da li ste sigurni da želite da odgovorite na ovo pitanje?",
				buttons: {
					Da: function(){
						odgovoriNaPostavljenoPitanje(id);
					},
					Ne: function(){

					},
				}
			});
		}
	};

	function odgovoriNaPostavljenoPitanje(id)
	{
		stringArray = {};
		stringArray['id'] = id;
		stringArray['odgovor'] = $('.odgovorNaPitanje').val();
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.odgovaranjeNaPitanje_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				izlistajSvaNeodgovorenaPitanja();
			},
			dataType: "json"
		});
	};


	function izlistajSveZaposlene()
	{
		var s = "";
		$('.sviZaposleniUHotelu').empty();
		$.ajax({
			method: "POST",
			url: Settings.izlistajSveZaposlene_url,
			success: function(data)
			{
				$.each(data, function(i, l){
					s += "<tr><td>"+l['ime']+"</td><td>"+l['prezime']+"</td><td>"+l['email_adresa']+"</td>";
					s += "<td>"+l['lozinka']+"</td>";
					if(l['tip_radnika'] === "Radnik u kaficu")
					{
						s += "<td> Radnik u kafiću </td><td>";
					}
					else
					{
						s += "<td>"+l['tip_radnika']+"</td><td>";
					}
					s += "<input type='button' value='Otpusti' class='otpusti btn btn-primary btn-sm  py-2 px-4 otpustanjeRadnika"+l['id']+"'></td>";
					s += "</tr>";

					$('.sviZaposleniUHotelu').append(s);

					$('.otpustanjeRadnika'+l['id']).click(function(d){
						console.log(l['id']);
						$.confirm({
							title: "Otpustanje radnika!",
							content: "Da li ste sigurni da zelite da otpustite ovog radnika?",
							buttons: {
								Da: function(){
									otpustiRadnika(l['id'], "");
								},
								Ne: function(){

								},
							}
						});
					});

					s = "";
				});
			},
			dataType: "json"
		});
	};

	function otpustiRadnika(id, filter)
	{
		stringArray = {};
		stringArray['id'] = id;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.otpustanjeRadnika_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				if(filter === "")
				{
					izlistajSveZaposlene();
				}
				else
				{
					filtriranjeRadnikaPremaTipuPoslaKojiObavljaju(filter);
				}
			},
			dataType: "json"
		});
	};


	$('.zaposljavanjeNovogRadnika').click(function(){
		var s = "<tr><td><input type='text' name='ime' class = 'zaposljavanje'/></td><td><input type='text' name='prezime' class = 'zaposljavanje'/></td><td><input type='text' name='email_adresa' class = 'zaposljavanje'/></td>";
		s += "<td><input type='text' name='lozinka' class = 'zaposljavanje'/></td>";
		s += "<td><select name='tip_radnika' class = 'zaposljavanje' style = 'height: 32px;'>";
		s += "<option value = 'Recepcioner' selected> Recepcioner </option>";
		s += "<option value = 'Radnik u kaficu'> Radnik u Kafiću </option>";
		s += "<option value = 'Radnik u restoranu'> Radnik u restoranu </option></select></td><td>"
		s += "<input type='button' value='Zaposli' class='otpusti btn btn-primary btn-sm  py-2 px-4 dugmeZaZaposljavanje'></td>";
		s += "</tr>";

		$('.zaposljavanjeNovogRadnika').hide();

		$('.sviZaposleniUHotelu').append(s);

		$('.dugmeZaZaposljavanje').click(function(){
			prikupiInformacijeZaZaposljavanjeNovogRadnika();
		});
	});

	function prikupiInformacijeZaZaposljavanjeNovogRadnika()
	{
		var stringArray = {};
		var b = true;
		var inputi = $('.zaposljavanje');
		for(var i = 0; i < inputi.length; i++)
		{
			if($(inputi[i]).val() != "")
			{
				stringArray[$(inputi[i]).attr('name')] = $(inputi[i]).val();
			}
			else
			{
				b = false;
			}
		}
		console.log(stringArray);
		if(b)
		{
			$.confirm({
				title: "Zaposljavanje novog radnika",
				content: "Da li ste sigurni da zelite da zaposlite ovog radnika?",
				buttons: {
					Da: function(){
						zaposliNovogRadnika(stringArray);
					},
					Ne: function(){

					},
				}
			});
		}
		else
		{
			$.confirm({
				title: "Nevalidno zaposljavanje!",
				content: "Za zaposljavanje radnika moraju biti unesene sve informacije!",
				buttons: {
					OK: function(){
						//$.alert('');
					},
				}
			});
		}
	}


	function zaposliNovogRadnika(stringArray)
	{
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.zaposljavanjeNovogRadnika_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				if(data['Status'] === "Uspesno zaposljen radnik!")
				{
					$('.zaposljavanjeNovogRadnika').show();
					izlistajSveZaposlene();
				}
				else
				{
					$.confirm({
						title: "Nevalidno zaposljavanje!",
						content: "Za zaposljavanje radnika moraju biti unesene sve informacije!",
						buttons: {
							OK: function(){
								//$.alert('');
							},
						}
					});
				}
			},
			dataType: "json"
		});
	};


	$('.pretrazivanjeRadnika').click(function(){
		var pretraga = $('.sadrzajPretrazivanja').val();
		if(pretraga === "")
		{
			izlistajSveZaposlene();
		}
		else
		{
			filtriranjeRadnikaPremaTipuPoslaKojiObavljaju(pretraga);
		}
	});

	function filtriranjeRadnikaPremaTipuPoslaKojiObavljaju(pretraga)
	{
		console.log("Katarina");
		$('.sviZaposleniUHotelu').empty();
		var s = "";
		var stringArray = {};
		stringArray['tip_radnika'] = pretraga;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.filtriranjeRadnikaPremaTipuPoslaKojiObavljaju_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<tr><td>"+l['ime']+"</td><td>"+l['prezime']+"</td><td>"+l['email_adresa']+"</td>";
					s += "<td>"+l['lozinka']+"</td>";
					if(l['tip_radnika'] === "Radnik u kaficu")
					{
						s += "<td> Radnik u kafiću </td><td>";
					}
					else
					{
						s += "<td>"+l['tip_radnika']+"</td><td>";
					}
					s += "<input type='button' value='Otpusti' class='otpusti btn btn-primary btn-sm  py-2 px-4 otpustanjeRadnika"+l['id']+"'></td>";
					s += "</tr>";

					$('.sviZaposleniUHotelu').append(s);

					$('.otpustanjeRadnika'+l['id']).click(function(d){
						console.log(l['id']);
						$.confirm({
							title: "Otpustanje radnika!",
							content: "Da li ste sigurni da zelite da otpustite ovog radnika?",
							buttons: {
								Da: function(){
									otpustiRadnika(l['id'], "");
								},
								Ne: function(){

								},
							}
						});
					});

					s = "";
				});
			},
			dataType: "json"
		});
	};

	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});


})(jQuery);