(function($){

	$(function(){
		console.log("Konobar Restoran");
		$('.deoZaRacun').hide();
		izlistajSveStolove();
	});

	function izlistajSveStolove()
	{
		var s = "";
		$('.listaStolovaRestoran').empty();
		$.ajax({
			method: "POST",
			url: Settings.listaSvihStolovaRestorana_url,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<div class='col-sm col-md-2 col-lg-2 ftco-animate fadeInUp ftco-animated'><div class='room'>";
					s += "<div class='text p-3 text-center'><a href = '#racunZaSto'><button class='broj_stola btn ";
					if(l['status'] === "Slobodan")
					{
						s += "btn-primary dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					else if(l['status'] === "Rezervisan")
					{
						s += "btn-custom dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					else
					{
						s += "btn-secondary dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					s += "<hr><ul class='list'><li><span>Broj mesta:</span>"+l['broj_mesta']+"</li>";
					s += "<li><span>Status:</span> "+l['status']+" </li></ul></div></div></div>";

					$('.listaStolovaRestoran').append(s);

					$('.dugmeZaRacun'+l['id']).click(function(d){
						console.log(l['id']);
						prikaziRacunZaSto(l['id'], l['broj_stola'], "");
						izlistajSvaJela(l['id'], l['broj_stola'], "");
						izlistajSvaPica(l['id'], l['broj_stola'], "");
					});

					s = "";
				});

				
			},
			dataType: "json"
		});
	};

	function prikaziRacunZaSto(id, brojStola, filter)
	{
		console.log(id);
		console.log(brojStola);
		$('.promeniBrojStola').empty();
		$('.promeniBrojStola').append(brojStola);
		$('.deoZaRacun').show();
		$('.prikazRacuna').empty();
		$(".brstolazaprikaz").empty();
		$(".brstolazaprikaz").append(brojStola);
		var s = "";
		var stringArray = {};
		stringArray['id'] = id;
		$.ajax({
			method: "POST",
			url: Settings.prikazRacunaZaSto_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				s += "<hr><li><span>Ukupno: </span>"+data;
				s += "<input type='button' value='Naplati' class='naplati float-right btn btn-primary btn-sm  py-2 px-4 naplacivanjeStolu"+id+"'></li><br><li>"
    			s += "<a href='#stolovi'><input type='button' value='Zatvori' class='naplati float-right btn btn-primary btn-sm  py-2 px-4 zatvori'/></a></li>";
    			$('.prikazRacuna').append(s);

    			$('.naplacivanjeStolu'+id).click(function(d){
    				console.log(id);
    				naplatiRacunZaSto(id, "");
    			});

    			$('.zatvori').click(function(){
    				$('.deoZaRacun').hide();
    			});

    			s = "";
			},
			dataType: "json"
		});

	};

	function izlistajSvaPica(id, brojStola, filter)
	{
		$('.kartaPica').empty();

		var s = "";
		s += "<h3> Karta pića ";
		s += "<select class=' float-right odabirPica'><option class='odabirZeljenogPica'>Odaberite vrstu pića</option>";
		s += "<option>Aperitiv</option><option>Dzin</option><option>Viski</option";
		s += "<option>Burbon</option><option>Konjak</option><option>Brendi</option>";
		s += "<option>Votka</option><option>Tekila</option><option>Rum</option><option>Rakija</option>";
		s += "<option>Pivo</option><option>Likeri</option><option>Gazirani sokovi</option>";
		s += "<option>Negazirani sokovi</option><option>Sveze cedjeni sokovi</option>";
		s += "<option>Mineralna voda</option><option>Energetski napici</option>";
		s += "<option>Bezalkoholni kokteli</option><option>Kokteli</option>";
		s += "<option>Kafe i topli napici</option>";
		s += "<option>Prikazi sve</option>";
		s += "</select></h3>";
		var stringArray = {};
		stringArray['id'] = id;
		console.log(stringArray);
		$('.kartaPica').append(s);
		$('.odabirZeljenogPica').hide();


		$('.listaPica').empty();
		s = "";
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.izlistavanjePicaIDodavanjeNaRacun_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<li><span>"+l['naziv_jela_ili_pica']+"</span><input type='button' value='+'";
					s += "class='float-right dodaj btn btn-primary btn-sm dodavanjePicaNaRacun"+l['id']+"'><label class='float-right'>"+l['cena']+" &nbsp; </label><hr></li>";

					$('.listaPica').append(s);

					$('.dodavanjePicaNaRacun'+l['id']).click(function(){
						dodajPiceNaRacunZaSto(id, l['id'], filter, brojStola);
					});

					s = "";
				});

			},
			dataType: "json"
		});

		$('.odabirPica').on('change', function(){
			if($(this).val()==="Prikazi sve")
			{
				izlistajSvaPica(id, brojStola, filter);
			}
			else
			{
				izlistajSvaPicaPremaVrstiPica(id, brojStola, filter, $(this).val());
			}
		});
	}

	function izlistajSvaPicaPremaVrstiPica(id, brojStola, filter, filterZaPica)
	{
		var stringArray = {};
		stringArray['vrsta_jela_ili_pica'] = filterZaPica;
		console.log(stringArray);
		var s = "";
		$('.listaPica').empty();
		$.ajax({
			method: "POST",
			url: Settings.izlistajSvaPicaPremaVrstiPica_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				$.each(data, function(i, l){
					s += "<li><span>"+l['naziv_jela_ili_pica']+"</span><input type='button' value='+'";
					s += "class='float-right dodaj btn btn-primary btn-sm dodavanjeJelaNaRacun"+l['id']+"'><label class='float-right'>"+l['cena']+" &nbsp; </label><hr></li>";

					$('.listaPica').append(s);

					$('.dodavanjePicaNaRacun'+l['id']).click(function(){
						dodajPiceNaRacunZaSto(id, l['id'], filter, brojStola);
					});

					s = "";
				});
			},
			dataType: "json"
		});
	}

	function izlistajSvaJela(id, brojStola, filter)
	{

		$('.jelovnik').empty();
		s = "";
		s += "<h3> Jela: ";
		s += "<select class = 'float-right odabirJela'><option class='odabirVrsteJela'>Odaberite vrstu jela</option><option>Omlet</option>";
		s += "<option>Jaja</option><option>Sendvic</option>";
		s += "<option>Hladno predjelo</option><option>Toplo predjelo</option>";
		s += "<option>Supa i corba</option><option>Riblji specijalitet</option>";
		s += "<option>Posna i vegeterijanska jela</option>";
		s += "<option>Preporuke sefa kuhinje</option>";
		s += "<option>Jela po porudzbini</option>";
		s += "<option>Specijalitet kuce</option><option>Jelo sa rostilja</option>";
		s += "<option>Prilog</option><option>Salata</option><option>Salata obrok</option>";
		s += "<option>Deciji meni</option><option>Nasa mala poslasticara</option>";
		s += "<option>Prikazi sve</option></select></h3>";
		$('.jelovnik').append(s);
		$('.odabirVrsteJela').hide();

		$('.odabirJela').on('change', function(){
			console.log($(this).val());
			if($(this).val()!="Odaberite vrstu jela")
			{
				if($(this).val()!="Prikazi sve")
				{
					izlistajJelaPremaVrstiJela(id, brojStola, filter, $(this).val());
				}
				else
				{
					izlistajSvaJela(id, brojStola, filter);
				}
			}
		});


		$('.listaJela').empty();

		s = "";
		var stringArray = {};
		stringArray['id'] = id;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.izlistavanjeJelaIDodavanjeNaRacun_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<li><span>"+l['naziv_jela_ili_pica']+"</span><input type='button' value='+'";
					s += "class='float-right dodaj btn btn-primary btn-sm dodavanjeJelaNaRacun"+l['id']+"'><label class='float-right'>"+l['cena']+" &nbsp; </label><hr></li>";

					$('.listaJela').append(s);

					$('.dodavanjeJelaNaRacun'+l['id']).click(function(){
						dodajJeloNaRacunZaSto(id, l['id'], filter, "Prikazi sve", brojStola);
					});

					s = "";
				});

			},
			dataType: "json"
		});
	}

	function izlistajJelaPremaVrstiJela(id, brojStola, filter, filterZaJela)
	{
		var stringArray = {};
		stringArray['vrsta_jela_ili_pica'] = filterZaJela;
		var s = "";
		$('.listaJela').empty();
		$.ajax({
			method: "POST",
			url: Settings.izlistajJelaPremaVrstiJela_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				$.each(data, function(i, l){
					s += "<li><span>"+l['naziv_jela_ili_pica']+"</span><input type='button' value='+'";
					s += "class='float-right dodaj btn btn-primary btn-sm dodavanjeJelaNaRacun"+l['id']+"'><label class='float-right'>"+l['cena']+" &nbsp; </label><hr></li>";

					$('.listaJela').append(s);

					$('.dodavanjeJelaNaRacun'+l['id']).click(function(){
						dodajJeloNaRacunZaSto(id, l['id'], filter, filterZaJela);
					});

					s = "";
				});
			},
			dataType: "json"
		});
	};

	function naplatiRacunZaSto(id, filter)
	{
		console.log(id);
		var stringArray = {};
		stringArray['id'] = id;
		$.ajax({
			method: "POST",
			url: Settings.naplatiRacunZaSto_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				if(data['Status'] === "Uspesno oslobodjen sto!")
				{
					$('.deoZaRacun').hide();
					if(filter==="")
					{
						izlistajSveStolove();
					}
					else
					{
						filtrirajStolovePremaBrojuMesta(filter);
					}
				}
				else
				{
					$.confirm({
						title: "Naplacivanje racuna",
						content: "Naplata racuna nije prosla uspesno!",
						buttons: {
							OK: function(){

							},
						}
					});
				}
			},
			dataType: "json"
		});
	};
	function dodajJeloNaRacunZaSto(id, cena, filter, filterZaJela, brojStola)
	{
		var stringArray = {};
		stringArray['id'] = id;
		stringArray['racun'] = cena;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.dodavanjeJeloNaRacun_url,
			data: stringArray,
			success: function(data)
			{
				if(data['Status'] === "Uspesno formiran racun za sto!")
				{
					prikaziRacunZaSto(id, brojStola, filter);
					/*if(filterZaJela != "Prikazi sve")
					{
						izlistajJelaPremaVrstiJela(id, brojStola, filter, filterZaJela);
					}
					else
					{
						izlistajSvaJela(id, brojStola, filter);
					}*/
				}
				else
				{
					$.confirm({
						title: "Dodavanje jela na racun",
						content: "Neuspesno dodavanje jela na racun za sto!",
						buttons: {
							OK: function(){

							},
						}
					});
				}
			},
			dataType: "json"
		});
	};

	function dodajPiceNaRacunZaSto(id, cena, filter, brojStola)
	{
		var stringArray = {};
		stringArray['id'] = id;
		stringArray['racun'] = cena;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.dodavanjePicaNaRacun_url,
			data: stringArray,
			success: function(data)
			{
				if(data['Status'] === "Uspesno formiran racun za sto!")
				{
					prikaziRacunZaSto(id, brojStola, filter);
					/*if(filterZaPica === "Prikazi sve")
					{
						izlistajSvaPica(id, brojStola, filter);
					}
					else
					{
						izlistajSvaPicaPremaVrstiPica(id, brojStola, filter, filterZaPica);
					}*/
				}
				else
				{
					$.confirm({
						title: "Dodavanje pica na racun",
						content: "Neuspesno dodavanje pica na racun za sto!",
						buttons: {
							OK: function(){

							},
						}
					});
				}
			},
			dataType: "json"
		});
	};

	$('.dugmeZaRezervacijuStola').click(function(){
		prikupiInformacijeZaRezervacijuStola();
	});

	function prikupiInformacijeZaRezervacijuStola()
	{
		var input = $('.rezervacijaStola');
		var b = true;
		var stringArray = {};
		for(var i = 0;i < input.length; i++)
		{
			if($(input[i]).val() != "")
			{
				if($(input[i]).attr('name') === "broj_mesta")
				{
					stringArray[$(input[i]).attr('name')] = parseInt($(input[i]).val());
				}
				else
				{
					stringArray[$(input[i]).attr('name')] = $(input[i]).val()
				}
			}
			else
			{
				b = false;
			}
		}
		console.log(stringArray);

		if(b)
		{
			$.confirm({
				title: "Rezervacija stola!",
				content: "Da li ste sigurni da želite da obavite rezervaciju?",
				buttons: {
					Da: function(){
						console.log(stringArray);
						rezervisiStoURestoranu(stringArray);
					},
					Ne: function(){
					},
				}
			});
		}
		else
		{
			$.confirm({
				title: "Rezervacija stola!",
				content: "Sve informacije u vezi sa rezervacijom moraju biti unete!",
				buttons: {
					OK: function(){
						
					},
				}
			});
		}
	};


	function rezervisiStoURestoranu(stringArray)
	{
		stringArray['kafic_ili_restoran'] = "Restoran";
		$.ajax({
			method: "POST",
			url: Settings.rezervisanjeStolaURestoranu_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				if(data['Status'] === "Uspesno obavljena rezervacija stola!")
				{
					$.confirm({
						title: "Obavljena rezervacija",
						content: "Uspešno ste obavili rezervaciju!",
						buttons: {
							OK: function(){
								$('.rezervacijaStola').val("");
							},
						}
					});
				}
				else if(data['Status'] === "Datum nije korektno unet!")
				{
					console.log("Nema");
					$.confirm({
						title: "Neobavljena rezervacija",
						content: "Datum koji ste uneli nije korektan!",
						buttons: {
							OK: function(){
								
							},
						}
					});
				}
				else
				{
					console.log("Nema");
					$.confirm({
						title: "Neobavljena rezervacija",
						content: "U kafiću nema slobodnih stolova za obavljanje rezervacije. Probajte da obavite rezervaciju za neki drugi datum!",
						buttons: {
							OK: function(){
								
							},
						}
					});
				}
			},
			dataType: "json"
		});
	};

	$('.pretrazivanjeStolovaPremaBrojuMesta').click(function(){
		console.log($('.sadrzajPretrazivanja').val());
		if($('.sadrzajPretrazivanja').val() === "")
		{
			izlistajSveStolove();
		}
		else
		{
			filtrirajStolovePremaBrojuMesta($('.sadrzajPretrazivanja').val());
		}
	});

	function filtrirajStolovePremaBrojuMesta(sadrzajPretrazivanja)
	{
		var s = "";
		$('.listaStolovaRestoran').empty();
		var stringArray = {};
		stringArray['broj_mesta'] = parseInt(sadrzajPretrazivanja);
		stringArray['kafic_ili_restoran'] = "Restoran";
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.pretraziStolovePremaBrojuMesta_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				$.each(data, function(i, l){
					s += "<div class='col-sm col-md-2 col-lg-2 ftco-animate fadeInUp ftco-animated'><div class='room'>";
					s += "<div class='text p-3 text-center'><a href = '#racunZaSto'><button class='broj_stola btn ";
					if(l['status'] === "Slobodan")
					{
						s += "btn-primary dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					else if(l['status'] === "Rezervisan")
					{
						s += "btn-custom dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					else
					{
						s += "btn-secondary dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					s += "<hr><ul class='list'><li><span>Broj mesta:</span>"+l['broj_mesta']+"</li>";
					s += "<li><span>Status:</span> "+l['status']+" </li></ul></div></div></div>";

					$('.listaStolovaRestoran').append(s);

					$('.dugmeZaRacun'+l['id']).click(function(d){
						console.log(l['id']);
						prikaziRacunZaSto(l['id'], l['broj_stola'], sadrzajPretrazivanja);
						izlistajSvaJela(id, brojStola, sadrzajPretrazivanja);
					});

					s = "";
				});
			},
			dataType: "json"
		});
	};

	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });



})(jQuery);