(function($){

	$(function(){
		console.log("Konobar kafic");
		$('.deoZaRacun').hide();
		izlistajSveStolove();
	});

	function izlistajSveStolove()
	{
		var s = "";
		$('.listaStolovaKafica').empty();
		$.ajax({
			method: "POST",
			url: Settings.listaSvihStolovaKafica_url,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<div class='col-sm col-md-2 col-lg-2 ftco-animate fadeInUp ftco-animated'><div class='room'>";
					s += "<div class='text p-3 text-center'><a href = '#racunZaSto'><button class='broj_stola btn ";
					if(l['status'] === "Slobodan")
					{
						s += "btn-primary dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					else if(l['status'] === "Rezervisan")
					{
						s += "btn-custom dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					else
					{
						s += "btn-secondary dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					s += "<hr><ul class='list'><li><span>Broj mesta:</span>"+l['broj_mesta']+"</li>";
					s += "<li><span>Status:</span> "+l['status']+" </li></ul></div></div></div>";

					$('.listaStolovaKafica').append(s);

					$('.dugmeZaRacun'+l['id']).click(function(d){
						console.log(l['id']);
						prikaziRacunZaSto(l['id'], l['broj_stola'], "");
						izlistajSvaPica(l['id'], l['broj_stola'], "");
					});

					s = "";
				});

				
			},
			dataType: "json"
		});
	};

	function prikaziRacunZaSto(id, brojStola, filter)
	{
		console.log(id);
		console.log(brojStola);
		$('.promeniBrojStola').empty();
		$('.promeniBrojStola').append(brojStola);
		$('.deoZaRacun').show();
		$('.prikazRacuna').empty();
		var s = "";
		var stringArray = {};
		stringArray['id'] = id;
		$.ajax({
			method: "POST",
			url: Settings.prikazRacunaZaSto_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				s += "<hr><li><span>Ukupno: </span>"+data;
    			s += "<input type='button' value='Naplati' class='naplati float-right btn btn-primary btn-sm  py-2 px-4 naplacivanjeStolu"+id+"'></li><br><li>";
    			s += "<a href='#stolovi'><input type='button' value='Zatvori' class='naplati float-right btn btn-primary btn-sm  py-2 px-4 zatvori'/></a></li>";

    			$('.prikazRacuna').append(s);

    			$('.naplacivanjeStolu'+id).click(function(d){
    				console.log(id);
    				naplatiRacunZaSto(id, filter);
    			});

    			$('.zatvori').click(function(){
    				$('.deoZaRacun').hide();
    			});

    			s = "";
			},
			dataType: "json"
		});
	};

	function izlistajSvaPica(id, brojStola, filter)
	{
		$('.kartaPica').empty();
		var s = "";
		s += "<h3> Karta pića ";
		s += "<select class=' float-right odabirPica'><option class='odabirZeljenogPica'>Odaberite vrstu pića</option>";
		s += "<option>Aperitiv</option><option>Dzin</option><option>Viski</option";
		s += "<option>Burbon</option><option>Konjak</option><option>Brendi</option>";
		s += "<option>Votka</option><option>Tekila</option><option>Rum</option><option>Rakija</option>";
		s += "<option>Pivo</option><option>Likeri</option><option>Gazirani sokovi</option>";
		s += "<option>Negazirani sokovi</option><option>Sveze cedjeni sokovi</option>";
		s += "<option>Mineralna voda</option><option>Energetski napici</option>";
		s += "<option>Bezalkoholni kokteli</option><option>Kokteli</option>";
		s += "<option>Kafe i topli napici</option>";
		s += "<option>Prikazi sve</option>";
		s += "</select></h3>";
		$('.kartaPica').append(s);

		$('.odabirPica').on('change', function(){
			if($(this).val()==="Prikazi sve")
			{
				izlistajSvaPica(id, brojStola, filter);
			}
			else
			{
				izlistajSvaPicaPremaVrstiPica(id, brojStola, filter, $(this).val());
			}
		});


		var stringArray = {};
		stringArray['id'] = id;
		$('.listaPica').empty();
		s = "";
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.izlistavanjePicaIDodavanjeNaRacun_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<li><span>"+l['naziv_jela_ili_pica']+"</span><input type='button' value='+'";
					s += "class='float-right dodaj btn btn-primary btn-sm dodavanjePicaNaRacun"+l['id']+"'><label class='float-right'>"+l['cena']+" &nbsp; </label><hr></li>";

					$('.listaPica').append(s);

					$('.dodavanjePicaNaRacun'+l['id']).click(function(){
						dodajPiceNaRacunZaSto(id, l['id'], filter, brojStola);
					});

					s = "";
				});

			},
			dataType: "json"
		});
	}

	function izlistajSvaPicaPremaVrstiPica(id, brojStola, filter, filterZaPica)
	{
		var stringArray = {};
		stringArray['id'] = id;
		stringArray['vrsta_jela_ili_pica'] = filterZaPica;
		console.log(stringArray);
		$('.listaPica').empty();
		s = "";
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.izlistajSvaPicaPremaVrstiPica_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				$.each(data, function(i, l){
					s += "<li><span>"+l['naziv_jela_ili_pica']+"</span><input type='button' value='+'";
					s += "class='float-right dodaj btn btn-primary btn-sm dodavanjePicaNaRacun"+l['id']+"'><label class='float-right'>"+l['cena']+" &nbsp; </label><hr></li>";

					$('.listaPica').append(s);

					$('.dodavanjePicaNaRacun'+l['id']).click(function(){
						dodajPiceNaRacunZaSto(id, l['id'], filter, brojStola);
					});

					s = "";
				});

			},
			dataType: "json"
		});
	}

	function naplatiRacunZaSto(id, filter)
	{
		console.log(id);
		var stringArray = {};
		stringArray['id'] = id;
		$.ajax({
			method: "POST",
			url: Settings.naplatiRacunZaSto_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				if(data['Status'] === "Uspesno oslobodjen sto!")
				{
					$('.deoZaRacun').hide();
					if(filter==="")
						izlistajSveStolove();
					else
						filtrirajStolovePremaBrojuMesta(filer);
				}
				else
				{
					$.confirm({
						title: "Naplacivanje racuna",
						content: "Naplata racuna nije prosla uspesno!",
						buttons: {
							OK: function(){

							},
						}
					});
				}
			},
			dataType: "json"
		});
	};

	function dodajPiceNaRacunZaSto(id, cena, filter, brojStola)
	{
		var stringArray = {};
		stringArray['id'] = id;
		stringArray['racun'] = cena;
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.dodavanjePicaNaRacun_url,
			data: stringArray,
			success: function(data)
			{
				if(data['Status'] === "Uspesno formiran racun za sto!")
				{
					prikaziRacunZaSto(id, brojStola, filter);
				}
				else
				{
					$.confirm({
						title: "Dodavanje pica na racun",
						content: "Neuspesno dodavanje pica na racun za sto!",
						buttons: {
							OK: function(){

							},
						}
					});
				}
			},
			dataType: "json"
		});
	};

	$('.dugmeZaRezervacijuStola').click(function(){
		prikupiInformacijeZaRezervacijuStola();
	});

	function prikupiInformacijeZaRezervacijuStola()
	{
		var input = $('.rezervacijaStola');
		var b = true;
		var stringArray = {};
		for(var i = 0;i < input.length; i++)
		{
			if($(input[i]).val() != "")
			{
				if($(input[i]).attr('name') === "broj_mesta")
				{
					stringArray[$(input[i]).attr('name')] = parseInt($(input[i]).val());
				}
				else
				{
					stringArray[$(input[i]).attr('name')] = $(input[i]).val()
				}
			}
			else
			{
				b = false;
			}
		}
		console.log(stringArray);

		if(b)
		{
			$.confirm({
				title: "Rezervacija stola!",
				content: "Da li ste sigurni da želite da obavite rezervaciju?",
				buttons: {
					Da: function(){
						console.log(stringArray);
						rezervisiStoUKaficu(stringArray);
					},
					Ne: function(){
					},
				}
			});
		}
		else
		{
			$.confirm({
				title: "Rezervacija stola!",
				content: "Sve informacije u vezi sa rezervacijom moraju biti unete!",
				buttons: {
					OK: function(){
						
					},
				}
			});
		}
	};


	function rezervisiStoUKaficu(stringArray)
	{
		stringArray['kafic_ili_restoran'] = "Kafic";
		$.ajax({
			method: "POST",
			url: Settings.rezervisanjeStolaUKaficu_url,
			data: stringArray,
			success: function(data)
			{
				console.log(data);
				if(data['Status'] === "Uspesno obavljena rezervacija stola!")
				{
					$.confirm({
						title: "Obavljena rezervacija",
						content: "Uspešno ste obavili rezervaciju!",
						buttons: {
							OK: function(){
								$('.rezervacijaStola').val("");
							},
						}
					});
				}
				else if(data['Status'] === "Datum nije korektno unet!")
				{
					console.log("Nema");
					$.confirm({
						title: "Neobavljena rezervacija",
						content: "Datum koji ste uneli nije korektan!",
						buttons: {
							OK: function(){
								
							},
						}
					});
				}
				else
				{
					console.log("Nema");
					$.confirm({
						title: "Neobavljena rezervacija",
						content: "U kafiću nema slobodnih stolova za obavljanje rezervacije. Probajte da obavite rezervaciju za neki drugi datum!",
						buttons: {
							OK: function(){
								
							},
						}
					});
				}
			},
			dataType: "json"
		});
	};

	$('.pretrazivanjeStolovaPoBrojuMesta').click(function(){
		if($('.sadrzajPretrazivanja').val()==="")
			izlistajSveStolove();
		else
			filtrirajStolovePremaBrojuMesta($('.sadrzajPretrazivanja').val());
	});

	function filtrirajStolovePremaBrojuMesta(sadrzajPretrazivanja)
	{
		var s = "";
		$('.listaStolovaKafica').empty();
		var stringArray = {};
		stringArray['broj_mesta'] = parseInt(sadrzajPretrazivanja);
		stringArray['kafic_ili_restoran'] = "Kafic";
		console.log(stringArray);
		$.ajax({
			method: "POST",
			url: Settings.pretraziStolovePremaBrojuMesta_url,
			data: stringArray,
			success: function(data){
				console.log(data);
				$.each(data, function(i, l){
					s += "<div class='col-sm col-md-2 col-lg-2 ftco-animate fadeInUp ftco-animated'><div class='room'>";
					s += "<div class='text p-3 text-center'><a href = '#racunZaSto'><button class='broj_stola btn ";
					if(l['status'] === "Slobodan")
					{
						s += "btn-primary dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					else if(l['status'] === "Rezervisan")
					{
						s += "btn-custom dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					else
					{
						s += "btn-secondary dugmeZaRacun"+l['id']+"' type='button'>"+l['broj_stola']+"</button></a>";
					}
					s += "<hr><ul class='list'><li><span>Broj mesta:</span>"+l['broj_mesta']+"</li>";
					s += "<li><span>Status:</span> "+l['status']+" </li></ul></div></div></div>";

					$('.listaStolovaKafica').append(s);

					$('.dugmeZaRacun'+l['id']).click(function(d){
						console.log(l['id']);
						prikaziRacunZaSto(l['id'], l['broj_stola'], sadrzajPretrazivanja);
					});

					s = "";
				});
			},
			dataType: "json"
		});
	};

	$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });



})(jQuery);