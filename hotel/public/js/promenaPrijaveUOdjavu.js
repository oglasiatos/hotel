(function($){


	$(function(){
		console.log("Promena prijave u odjavu!");
		promeniPrijavuUOdjavuUkolikoJePotrebno();
	});

	function promeniPrijavuUOdjavuUkolikoJePotrebno()
	{
		var s = "";
		$.ajax({
			method: "POST",
			url: Settings.proveriDaLiJeKorisnikPrijavljen_url,
			success: function(data){
				console.log(data);
				if(data['Status'] === "Korisnik je ulogovan!")
				{
					$('.klasaPrijavljivanje').empty();
					$('.klasaRegistracija').empty();
					s += "<a class='nav-link odjaviSe'>Odjavi se</a>";

					$('.klasaPrijavljivanje').append(s);

					$('.odjaviSe').click(function(){
						$.ajax({
							method: "POST",
							url: Settings.odjaviSe_url,
							success: function(data){
								if(data['Status'] === "Uspesna odjava!")
								{
									window.location = Settings.pocetnaStranica_url;
								}
							},
							dataType: "json"
						});
					});
				}
			},
			dataType: "json"
		});
	};

	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});

})(jQuery);