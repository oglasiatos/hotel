<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Vinelab\NeoEloquent\Schema\Blueprint;
use Vinelab\NeoEloquent\Migrations\Migration;
Route::get('/', function () {
    /*Neo4jSchema::label('User', function(Blueprint $label)
	{
	    $label->unique('uuid');
	    $label->index('ime');
	});*/
	class User extends NeoEloquent {

	    protected $label = 'User'; // or array('User', 'Fan')

	    protected $fillable = ['name', 'email'];
	}

	$user = User::create(['name' => 'mika', 'email' => 'miki@email.com']);

    //return view('welcome');
});
